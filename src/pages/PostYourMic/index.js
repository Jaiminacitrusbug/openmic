import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import HeaderDashboard from '../../components/HeaderDashboard/index';
import TopMenu from '../../components/TopMenu';
import PostMic from '../../components/PostYourMic/index';
import Footer from '../../components/Footer';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
class PostYourMic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticated: false,
    };
  }
  componentDidMount() {
    const { authenticated } = this.props;
    this.setState({ authenticated }, () => {
    });
  }

  componentDidUpdate(prevProps) {
    if(prevProps.authenticated !== this.props.authenticated) {
      this.setState({ authenticated: this.props.authenticated });
    } 
  }

  render() {
    const { authenticated } = this.state;
    return (
      <div className="wrapper">
        {authenticated === true? 
        <HeaderDashboard/>
       :
        <TopMenu />
        }
        <ToastContainer/>
  	   <PostMic />
	 			<Footer/>				
      </div>
    );
  }
}

const mapStateToProps = state => ({
  authenticated: state.authUser.authenticated,
});

// const mapDispatchToProps = dispatch => ({
//   addDateTime: date => dispatch(addDateTime(date)),
// });

export default withRouter(
  connect(
    mapStateToProps,
    null,
  )(PostYourMic),
);
