import React, { Component } from 'react';
import Flatpickr from 'react-flatpickr';
import 'flatpickr/dist/themes/material_blue.css';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { setStepCount, addDateTime } from '../../../actions/postMicFormActions';
import moment from 'moment';
import Select from 'react-select';
const options = [
  { value: 1, label: 'Monday' },
  { value: 2, label: 'Tuesday' },
  { value: 3, label: 'Wednesday' },
  { value: 4, label: 'Thursday' },
  { value: 5, label: 'Friday' },
  { value: 6, label: 'Saturday' },
  { value: 7, label: 'Sunday' },
];
const formatAMPM=(date) =>{
  var hours = date[0].getHours();
  var minutes = date[0].getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}

class DateTimeDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: '',
      errorForDate: '',
      day: '',
      errorForDay: '',
      time: ''
    
    };
    this.handlePrevious = this.handlePrevious.bind(this);
    this.onChange = this.onChange.bind(this);
    this.handleSubmitDateTime = this.handleSubmitDateTime.bind(this);
    this.handleDay = this.handleDay.bind(this);
  }

  componentDidMount() {
    const { selectedDate } = this.props;
    if (selectedDate) {
      this.setState({ date: selectedDate.time, day: selectedDate.day, time: selectedDate.newtime })
    }
  }

  handlePrevious() {
    const { setStepCount, addDateTime } = this.props;

    if (this.state.date === undefined) {
      this.setState({ errorForDate: 'Select Time'})
    }
    if (this.state.day === undefined) {
      this.setState({ errorForDay: 'Select Day'})
    }

    if ( this.state.date !== undefined && this.state.day !== undefined) {
      const count = 0;
    addDateTime(this.state.date);
    setStepCount(count);
    } 
    // else {
    //   this.setState({ errorForDate: 'Select Time'})   
    // }
  }

 
  onChange(date) {
    //console.log("date...",date)
    const newTime = date[0].getHours() + ':' + date[0].getMinutes();
    this.setState({ date: newTime,time:formatAMPM(date), errorForDate: '' }, () => {
    })
  }

  handleSubmitDateTime() {
    const { setStepCount, addDateTime } = this.props;
    if (this.state.date === undefined) {
      this.setState({ errorForDate: 'Select Time'})
    }
    if (this.state.day === undefined) {
      this.setState({ errorForDay: 'Select Day'})
    }
     
    if ( this.state.date !== undefined && this.state.day !== undefined) {
      const count = 2;
    const dayTime = {
      day: this.state.day,
      time: this.state.date,
      newtime: this.state.time,
    }
    addDateTime(dayTime);
    setStepCount(count);
  }
    // else {
    //   this.setState({ errorForDate: 'Select Time'})   
    // }
  }

  handleDay(day) {
    this.setState({ day, errorForDay: '' }, () => {
   });
  }

  render() {
    return (
      <fieldset>
        <div className="row">
            <div className="col-lg-12">
                <div className="step-title">
                {this.props.authenticated === false?
                 <h4>Step 2 of 7</h4>
                :
                <h4>Step 1 of 6</h4>
              }
                 <p>When does this mic happen? </p>
                </div>
                </div>
                    <div className="col-lg-6 col-md-6">
                      <div className="form-group">
                        <label className="has-float-label" >
                        {/* <input type='text' id="datetime" className="form-control" placeholder="select date" /> */}
                        <Flatpickr data-enable-time
                          options={{ 
                            enableTime: true,                           
                            noCalendar: true,
                            dateFormat: "G:i K",
                            // minTime: "00:00 AM",
                            // maxTime: "12:00 PM",
                            time_24hr: true,
                          }}
                          onChange={this.onChange}
                          placeholder="Select Time"
                          className="form-control"
                          value={this.state.date}
                        />
                        <span>Schedule Time *</span>
                      </label>
                      <label className='error'>{this.state.errorForDate}</label>
                    </div>
                </div>

                <div className="col-lg-6">
                  <div className="form-group">
                    <label className="has-float-label">
                      <div className="select2-div">
                        <Select
                          value={this.state.day}
                          onChange={this.handleDay}
                          options={options}
                          className="select-multiple"
                          placeholder="Select Day *"
                        />
                      </div>
                      {/* <input type="text" placeholder="Cost Type" className="form-control" /> */}
                      <span>Select Day*</span>
                    </label>
                    <label className="error">{this.state.errorForDay}</label>
                  </div>
                </div>
                
        </div>
        {/* <div className="col-lg-12 text-left"> */}
     
      {/* </div> */}

   

      <div className="row">
        <div className="col-lg-12">
            <div className="btn-groups-steps">
          {/* {this.props.authenticated === false?  <button onClick={this.handlePrevious} className="btn btn-prev">Previous</button> : null } */}
            <button onClick={this.handleSubmitDateTime} className="btn btn-next">Next</button>
            </div>
        </div>
      </div>

     
      </fieldset>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  setStepCount: count => dispatch(setStepCount(count)),
  addDateTime: date => dispatch(addDateTime(date)),
});
const mapStateToProps = state => ({
  count: state.accountDetails.count,
  selectedDate: state.accountDetails.selectedDate,
  authenticated: state.authUser.authenticated,
});
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(DateTimeDetails),
);
