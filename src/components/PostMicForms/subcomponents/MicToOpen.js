import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { addDescribe, setStepCount } from '../../../actions/postMicFormActions';

class MicToOpen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRadio: this.props.describeData &&  this.props.describeData.reason !== '' ? this.props.describeData.reason: '',
      displayText: this.props.describeData &&  this.props.describeData.reason !== '' && this.props.describeData.reason === 'No'? 'block':'none',
      describe: this.props.describeData &&  this.props.describeData.describe !== '' ? 
      this.props.describeData.describe : '',
      errorRadio: '',
      errorDescribe:'',
    };
    this.handleOnRadioChange = this.handleOnRadioChange.bind(this);
    this.handleTextarea = this.handleTextarea.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handlePreviousMic = this.handlePreviousMic.bind(this);
    this.radioOption = [
      { name: "Yes", value: "Yes" },
      { name: "No", value: "No" },
    ]
  }

  handleOnRadioChange(e) {
    const { value } = e.target;
    this.setState({ selectedRadio: value, errorRadio: '' }, () => {
      if (value === 'Yes') {
        this.setState({ displayText: 'none' })
      } else {
        this.setState({ displayText: 'block' })
      }
    })
  }

  handleTextarea(e) {
    const { value } = e.target;
    this.setState({ describe: value, errorDescribe: '' });
  }

  handleSubmit() {
    const { selectedRadio, describe } = this.state;
    if(selectedRadio !== undefined) {
  
      this.setState({ errorRadio: ''});
   if (selectedRadio === "Yes") {
      const describeData = {
        reason: selectedRadio,
      };
      this.props.addDescribe(describeData);
      const count = 3;
      this.props.setStepCount(count);
    } else {
      if (describe !== undefined) {
        const describeData = {
          reason: selectedRadio,
          describe: describe,
        };
        this.props.addDescribe(describeData);
        const count = 3;
        this.props.setStepCount(count);
      } else {
        this.setState({ errorDescribe: 'Please describe requirments '})
      }
    }   
   } else {
     this.setState({ errorRadio: 'Select One Option'});
   }
  }

  handlePreviousMic() {
   
    const { selectedRadio, describe } = this.state;
    if(selectedRadio !== undefined) {
     this.setState({ errorRadio: ''});
    if (selectedRadio === "Yes") {
       const describeData = {
         reason: selectedRadio,
       };
       this.props.addDescribe(describeData);
       const count = 1;
      this.props.setStepCount(count);
      } else {
       if (describe !== undefined) {
         const describeData = {
           reason: selectedRadio,
           describe: describe,
         };
         this.props.addDescribe(describeData);
         const count = 1;
         this.props.setStepCount(count);
        
        } else {
         this.setState({ errorDescribe: 'Please describe requirments '})
       }
     }   
    } else {
      this.setState({ errorRadio: 'Select One Option'});
    }
  }

  render() {
    return (
      <fieldset>
        <div className="row">
          <div className="col-lg-6">
            <div className="step-title">
            {this.props.authenticated === false?
                 <h4>Step 3 of 7</h4>
                :
                <h4>Step 2 of 6</h4>
              }
           <p>Is this mic open to all performers? </p>
            </div>
          </div>

          <div className="col-lg-12">
            <div className="radio-check">
              {this.radioOption.map((data, index) => {
                return <label key={index} className="container-radio">{data.name},{data.name === "No" ? <span>This mic is open only certian demographics.</span> : null}
                  <input type="radio" className="radio-no" name="radio" value={data.name}
                    checked={this.state.selectedRadio === data.name}
                    onChange={this.handleOnRadioChange}
                  />
                  <span className="checkmark"></span>
                </label>
              })}
                <label className="error">{this.state.errorRadio}</label>
            </div>
          </div>

          <div className="col-lg-12">
            <div className="form-group radio-checked" style={{ display: this.state.displayText }}>
              <label className="has-float-label">
                <textarea className="form-control text-area-100" name="describe" rows="4" id="describe" placeholder="please describe requirments to perform."
                  defaultValue={this.state.describe} onChange={this.handleTextarea} /> 
                {/* <span className="">Describe requirments *</span> */}
              </label>
              <label className="error">{this.state.errorDescribe}</label>
            </div>
          </div>
        </div>
        {/* <div className="col-lg-12 text-left"> */}
         
        {/* </div> */}
        <div className="row">
          <div className="col-lg-12 ">
              <div className="btn-groups-steps">
                {/* <a href="/postMicForm" className="btn btn-login">Agree</a> */}
                  <button onClick={this.handlePreviousMic} className="btn btn-prev">Previous</button>
                  <button onClick={this.handleSubmit} className="btn btn-next">Next</button>
              </div>
          </div>
        </div>
      </fieldset>
    );
  }
}

const mapDispatchToProps = dispatch => ({
   setStepCount: count => dispatch(setStepCount(count)),
  // addDateTime: date => dispatch(addDateTime(date)),
  addDescribe: describeData => dispatch(addDescribe(describeData)),
});
const mapStateToProps = state => ({
   count: state.accountDetails.count,
   describeData: state.accountDetails.describeData,
   authenticated: state.authUser.authenticated,
  // selectedDate: state.accountDetails.selectedDate,
});
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(MicToOpen),
);