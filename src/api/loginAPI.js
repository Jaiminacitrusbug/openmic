import axios from 'axios'
import {errorHandle }from './errorHandling'
import {APP_CONFIG} from '../constants/config'

export function logIn(data) {
  try {
      return axios.post(APP_CONFIG.API_URL + 'login', data,  
      {
          headers:{
          'Content-Type': 'application/json',
        },
      }
        )
          .then(function (response) {
              return response.data
          })
          .catch(function (error) {
              return errorHandle(error)
          })
  }catch (err) {console.log('Err::: ', err); return err }
}

export function forgetPassword(email) {
  try {
      return axios.post(APP_CONFIG.API_URL + 'forgot-password', email,  
      {
          headers:{
          'Content-Type': 'application/json',
        },
      }
        )
          .then(function (response) {
              return response.data
          })
          .catch(function (error) {
              return errorHandle(error)
          })
  }catch (err) {console.log('Err::: ', err); return err }
}

export function forgetPasswordValidation(token) {
  try {
      return axios.post(APP_CONFIG.API_URL + 'forgetPassword/' + token,  
      {
          headers:{
          'Content-Type': 'application/json',
        },
      }
        )
          .then(function (response) {
              return response.data
          })
          .catch(function (error) {
              return errorHandle(error)
          })
  }catch (err) {console.log('Err::: ', err); return err }
}

export function changePasswrod(userId,userPassword) {
  try {
      return axios.put(APP_CONFIG.API_URL + 'set-password/' + userId, userPassword,  
      {
          headers:{
          'Content-Type': 'application/json',
        },
      }
        )
          .then(function (response) {
              return response.data
          })
          .catch(function (error) {
              return errorHandle(error)
          })
  }catch (err) {console.log('Err::: ', err); return err }
}

export function changeCurrentPasswrod(userId,userPassword) {
  try {
      return axios.put(APP_CONFIG.API_URL + 'change-password/' + userId, userPassword,  
      {
          headers:{
          'Content-Type': 'application/json',
          'authorization': sessionStorage.getItem('token'),
        },
      }
        )
          .then(function (response) {
              return response.data
          })
          .catch(function (error) {
              return errorHandle(error)
          })
  }catch (err) {console.log('Err::: ', err); return err }
}

