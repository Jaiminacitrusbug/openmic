import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from 'yup';
import logoBlack from '../../assets/images/Size/Logoforwhite2.png';
import history from '../../services/BrowserHistory';
import { logIn } from '../../api/loginAPI';
import { userLogin, setAuthenticatedStatus } from '../../actions/authActions';
import { toast } from 'react-toastify';

class LoginModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keepLogin: false,
      showIcon: 'fa fa-fw fa-eye field-icon toggle-password',
      type: 'password',
      loader: false,
    };
    this.handleCheckbox = this.handleCheckbox.bind(this);
    this.handleShowPassword = this.handleShowPassword.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(values) {
    const { email, password } = values;
    const loginData = {
      email: email,
      password: password,
    }
    this.setState({ loader: true });
    logIn(loginData).then((result) => {
      if (result) {
        if (result.code === 200 && result.status === true) {
          sessionStorage.setItem('token', result.data.token);
          sessionStorage.setItem('user', JSON.stringify(result.data));
          this.props.userLogin(result.data);
          this.props.setAuthenticatedStatus(true);
          this.props.hide();
          this.setState({ loader: false });
          history.push('/dashboard');
        }  if (result.code === 200 && result.status === false) {
          toast.error(result.message);
          this.setState({ loader: false });
        }
      } else {
        this.setState({ loader: false });
      }  
    })

  };

  handleCheckbox(e) {
    const { checked } = e.target;
    this.setState({ keepLogin: checked });
  }

  handleShowPassword() {
    if (this.state.type === 'text') {
      this.setState({ type: 'password', showIcon: 'fa fa-fw fa-eye field-icon toggle-password' })
    } else {
      this.setState({ type: 'text', showIcon: 'fa fa-fw field-icon toggle-password fa-eye-slash' })
    }
  }

  render() {

    return (
      <Modal id="login-post-mic" className="modal fade modal-custom modal-login" role="dialog" show={this.props.display} onHide={this.props.hide}>
        <div className="modal-header">
          <h4 className=""></h4>
          <button type="button" className="close" data-dismiss="modal" onClick={this.props.hide}>&times;</button>
        </div>
        <div className="modal-body">
          <Formik
            initialValues={{
              email: '',
              password: '',
            }}
            validationSchema={Yup.object().shape({
              email: Yup.string()
                .email('Email is invalid')
                .required('Email is required'),
              password: Yup.string()
                .min(6, 'Password must be at least 6 characters')
                .required('Password is required'),
            })}
            onSubmit={this.handleSubmit}
          >
            {(values) => (
              <Form>
                <div className="modal-logo">
                  <a href="#">
                    <img src={logoBlack} alt="logo" />
                  </a>
                </div>
                <div className="modal-heading dotted-gradient">
                  <h3>Login and Post your mic...</h3>
                </div>
                <div className="login-form">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="account-text">
                        <p>You need an account to edit, manage, and eventually delete your mic when the venue goes out of business.</p>
                        <p>If you not have an account. <Link to="/signup" >Please register and create an account.</Link></p>
                      </div>
                    </div>

                    <div className="col-lg-6">
                      <div className="form-group">
                        <label className="has-float-label">
                          <Field
                            type="text"
                            name="email"
                            placeholder="Email Address"
                            className="form-control"
                          />
                          <span>Email Address *</span>
                        </label>
                        <ErrorMessage name="email" component="span" className="error" />
                      </div>
                    </div>
                    <div className="col-lg-6">
                      <div className="form-group">
                        <label className="has-float-label">
                          <Field
                            type={this.state.type}
                            name="password"
                            placeholder="Password"
                            className="form-control"
                          />
                          <span>Password *</span>
                        </label>
                        <span toggle="#password-field" onClick={this.handleShowPassword} className={this.state.showIcon}></span>
                        <ErrorMessage name="password" component="span" className="error" />
                      </div>
                    </div>

                    <div className="col-lg-6">

                      <label className="container-check">Keep me signed in
													<input type="checkbox" checked={this.state.keepLogin} onChange={this.handleCheckbox} />
                        <span className="checkmark"></span>
                      </label>
                    </div>
                    <div className="col-lg-6 text-right">
                      <div className="forgot-password-link">
                        <a href="/fpassword">Forgot password</a>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="login-form-footer">
                  <div className="row">
                    <div className="col-lg-4">
                      <div className="skip-arrow">
                        <Link to="/signup" >Skip and register <span><i className="material-icons">
                          arrow_right_alt
										</i></span>
                        </Link>

                      </div>
                    </div>
                    <div className="col-lg-8 text-right">
                      <div className="btn-groups">
                        <button onClick={this.props.hide} href="#" className="btn">Not Now</button>
                        <button disabled={this.state.loader} type="submit" className="btn btn-login">Login Now {this.state.loader === true ? <span className="spinner-border spinner-border-sm"></span> : null}</button>
                      </div>
                    </div>
                  </div>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </Modal>
    );
  }
}


const mapDispatchToProps = dispatch => ({
  //   addAddtionalInfo: additionalData => dispatch(addAddtionalInfo(additionalData)),
  userLogin: userData => dispatch(userLogin(userData)),
  setAuthenticatedStatus: auth => dispatch(setAuthenticatedStatus(auth)),
});
const mapStateToProps = state => ({
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(LoginModal),
);
