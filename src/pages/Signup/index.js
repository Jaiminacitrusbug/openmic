import React, { Component } from 'react';
import TopMenu from '../../components/TopMenu';
import Footer from '../../components/Footer';
import SignUp from '../../components/SignUp';
class Signup extends Component {
  render() {
    return (
      <div className="wrapper">
        <TopMenu/>
        <SignUp/>
        <Footer/>
      </div>
    );
  }
}

export default Signup;