// open mic development
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import history from '../../services/BrowserHistory';
import { Modal } from 'react-bootstrap';
import Select from 'react-select';
import LoginModal from '../../components/LoginModal';
//images
import logo from '../../assets/images/Size/Logoforblack@2x.png';
import logoBlack from '../../assets/images/Size/Logoforwhite2.png';
//import '../../assets/js/cbpAnimatedHeader';
import { renderLoginModal } from '../../actions/postMicFormActions';
import '../../assets/css/animate.min.css';
import theater from '../../assets/images/icon/theater.svg';
const options = [
  { value: 'I can’t acces my account', label: 'I can’t acces my account' },
  { value: 'Something Else', label: 'Something Else...' },
];

class TopMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      renderContactUsModal: false,
      accountType: '',
      renderLoginModal: false,
      styleForNav: 'cd-shadow-layer',
      navDiv: 'nav-div clearfix',
    };
    this.handleContanctUsModal = this.handleContanctUsModal.bind(this);
    this.handleCloseContanchUsModal = this.handleCloseContanchUsModal.bind(this);
    this.handleAccountType = this.handleAccountType.bind(this);
    this.handleLoginModal = this.handleLoginModal.bind(this);
    this.handleCloseLoginModal = this.handleCloseLoginModal.bind(this);
    this.handleNavBar = this.handleNavBar.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll.bind(this), true);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll.bind(this), true);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.renderLogin !== this.props.renderLogin) {
      if (this.props.renderLogin === true) {
        this.setState({ renderLoginModal: true });
      }
    }
  }

  handleScroll(event) {
    const winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    let sticky = document.getElementsByClassName('header-div cbp-af-header clearfix');
    if (sticky.length > 0) {
      if (winScroll >= 0) {
        sticky[0].classList.add('cbp-af-header-shrink');
      } else {
        sticky[0].classList.remove('cbp-af-header-shrink');
      }
    }
  }

  //methods for handle contact us modal 
  handleContanctUsModal() {
    this.setState({ renderContactUsModal: true });
  }

  handleCloseContanchUsModal() {
    this.setState({ renderContactUsModal: false });
  }

  handleAccountType(accountType) {
    this.setState({ accountType }, () => {
      if (accountType.value === 'I can’t acces my account') {
        history.push('/fpassword');
        this.handleCloseContanchUsModal();
      } else {
        history.push('/contactUs');
        this.handleCloseContanchUsModal();
      }
    });
  }

  handleLoginModal() {
    this.setState({ renderLoginModal: true });
  }

  handleCloseLoginModal() {
    this.setState({ renderLoginModal: false });
    this.props.renderLoginModal(false);
  }

  handleNavBar() {
    if (this.state.styleForNav === 'cd-shadow-layer') {
      this.setState({ styleForNav: 'cd-shadow-layer displayblock', navDiv: 'nav-div clearfix width80' });
    } else {
      this.setState({ styleForNav: 'cd-shadow-layer', navDiv: 'nav-div clearfix' });
    }
  }


  render() {
    return (
      <header>
        <div className="header-div cbp-af-header clearfix">
          <div className="inner-top-header-div clearfix">
            <div className="container">
              <div className="row">
                <div className="col-sm-12 col-lg-12">
                  <div className="logo-div">
                    <Link className="logo_link clearfix" to="/">
                      <img src={logo} className="logo-white" alt="I-mobile" /></Link>
                  </div>
                  <nav>
                    <div className="top-nav1">
                      <div className={this.state.styleForNav}></div>
                      <div className="nav-m-bar">
                        <a href="#" onClick={this.handleNavBar} className="opennav" data-placement="bottom" title="" data-original-title="Menu">
                          <i className="menu-bars"></i></a>
                      </div>
                      {/* <div className="fest-icon-bar">
                        <Link to="/comedyFestival"><span> Comedy festivals</span></Link>
                    </div> */}
                      <div className={this.state.navDiv} id="mySidenav">
                        <a href="#" onClick={this.handleNavBar} className="closebtn">&times;</a>


                        <ul className="nav ullist-inline " id="nav-res">
                        <li><Link to="/comedyFestival"><span> Comedy festivals</span></Link></li>
                          <li><a href="#" onClick={this.handleContanctUsModal} data-toggle="modal" data-target="#modal-conatct">Contact us</a></li>
                          <li><a href="#" onClick={this.handleLoginModal} data-toggle="modal" data-target="#login-post-mic">Login or Create Free Account</a></li>
                          {/* <li className="btn-signup"><Link to="/postYourMic" >Sign Up</Link></li> */}
                          <li className="btn-signup"><Link to="/signup" >Sign Up</Link></li>
                          
                        </ul>
                      </div>
                    </div>
                  </nav>
                </div>
              </div>
            </div>
          </div>
          <Modal id="modal-conatct" className="modal fade modal-custom  modal-contact" role="dialog" show={this.state.renderContactUsModal} onHide={this.handleCloseContanchUsModal}>
            <div className="modal-header">
              <h4 className=""></h4>
              <button type="button" className="close" data-dismiss="modal" onClick={this.handleCloseContanchUsModal}>&times;</button>
            </div>
            <div className="modal-body">
              <div className="modal-logo">
                <a href="#">
                  <img src={logoBlack} alt="logo" />
                </a>
              </div>
              <div className="modal-heading dotted-gradient">
                <h3>Contact Us</h3>
              </div>
              <div className="Contact-form">
                <div className="row">
                 

                  <div className="col-lg-12">
                    <div className="form-group">
                      <span className="has-float-label">
                        <div className="select2-div">
                          <Select
                            value={this.state.accountType}
                            onChange={this.handleAccountType}
                            options={options}
                            className="select-multiple"
                          />
                          {/* <select className="select-multiple">
													<option>I can’t acces my account</option>
													<option>I can't link my phone number</option>
												</select> */}
                        </div>
                        <label htmlFor="second">Topic</label>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Modal>
          <LoginModal display={this.state.renderLoginModal} hide={this.handleCloseLoginModal} />
        </div>
      </header>
    );
  }
}


const mapStateToProps = state => ({
  renderLogin: state.accountDetails.renderLogin,
});

const mapDispatchToProps = dispatch => ({
  renderLoginModal: renderLogin => dispatch(renderLoginModal(renderLogin)),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(TopMenu),
);