import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import profile from '../../assets/images/User.png';

import { userAccountUpdate, getProfileAvatar } from '../../api/userAccount';
class ProfileInfo extends Component {
  constructor(props){
    super(props);
    this.state = {
      userData: JSON.parse(sessionStorage.getItem('user')),
      profileAvatar: [],
			profileAvatarUrl: profile,
    };
  }

  componentDidMount() {
    this.getProfileImages();
  }

  componentDidUpdate(prevProps) {
    const { account } = this.props;
    if (prevProps.account !== account) {
      this.getProfileImages();
    }
  }

  getProfileImages() {
    getProfileAvatar().then((result) => {
      if (result) {
        if (result.code === 200 && result.status === true) {
          this.setState({ profileAvatar: result.data, userData: JSON.parse(sessionStorage.getItem('user'))}, () => {
					const { profileAvatar, userData } = this.state;
						profileAvatar.map((data) => {
							if (data.id === userData.profile_avtar) {
								this.setState({ profileAvatarUrl: data.profile_avtar})
							}
						})
					});
        }
      }
    })
  }

  render() {
    const { userData, profileAvatarUrl } = this.state; 
    return (
      <div className="profile-section">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="heading-comedy">
              <div className="img-circle">
                  <img src={profileAvatarUrl} alt="theater" />
              </div>
              <h3> {userData.first_name} {userData.last_name}</h3>
              
            </div>
            <div className="content-comedy">
              <p>Let’s make laugh everyone</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    );
  }
}

// const mapDispatchToProps = dispatch => ({
// 	isAccoutUpdated: account => dispatch(isAccoutUpdated(account))
// });

const mapStateToProps = state => ({
	account: state.dashboard.account,
});


export default withRouter(
  connect(
    mapStateToProps,
		// mapDispatchToProps
    null
    )(ProfileInfo),
);
