import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import TopMenu from '../../components/TopMenu';
import Footer from '../../components/Footer';
import { ToastContainer } from 'react-toastify';
class Thanks extends Component {
  render() {
    return (
      <div className="wrapper">
	  	  <ToastContainer autoClose={2000} />
      <TopMenu />
      <div className="middle-container">
		<section className="thanks-secttion">
			<div className="container">
				<div className="row justify-content-center">
					<div className="col-lg-8">
							<div className="thanks-div">
									<div className="thanks-heading">
											<h2>Thanks</h2>
											<p>Thanks we have received your message and will get back to you soon.</p>
									</div>
				
									<div className="btn-go">
										<Link to="/" className="btn btn-blue-light">Go to home</Link>
									</div>
								</div>
					</div>
				</div>				
			</div>
		</section>
	</div>
      <Footer/>

</div>
    );
  }
}

export default Thanks;