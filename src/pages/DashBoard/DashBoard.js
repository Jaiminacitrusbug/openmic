import React, { Component } from 'react';
import HeaderDashboard from '../../components/HeaderDashboard/index';
import ProfileInfo from '../../components/HeaderDashboard/profileInfo'; 
import DashBoardMain from '../../components/DashBoardMain/index';
import Footer from '../../components/Footer';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
class Dashboard extends Component {
  render() {
    return (
      <div className="wrapper">
        <HeaderDashboard />
        <ToastContainer/>
        <div className="middle-container min-height">
          <ProfileInfo/>
          <DashBoardMain />
        </div>
        <Footer />
      </div>
    );
  }
}

export default Dashboard;