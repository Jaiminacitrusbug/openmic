import React, { Component } from 'react';
import TopMenu from '../../components/TopMenu';
import SetNewPassword from '../../components/SetNewPasswrod/SetNewPassword';
import Footer from '../../components/Footer';


class setPassword extends Component {
  render() {
    return (
    	<div className="wrapper">
				<TopMenu />
        <SetNewPassword/>
    		<Footer/>
    	</div>
    );
  }
}

export default setPassword;