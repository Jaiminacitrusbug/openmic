import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { setStepCount } from '../../../actions/postMicFormActions';
import { postNewMic, postNewMicNewUser } from '../../../api/getMicApi';
import moment from 'moment';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import history from '../../../services/BrowserHistory';
import { Modal } from 'react-bootstrap';
import email from '../../../assets/images/email.png';
import { getMicCategoty } from '../../../api/getMicApi'; 
import defaultMic from '../../../assets/images/mic.png'
class MicPreview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imgPreview: '',
      nameOfMic: '',
      address: '',
      city: '',
      userData: JSON.parse(sessionStorage.getItem('user')),
      renderSendMailModal: false,
      category: [],
      loading: false,
      loadingDraft: false,
    };
    this.handlePrevious = this.handlePrevious.bind(this);
    this.handleSaveInDraft = this.handleSaveInDraft.bind(this);
    this.handlePublish = this.handlePublish.bind(this);
    this.handleRenderSendMail = this.handleRenderSendMail.bind(this);
    this.handleSaveInDraftNewUser = this.handleSaveInDraftNewUser.bind(this);
  }

  componentDidMount() {
    const { additionalData, micData } = this.props;
    this.setState({ 
      imgPreview: additionalData.imagePreviewUrl,
      nameOfMic: micData.nameOfMic,
      address: micData.address,
      city: micData.venue,
    })
    this.getCategory();
  }

  getCategory() {
    getMicCategoty().then((result) => {
      if (result && result.code === 200 && result.status === true) {
       let categoryOption = [];
       result.data.map((item) => {
        categoryOption.push({
          value: item.id,
          label: item.category,
        });
       });
       this.setState({ category: categoryOption });
      }
    })
  }

  componentDidUpdate(prevProps) {
    const { additionalData, accountDetails, selectedDate, describeData, costType, micData } = this.props;
    if(prevProps.additionalData !== additionalData) {
      this.setState({ 
        imgPreview: additionalData.imagePreviewUrl,
      })
    }
    if(prevProps.micData !== micData) {
      this.setState({ 
        nameOfMic: micData.nameOfMic,
        address: micData.address,
        city: micData.venue,
      })
    }
    if (prevProps.selectedDate !== selectedDate) {

    }

    if(prevProps.describeData !== describeData) {

    }

    if(prevProps.costType !== costType) {

    }
    
  }
  
  handlePrevious() {
    const count = 5;
    this.props.setStepCount(count);
  }

  handleSaveInDraftNewUser() {
    const { accountDetails, additionalData, costType, describeData, selectedDate, micData } = this.props
    var formData = new FormData();
    formData.append('mic_name', micData.nameOfMic);
    formData.append('producer', '');
    formData.append('producer_email', accountDetails.email);
    formData.append('mic_venue', micData.venue);
    formData.append('address', micData.address);
    formData.append('region', micData.region.id);
  //  formData.append('capacity', micData.capacity);
    formData.append('mic_datetime', '');
    formData.append('mic_status', 'SAVED');
    formData.append('about_mic', micData.aboutShow);
    formData.append('mic_cost_type', 'PAID');
    formData.append('mic_cost',costType.costAmount === null || costType.costAmount === '' || costType.costAmount === undefined? 0 : costType.costAmount );
    formData.append('open_to_all', describeData.reason === 'Yes'? true: false);
    formData.append('featured', 'False');
    formData.append('mic_entry_type', 1);
    formData.append('mic_fees', costType.drink.value);
   // formData.append('timeslot', costType.timeSlot.value);
    formData.append('poster_image', additionalData.file !== undefined ? additionalData.file:'');
    formData.append('category', costType.selectedCategory === true? this.state.category[0].value : '');
    formData.append('additional_description', additionalData.Description !== undefined ? additionalData.Description: '');
    formData.append('mic_time',moment(selectedDate.newtime[0]).format("HH:mm:ss"));
    formData.append('mic_day', selectedDate.day.value);
    this.setState({ loadingDraft: true });
    postNewMicNewUser(formData).then((result) => {
      if (result) {
        if (result.code === 200 && result.status === false) {
          toast.info(result.message);
          this.setState({ renderSendMailModal: true });
          this.setState({ loadingDraft: false });
        }
      } else {
        toast.error(result.message);
        this.setState({ loadingDraft: false });
      }
    })
  }


  handleSaveInDraft() {
    const { accountDetails, additionalData, costType, describeData, selectedDate, micData } = this.props
    var formData = new FormData();
    formData.append('mic_name', micData.nameOfMic);
    formData.append('producer', this.state.userData.id);
    formData.append('mic_venue', micData.venue);
    formData.append('address', micData.address);
    formData.append('region', micData.region.id);
    formData.append('capacity', micData.capacity);
    formData.append('mic_datetime', '');
    formData.append('mic_status', 'SAVED');
    formData.append('about_mic', micData.aboutShow);
    formData.append('mic_cost_type', 'PAID');
    formData.append('mic_cost',costType.costAmount === null || costType.costAmount === '' || costType.costAmount === undefined? 0 : costType.costAmount );
    formData.append('open_to_all', describeData.reason === 'Yes'? true: false);
    formData.append('featured', 'False');
    formData.append('mic_entry_type', 1);
    formData.append('mic_fees', costType.drink.value);
   // formData.append('timeslot', costType.timeSlot.value);
    formData.append('poster_image',additionalData.file !== undefined ? additionalData.file:'');
    formData.append('additional_description', additionalData.Description !== undefined ? additionalData.Description: '');
    formData.append('category', costType.selectedCategory === true? this.state.category[0].value : '');
    formData.append('mic_time',moment(selectedDate.newtime[0]).format("HH:mm:ss"));
    formData.append('mic_day', selectedDate.day.value);
    this.setState({ loadingDraft: true });
    postNewMic(formData).then((result) => {
      if (result) {
        if (result.code === 201 && result.status === true) {
          toast.info(result.message);
          setTimeout(() => {
            history.push('/Dashboard');
            this.setState({ loadingDraft: false });
          }, 1000);
        }  else {
          toast.error(result.message);
          this.setState({ loadingDraft: false });
        }
      }
    })
  }

  handlePublish() {
    const { accountDetails, additionalData, costType, describeData, selectedDate, micData } = this.props
    var formData = new FormData();
    formData.append('mic_name', micData.nameOfMic);
    formData.append('producer', this.state.userData.id);
    formData.append('mic_venue', micData.venue);
    formData.append('address', micData.address);
    formData.append('region', micData.region.id);
    //formData.append('capacity', micData.capacity);
    formData.append('mic_datetime','');
    formData.append('mic_status', 'UNDER_REVIEW');
    formData.append('about_mic', micData.aboutShow);
    formData.append('mic_cost_type', 'PAID');
    formData.append('mic_cost',costType.costAmount === null || costType.costAmount === '' || costType.costAmount === undefined? 0 : costType.costAmount );
    formData.append('open_to_all', describeData.reason === 'Yes'? true: false);
    formData.append('featured', 'False');
    formData.append('mic_entry_type', 1);
    formData.append('mic_fees', costType.drink.value);
    
    formData.append('poster_image', additionalData.file !== undefined ? additionalData.file:'');
    formData.append('additional_description', additionalData.Description !== undefined ? additionalData.Description: '');
    formData.append('category', costType.selectedCategory === true? this.state.category[0].value : '');
    //console.log("time data with form ", moment(selectedDate.newtime[0]).format("HH:mm:ss A"))
    formData.append('mic_time', moment(selectedDate.newtime[0]).format("HH:mm:ss A"));
   // formData.append('mic_time',selectedDate.newtime[0].value);
  
    this.setState({ loading: true });
    postNewMic(formData).then((result) => {
      if (result) {
        if (result.code === 201 && result.status === true) {
          toast.info(result.message);
          setTimeout(() => {
            history.push('/Dashboard');
           // toast.success(result.message);
            this.setState({ loading: false });
          }, 1000);
        }  else {
          toast.error(result.message);
          this.setState({ loading: false });
        }
      }
    })
  }

  handleRenderSendMail() {
    this.setState({ renderSendMailModal: false });
  }

  render() {
    const { imgPreview, nameOfMic, address, city } = this.state;
    return (
      <fieldset>
        <ToastContainer/>
        <div className="row">
          <div className="col-lg-12">
            <div className="step-title">
            {this.props.authenticated === false?
                 <h4>Step 7 of 7</h4>
                :
                <h4>Step 6 of 6</h4>
              }
              <p>Review Your Mic Details </p>
            </div>
          </div>
          <div className="col-lg-3 col-md-3">
            <div className="review-details">
              <h4>Uploaded Poster</h4>
              <div className="img-upload">
              <img src={imgPreview !== undefined? imgPreview : defaultMic} alt="" />
              </div>
              
            </div>
          </div>

          <div className="col-lg-9 col-md-9">
            <ul className="post-review">
              <li><span>Name of Mic:</span>{nameOfMic}</li>
              <li><span>Address:</span>{address}</li>
              <li><span>Venue:</span>{city}</li>
            </ul>
          </div>
          </div>
          <div className="row">
          <div className="col-lg-12">
          <div className="btn-groups-steps btn-groups-steps-publish">
            <button onClick={this.handlePrevious} className="btn btn-prev">Previous</button>
            <div className="Publish-group">
            {this.props.authenticated === true?
              <button disabled={this.state.loadingDraft} onClick={this.handleSaveInDraft} className="btn btn-draft mobile">Save in draft{this.state.loadingDraft === true ?<div className="spinner-border" role="status">
              <span className="sr-only">Loading...</span>
            </div> : null }</button>
              :
              <button disabled={this.state.loadingDraft} onClick={this.handleSaveInDraftNewUser} className="btn btn-draft mobile">Save in draft  {this.state.loadingDraft === true ?<div className="spinner-border" role="status">
              <span className="sr-only">Loading...</span>
            </div> : null }</button>
            } 
              {this.props.authenticated === true?
                <button disabled={this.state.loading} onClick={this.handlePublish} className="btn btn-next mobile">Publish
                 {this.state.loading === true ?<div className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
              </div> : null }
              </button>
              : null }
            </div>
           
            </div>
          </div>
        </div>
       
        <Modal className="modal fade modal-custom" show={this.state.renderSendMailModal} hide={this.handleRenderSendMail}>
          <div className="modal-header">
            <h4 className=""></h4>
            <button type="button" className="close" data-dismiss="modal" onClick={this.handleRenderSendMail}>&times;</button>
          </div>
          <div className="modal-body">
            <div className="modal-logo">
              <a href="#">
                <img src={email} alt="logo" />
              </a>
            </div>
            <div className="row">
              <div className="col-lg-12">
                <div className="resend-mail">
                  <p>Welcome in openmiclist.</p>
                  <p>Congratulation your account is successfully created.</p>
                  <p>You have been received an <span style={{ color: '#000000', fontWeight: '600' }}> email for account activation.</span></p>
                  <p>We have put your <span style={{ color: '#000000', fontWeight: '600' }}>POST in draft,</span>  Once you activate your account then you can log in and publish your mic.</p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12 text-center text-top">
                <div className="btn-groups">
                  <button type="submit" className="btn btn-login" onClick={this.handleEmail}>Resend Mail</button>
                </div>
              </div>
              <div className="col-lg-12 text-center text-top">
                <div className="login-form account-text">
                  <p><a href="/">Go to home</a></p>
                </div>
              </div>
            </div>
          </div>
          <div className="modal-footer" style={{ justifyContent: 'center' }}>
            <div className="row">
              <div className="col-lg-12 text-center">
                {/* <div className="resend-mail"> */}
                <div className="account-text text-center">
                  <p>If you never received an email from us, there’s a few places the email may have landed:<br />
                    Please check your Spam, junk, and bulk mail folders: Sometimes our emails are blocked or filtered as<br /> spam by certain email providers or internet service providers (ISPs). You may also click the Resend Mail button above.</p>
                </div>
                {/* </div> */}
              </div>
            </div>
          </div>
        </Modal>
      </fieldset>
    );
  }
}

 const mapDispatchToProps = dispatch => ({
   setStepCount: count => dispatch(setStepCount(count)),
//   addAddtionalInfo: additionalData => dispatch(addAddtionalInfo(additionalData)),
 });
const mapStateToProps = state => ({
  count: state.accountDetails.count,
  additionalData: state.accountDetails.additionalData,
  accountDetails: state.accountDetails.accountDetails,
  selectedDate: state.accountDetails.selectedDate,
  describeData: state.accountDetails.describeData,
  costType: state.accountDetails.costType,
  micData: state.accountDetails.micData,
  authenticated: state.authUser.authenticated,
});
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(MicPreview),
);
