webpackHotUpdate("main",{

/***/ "./src/api/comedyFestivalApi.js":
/*!**************************************!*\
  !*** ./src/api/comedyFestivalApi.js ***!
  \**************************************/
/*! exports provided: getComedyFestivals, addComedyFestivals */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getComedyFestivals", function() { return getComedyFestivals; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addComedyFestivals", function() { return addComedyFestivals; });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _errorHandling__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./errorHandling */ "./src/api/errorHandling.js");
/* harmony import */ var _constants_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../constants/config */ "./src/constants/config.js");



function getComedyFestivals() {
  try {
    return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(_constants_config__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"].API_URL + 'comedy-festivals', {
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => {
      return response.data;
    }).catch(error => {
      return Object(_errorHandling__WEBPACK_IMPORTED_MODULE_1__["errorHandle"])(error);
    });
  } catch (err) {
    console.log('Err::: ', err);
    return err;
  }
}
function addComedyFestivals(data) {
  try {
    return axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(_constants_config__WEBPACK_IMPORTED_MODULE_2__["APP_CONFIG"].API_URL + 'comedy-festivals', data, {
      headers: {
        'Content-Type': 'application/json' // 'Content-Type' : 'multipart/form-data',

      }
    }).then(response => {
      return response.data;
    }).catch(error => {
      return Object(_errorHandling__WEBPACK_IMPORTED_MODULE_1__["errorHandle"])(error);
    });
  } catch (err) {
    console.log('Err::: ', err);
    return err;
  }
}

/***/ })

})
//# sourceMappingURL=main.09d9ff3134e9889fd977.hot-update.js.map