import React, { Component } from 'react';
import TopMenu from '../../components/TopMenu';
import EmailCofirmForm from '../../components/EmailCofirmForm/EmailCofirmForm';
import Footer from '../../components/Footer';

class emailConfirmation extends Component {
  render() {
    return (
    	<div className="wrapper">
				<TopMenu />
        <EmailCofirmForm/>
    		<Footer/>
    	</div>
    );
  }
}

export default emailConfirmation;