import { ValidatorForm } from "react-material-ui-form-validator"
import { formatPhoneNumber } from "./formaters"

export function custmFormValidation(that) {
  ValidatorForm.addValidationRule("isRequired", value => {
    // debugger;

    if (value === null || value === "" || value === undefined || value.trim() === "") {
      return false
    } else {
      return true
    }
  })
  ValidatorForm.addValidationRule("isTextInput", value => {
    
    let regex = /^[a-z0-9\d\s]+$/i

    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })

  ValidatorForm.addValidationRule("isSpecial", value => {
    
    let regex = /[0-9a-zA-Z ,-./]+$/

    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })


  ValidatorForm.addValidationRule("isAddress", value => {
    //let regex = /^\d+\s[A-z]+\s[A-z]+/
    let regex = /[0-9a-zA-Z &'#,-./]+$/

    if (!regex.test(value) && value.trim() !== "") {
      return false
    } else {
      return true
    }
  })


  ValidatorForm.addValidationRule("isMacAddress", value => {
    /*let regex = /^([0:9:F]{2}[:-]){5}([0-9A-F]{2})$/i*/
    let regex = /^[a-fA-F0-9:]{17}|[a-fA-F0-9]{12}$/
    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })
  ValidatorForm.addValidationRule("isDescription", value => {
    // let regex = /.{0,70}$/
    let regex = /[A-za-z0-9-_'.,&@:?!*()$%+=#<>~]{0,70}/
    if (value.trim() === "") {
      return false
    } else {
      return regex.test(value)
    }
  })
  ValidatorForm.addValidationRule("isMake", value => {
    let regex = /.{0,45}$/
    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })

  ValidatorForm.addValidationRule("isPort", value => {
    let regex = /^(6553[0-5]|655[0-2]\\d|65[0-4]\\d{2}|6[0-4]\\d{3}|5\\d{4}|[0-9]\\d{0,3})?$/
    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })

  ValidatorForm.addValidationRule("isIpAddress", value => {
    /*let regex = /^([0:eturn false;9:F]{2}[:-]){5}([0-9A-F]{2})$/i*/
    //let regex = /^\d{1,2}\.\d{1,2}\.\d{1,2}\.\d{1,2}$/
    if (value) {
      let regex = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
      try {
        value = value.replace(/_/gi, "")
      } catch (e) {}
      if (!regex.test(value) && value.trim() != "") {
        return false
      } else {
        return true
      }
    } else {
      return true
    }
  })

  ValidatorForm.addValidationRule("isSubnetMask", value => {
    /*let regex = /^([0:eturn false;9:F]{2}[:-]){5}([0-9A-F]{2})$/i*/
    //let regex = /^\d{1,2}\.\d{1,2}\.\d{1,2}\.\d{1,2}$/
    let regex = /^(((255\.){3}(255|254|252|248|240|224|192|128|0+))|((255\.){2}(255|254|252|248|240|224|192|128|0+)\.0)|((255\.)(255|254|252|248|240|224|192|128|0+)(\.0+){2})|((255|254|252|248|240|224|192|128|0+)(\.0+){3}))$/
    try {
      value = value.replace(/_/gi, "")
    } catch (e) {}
    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })

  ValidatorForm.addValidationRule("isDate", value => {
    let regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/

    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })
  ValidatorForm.addValidationRule("isNumeric", value => {
    let regex = /^[0-9]*$/

    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })

  ValidatorForm.addValidationRule("isValidAmount", value => {
    let regex = /^\s*-?[1-9]\d*(\.\d{1,2})?\s*$/
    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })

  ValidatorForm.addValidationRule("isString", value => {
    //let regex =/^[\\p{L} .'-]+$/;
    let regex = /^[A-Za-z\s]+$/
    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })


  ValidatorForm.addValidationRule("isWebsite", value => {

    let regex= /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/

    //Matches Case
    // https://www.google.com
    // http://www.google.com
    // http://www.google.in
    // www.google.com

    if (value) {
      if (!regex.test(value)) {
        return false
      } else {
        return true
      }
    } else {
      return true
    }

    // if (regex.test(value)) {
    // 	return true;
    // }
    // else {
    // 	return false;
    // }
  })


  export function formatPhoneNumber(phone) {
    //normalize string and remove all unnecessary characters
    if (phone) {
      phone = phone.replace(/[^\d]/g, "")
      return phone.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3")
    }
    return null
  }
  
  ValidatorForm.addValidationRule("isPhone", value => {

    let regex = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/
    //Matches Case
    // 123-456-7890
    // (123) 456-7890
    // 123 456 7890
    // 123.456.7890
    // +91 (123) 456-7890
    value = formatPhoneNumber(value)
    if (value) {
      if (!regex.test(value)) {
        return false
      } else {
        return true
      }
    } else {
      return true
    }

    // if (regex.test(value)) {
    // 	return true;
    // }
    // else {
    // 	return false;
    // }
  })
  ValidatorForm.addValidationRule("isContact", value => {

    let regex = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/
    if (value) {
      if (!regex.test(value) && value.trim() != "") {
        return false
      } else {
        return true
      }
    } else {
      return true
    }
  })

  /* ValidatorForm.addValidationRule('isFax', (value) => {
		let regex = /^\+?[0-9]{6,}$/
		if (!regex.test(value) && value.trim() != '') {
			return false;
		}
		else {
			return true;
		}


	}); */
  ValidatorForm.addValidationRule("isZip", value => {
    //regex = /\d{0,9}/
    let regex = /^(?=.*\d.*)[A-Za-z0-9]{3,10}$/
    if (value) {
    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  } else {
    return true
  }
  })
  ValidatorForm.addValidationRule("isLatt", value => {
    //let regex = /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/
    let test = parseFloat(value)
    let highest = parseFloat(90)
    let lowest = parseFloat(-90)
    if (test <= highest && test >= lowest) {
      return true
    } else {
      return false
    }
  })
  ValidatorForm.addValidationRule("isLong", value => {
    //let regex = /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/
    //	let regex = /^((-)?(([1-9]?[0-9]{1}|[1-9]?[0-9]{1}(\\.){1}|[1-9]?[0-9]{1}(\\.){1}[0-9]{1,7}|1[0-7]{1}[0-9]{1}|1[0-7]{1}[0-9]{1}(\\.){1}|1[0-7]{1}[0-9]{1}(\\.){1}[0-9]{1,7}|180|180(\\.){1}|180(\\.){1}(0){1,7}))?)?$/
    let test = parseFloat(value)
    let highest = parseFloat(180)
    let lowest = parseFloat(-180)
    if (test <= highest && test >= lowest) {
      return true
    } else {
      return false
    }
  })
  ValidatorForm.addValidationRule("isElevation", value => {
    let regex = /^([0-9]|,){0,4}(\\.[0-9]{0,2})?$/
    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })
  ValidatorForm.addValidationRule("isRange", value => {
    let regex = /^([0-9]|,){0,4}(\\.[0-9]{0,2})?$/
    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })
  ValidatorForm.addValidationRule("isPower", value => {
    let regex = /^[0-9\\-]{0,9}$/
    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })
  ValidatorForm.addValidationRule("isFadeMargin", value => {
    let regex = /^[0-9\\-]{0,9}$/
    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })
  ValidatorForm.addValidationRule("isDegree", value => {
    let regex = /^(?:36[0]|3[0-5][0-9]|[12][0-9][0-9]|[1-9]?[0-9])?$/
    // if (!regex.test(value) && value.trim() != "") {
    if (!regex.test(value)) {
      return false
    } else {
      return true
    }
  })
  ValidatorForm.addValidationRule("isPrice", value => {
    let regex = /^(\d*([.,](?=\d{3}))?\d+)+((?!\2)[.,]\d\d)?$/

    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })

  ValidatorForm.addValidationRule("isMI", value => {
 
    if (value) {
      let regex = /^[a-zA-Z]$/
      if (!regex.test(value)) {
        return false
      } else {
        return true
      }
    } else {
      return true
    }
  })
  ValidatorForm.addValidationRule("isSSS", value => {
    let regex = /^\d{3}-?\d{2}-?\d{4}$|^XXX-XX-XXXX$/
    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })
  ValidatorForm.addValidationRule("isOrg", value => {

    let regex = /^[.@&]?[a-zA-Z0-9 ]+[ !.@&()]?[ a-zA-Z0-9!()]+/
    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })
  ValidatorForm.addValidationRule("isEmailVal", value => {
    return isEmailValFunction(value, true)
  })


  ValidatorForm.addValidationRule("isAlphanumeric", value => {

    let regex = /^([a-zA-Z0-9 -]){0,45}$/
    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })
  ValidatorForm.addValidationRule("isAlphabet", value => {

    let regex = /^([a-zA-Z-]){0,45}$/
    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })

  ValidatorForm.addValidationRule("isInteger", value => {
    let regex = /^([0-9]){0,45}$/
    if (!regex.test(value) /* && value.trim() != '' */) {
      return false
    } else {
      return true
    }
  })
  ValidatorForm.addValidationRule("isStrongPassword", value => {
    //let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,30})/

    let regex = /(?=^.{8,30}$)((?=.*\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*/

    if (!regex.test(value)) {
      return false
    } else {
      return true
    }
  })
  ValidatorForm.addValidationRule("isDomain", value => {
    let regex = /^[a-zA-Z0-9][-a-zA-Z0-9]+[a-zA-Z0-9].[a-z]{2,3}(.[a-z]{2,3})?(.[a-z]{2,3})?$/

    if (!regex.test(value)) {
      return false
    } else {
      return true
    }
  })
  ValidatorForm.addValidationRule("isAlias", value => {

    let regex = /^[a-zA-Z0-9 ]{2,30}$/

    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })

  ValidatorForm.addValidationRule("isUserVal", value => {

    let regex = /[a-zA-Z0-9 ]{2,30}$/

    if (!regex.test(value)) {
      return false
    } else {
      return true
    }
  })

  ValidatorForm.addValidationRule("isSerial", value => {
    //let regex = /^([0-9]){0,14}$/
    let regex = /^[a-zA-Z0-9]*$/
    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })

  ValidatorForm.addValidationRule("isIMSI", value => {
    //let regex = /^([0-9]){0,14}$/
    let regex = /^[0-9]{0,15}$/
    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })

  ValidatorForm.addValidationRule("isServiceType", value => {
    let regex = /^([a-zA-Z0-9 ]){0,80}$/
    if (!regex.test(value) && value.trim() != "") {
      return false
    } else {
      return true
    }
  })
  ValidatorForm.addValidationRule("isLink", value => {
    let regex = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi
    if (!regex.test(value) && value != "") {
      return false
    } else {
      return true
    }
  })

  ValidatorForm.addValidationRule("isMacAddressOne", value => {
    if (value.length === 17 || value.length === 0) {
      return true
    } else {
      return false
    }
  })

  ValidatorForm.addValidationRule("isDecimal", value => {
    let regex = /^\d+(\.\d{1,2})?$/
    return regex.test(value)
  })

  ValidatorForm.addValidationRule("isPhoneNumber", value => {
    let regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s]?[0-9]{3}[-\s]?[0-9]{4}$/
    if (value === null || value === "" || value === undefined) {
      return true
    } else {
      return regex.test(value)
    }
  })
  ValidatorForm.addValidationRule("isNotZero", value => {
    let regex = /^[1-9]*$/
    if (parseInt(value)) {
      return true
    } else {
      return false
    }
  })

  ValidatorForm.addValidationRule("isValidCreditCard", value => {
    let storedCreditCard = /^[0-9][x]{11,11}[0-9]{4,4}$/
    let visaPattern = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/
    let mastPattern = /^(?:5[1-5][0-9]{14})$/
    let amexPattern = /^(?:3[47][0-9]{13})$/
    let discPattern = /^(?:6(?:011|5[0-9][0-9])[0-9]{12})$/
    if (value !== "" && storedCreditCard.test(value)) {
      return true
    } else {
      let isVisa = visaPattern.test(value) === true
      let isMast = mastPattern.test(value) === true
      let isAmex = amexPattern.test(value) === true
      let isDisc = discPattern.test(value) === true
      if (value !== "" && (isVisa || isMast || isAmex || isDisc)) {
        return true
      } else {
        false
      }
    }
  })

  ValidatorForm.addValidationRule("ValidateCardExpiryMonth", value => {
    if (value !== "") {
      return parseInt(value) >= new Date().getMonth() + 1 ? true : false
    } else {
      return false
    }
  })

  ValidatorForm.addValidationRule("CommaSeperatedEmail", value => {
    if (!value) {
      return true
    }

    for (const emailAddress of value.split(",")) {
      if (!isEmailValFunction(emailAddress, false)) {
        return false
      }
    }

    return true
  })

  ValidatorForm.addValidationRule("CommaSeperatedEmailTrim", value => {
    if (!value) {
      return true
    }

    for (const emailAddress of value.split(",").map(x => x.trim())) {
      if (!isEmailValFunction(emailAddress, false)) {
        return false
      }
    }

    return true
  })

  ValidatorForm.addValidationRule("routing_number", value => {
    let sum = 0
    if (value.length < 9) {
      return false
    } else {
      let i
      for (i = 0; i < value.length; i += 3) {
        sum +=
          parseInt(value.charAt(i), 10) * 3 +
          parseInt(value.charAt(i + 1), 10) * 7 +
          parseInt(value.charAt(i + 2), 10)
      }
      if (sum != 0 && sum % 10 == 0) {
        return true
      } else {
        false
      }
    }
  })
}

const isEmailValFunction = (value, canInputBeFalsy = false) => {
  const EMAIL_ADDRESS_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,4}))$/

  if (canInputBeFalsy) {
    if (!value) {
      return true
    }
  }

  return EMAIL_ADDRESS_REGEX.test(value)
}
