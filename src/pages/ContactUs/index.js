import React, { Component } from 'react';
import TopMenu from '../../components/TopMenu';
import ContactAdmin from '../../components/ContactUs';
import Footer from '../../components/Footer';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


class ContactUs extends Component {
  render() {
    return (
      <div className="wrapper">
      <TopMenu/>
      <ToastContainer/>
      <ContactAdmin/>
      <Footer/>	
      </div>
    );
  }
}

export default ContactUs;