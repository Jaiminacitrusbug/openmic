import * as types from './actionTypes';

export const getpostMicData = (postMicData) => ({
  type: types.GET_MIC_DATA,
  payload: postMicData,
});

export const getSelectedMic = (micData) => ({
  type: types.SET_SELECTED_MIC,
  payload: micData,
})

export const getRegion = (regionData) => ({
  type: types.GET_REGION,
  payload: regionData,
});

export const getFees = (feesData) => ({
  type: types.GET_FEES,
  payload: feesData,
});



