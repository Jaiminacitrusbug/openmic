import React, { Component } from 'react';
import { getComedyFestivals, addComedyFestivals } from '../../api/comedyFestivalApi';
import { ToastContainer, toast } from 'react-toastify';
import { Modal } from 'react-bootstrap';
import blackLogo from '../../assets/images/logo-black.svg';
import Flatpickr from 'react-flatpickr';
import moment from 'moment';
import { getRegions } from '../../api/getMicApi';
import Banner from '../../assets/images/Artboard.png'
import { getBanner } from '../../api/advertisementApi';
class ComedyFestivals extends Component {
	constructor(props) {
		super(props);
		this.state = {
			comedyFestivalData: [],
			display: false,
			loading: false,
			name: '',
			city: '',
			state: '',
			country: '',
			email: '',
			link: '',
			phone: '',
			date: '',
			errorName: '',
			errorCity: '',
			errorState: '',
			errorCountry: '',
			errorEmail: '',
			errorLink: '',
			errorPhone: '',
			errorDate: '',
			banner: undefined,
		};
	}

	componentDidMount() {
		this.getComedyFestivalsData();
		this.getRegionsData();
		this.getBannerImages();
	}

	getComedyFestivalsData = () => {
		getComedyFestivals().then((res) => {
			const code = res.code;
			const status = res.status;
			switch (code) {
				case 200:
					if (status === true) {
						let newArray = [];
						Object.keys(res.data).map((key, value) => {
							if (res.data[key].length > 0) {
								newArray.push({
									label: key,
									data: res.data[key],
								});
							}
						});
						this.setState({ comedyFestivalData: newArray });
					} else {
						toast.error(res.message)
					}
					break;
				case 400:
					if (res) {
						toast.error(res.message)
					}
					break;
				default:
					break;
			}
		})
	}

	getRegionsData = () => {
		getRegions().then((res) => {
			const code = res.code;
			const status = res.status;
			switch (code) {
				case 200:
					if (status === true) {
					} else {
						toast.error(res.message)
					}
					break;
				case 400:
					if (res) {
						toast.error(res.message)
					}
					break;
				default:
					break;
			}
		})
	}

	handleModal = () => {
		this.setState({ display: !this.state.display });
	}

	handleTextChange = (e) => {
		const { value, name } = e.target;
		this.setState({ [name]: value }, () => {
			if (this.state.name !== '') {
				this.setState({ errorName: '' });
			}
			if (this.state.city !== '') {
				this.setState({ errorCity: '' });
			}
			if (this.state.state !== '') {
				this.setState({ errorState: '' });
			}
			if (this.state.country !== '') {
				this.setState({ errorCountry: '' });
			}
			if (this.state.email !== '') {
				this.setState({ errorEmail: '' });
			}
			if (this.state.email) {
				if (!this.validate(this.state.email)) {
					this.setState({ errorEmail: 'Enter valid email' });
				} else {
					this.setState({ errorEmail: '' });
				}
			}
			if (this.state.link.length > 0) {
				if (!this.isUrlValid(this.state.link)) {
					this.setState({ errorLink: 'Enter valid link' });
				} else {
					this.setState({ errorLink: '' });
				}
			} else {
				this.setState({ errorLink: '' });
			}
			if (this.state.phone.length > 0) {
				if (!this.isValidPhoneNumber(this.state.phone)) {
					this.setState({ errorPhone: 'Enter valid phone number' });
				} else {
					this.setState({ errorPhone: '' });
				}
			} else {
				this.setState({ errorPhone: '' });
			}
		});
	}

	validate = text => {
		let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		if (reg.test(text) === false) {
			return false;
		} else {
			return true;
		}
	};

	isUrlValid = userInput => {
		var res = userInput.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
		if (res == null)
			return false;
		else
			return true;
	}

	isValidPhoneNumber = inputText => {
		var phoneNo = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		if (inputText.match(phoneNo)) {
			return true;
		}
		else {
			return false;
		}
	}

	onChangeDate = (date) => {
		this.setState({ date: moment(date[0]).format('Y-MM-DD'), errorDate: ''})
	}

	handleSavePost = () => {
		const { name, city, state, country, email, date, link, phone, errorEmail, errorLink, errorPhone } = this.state;
		if (name.trim() === '') {
			this.setState({ errorName: 'Please enter name' });
			return;
		}
		if (city.trim() === '') {
			this.setState({ errorCity: 'Please enter city' });
			return;
		}
		if (state.trim() === '') {
			this.setState({ errorState: 'Please enter state' });
			return;
		}
		if (country.trim() === '') {
			this.setState({ errorCountry: 'Please enter country' });
			return;
		}
		if (email.trim() === '') {
			this.setState({ errorEmail: 'Please enter email' });
			return;
		}
		if(link.trim() === '') {
			this.setState({ errorLink: 'Please enter link' });
			return;
		}
		if (date === '') {
			this.setState({ errorDate: 'Please select date' });
			return;
		}
		if (errorEmail !== '' || errorLink !== '' || errorPhone !== '') {
			return;
		}
		const festivalData = {
			name: name,
			city: city,
			state: state,
			country: country,
			email: email,
			link: link,
			phone: phone,
			month_year: date,
			status: "UNDER_REVIEW"
		}
		this.setState({ loading: true});
		addComedyFestivals(JSON.stringify(festivalData)).then((res) => {
			if (res) {
				const code = res.code;
				const status = res.status;
				switch (code) {
					case 201:
						if (status === true) {
							toast.success(res.message);
							this.setState({ loading: false});
							// this.getComedyFestivalsData();
							this.handleModal();
						} else {
							toast.error(res.message);
						}
						break;
					case 400:
						this.setState({ loading: false});
						toast.error(res.message);
						break;
					default:
						break;
				}
			}
		})
	}

	getBannerImages = () => {
		getBanner('main').then((res) => {
			if (res.code === 200 && res.status === true) {
				this.setState({banner: res.data});
			} else if (res.code === 200 && res.status === false) {
	
			} else if (res.code === 400) {
	
			}
		})
	}

	render() {
		const { banner } = this.state;
		return (
			<div className="container">
				<ToastContainer />
				<div className="middle-container">
					<div className="min-hight">
					<section className="banner-section">
			<a href={banner!== undefined && banner.link} target="_blank">
				<img src={banner!== undefined && banner.image !== ''? banner.image : Banner} style={{width: '100%', height:'100%'}} />
				</a>
			</section>
						<div className="add-comedy-btn">
							<div className="btn-group-comedy">
								<div className="btn-view-othersday">
									<a onClick={this.handleModal} className="btn btn-primary btn-blue">Add new festival </a>
								</div>
							</div>
						</div>
						<div className="table-responsive">

							<table className="table table-striped">
								<thead className="thead-dark">
									<tr>
										<th className="table-th">Name</th>
										<th className="table-th">City</th>
										<th className="table-th">State</th>
										<th className="table-th">Country</th>
										<th className="table-th">Contact</th>
										<th className="table-th">Link</th>
										<th className="table-th">Phone</th>
									</tr>
								</thead>
								{this.state.comedyFestivalData.length > 0 ?
									this.state.comedyFestivalData.map((data, index) => {
										return <tbody key={index}><tr>
											<td className="table-td-month">{data.label}</td>
										</tr>
											{data.data.length > 0 && data.data.map((item, index) => {
												return <tr key={index}>
													<td>{item.name}</td>
													<td>{item.city}</td>
													<td>{item.state}</td>
													<td>{item.country}</td>
													<td><a href={"mailto:" + item.email}>email</a></td>
													<td><a href={item.link} target="_blank">link</a></td>
													<td>{item.phone}</td>
												</tr>
											})}
										</tbody>
									}) : <tbody>
										<td className="table-td-month">No Data Found</td>
									</tbody>}
							</table>
						</div>
					</div>

				</div>
				<Modal id="login-post-mic" className="modal fade modal-custom modal-login" role="dialog" show={this.state.display} onHide={this.handleModal}>
					<div className="modal-header">
						<h4 className=""></h4>
						<button type="button" className="close" data-dismiss="modal" onClick={this.handleModal}>&times;</button>
					</div>
					<div className="modal-body">
						<div className="modal-logo">
							<a href="#">
								<img src={blackLogo} alt="logo" />
							</a>
						</div>
						<div className="modal-heading dotted-gradient">
							<h3>Add Comedy Festival</h3>
						</div>
						<div className="post-mic-form">
							<div className="row">

								<div className="col-lg-6">
									<div className="form-group">
										<label className="has-float-label">
											<input name="name" value={this.state.name} onChange={this.handleTextChange} type="text" placeholder="Name" className="form-control" />
											<span>Name *</span>
										</label>
										<label className="error">{this.state.errorName}</label>
									</div>
								</div>

								<div className="col-lg-6">
									<div className="form-group">
										<label className="has-float-label">
											<input name="city" value={this.state.city} onChange={this.handleTextChange} type="text" placeholder="City" className="form-control" />
											<span>City *</span>
										</label>
										<label className="error">{this.state.errorCity}</label>
									</div>
								</div>

								<div className="col-lg-6">
									<div className="form-group">
										<label className="has-float-label">
											<input name="state" value={this.state.state} onChange={this.handleTextChange} type="text" placeholder="State" className="form-control" />
											<span>State *</span>
										</label>
										<label className="error">{this.state.errorState}</label>
									</div>
								</div>

								<div className="col-lg-6">
									<div className="form-group">
										<label className="has-float-label">
											<input name="country" value={this.state.country} onChange={this.handleTextChange} type="text" placeholder="Country" className="form-control" />
											<span>Country *</span>
										</label>
										<label className="error">{this.state.errorCountry}</label>
									</div>
								</div>

								<div className="col-lg-6">
									<div className="form-group">
										<label className="has-float-label">
											<input name="email" value={this.state.email} onChange={this.handleTextChange} type="text" placeholder="Email" className="form-control" />
											<span>Email *</span>
										</label>
										<label className="error">{this.state.errorEmail}</label>
									</div>
								</div>

								<div className="col-lg-6">
									<div className="form-group">
										<label className="has-float-label">
											<input name="link" value={this.state.link} onChange={this.handleTextChange} type="text" placeholder="Link" className="form-control" />
											<span>Link *</span>
										</label>
										<label className="error">{this.state.errorLink}</label>
									</div>
								</div>

								<div className="col-lg-6">
									<div className="form-group">
										<label className="has-float-label">
											<input name="phone" value={this.state.phone} onChange={this.handleTextChange} type="text" placeholder="Phone" className="form-control" />
											<span>Phone</span>
										</label>
										<label className="error">{this.state.errorPhone}</label>
									</div>
								</div>

								<div className="col-lg-6 col-md-6">
									<div className="form-group">
										<label className="has-float-label" >
											{/* <input type='text' id="datetime" className="form-control" placeholder="select date" /> */}
											<Flatpickr
												onChange={this.onChangeDate}
												options={
													{ dateFormat: "Y-m-d",
													minDate: "today",
													enable: [
												function(date) {
												// return true to enable

												return (date.getDate() < 2);

												}
												] }
												}
												placeholder="Select Date"
												className="form-control"
												value={this.state.date}
											/>
											<span>Schedule Date *</span>
										</label>
										<label className='error'>{this.state.errorDate}</label>
									</div>
								</div>

								<div className="col-lg-12 text-right">
									<div className="btn-groups">
										<button onClick={this.handleSavePost} className="btn btn-login">Save Post {this.state.loading === true ? <div className="spinner-border" role="status">
											<span className="sr-only">Loading...</span>
										</div> : null}</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</Modal>
			</div>
		);
	}
}

export default ComedyFestivals;