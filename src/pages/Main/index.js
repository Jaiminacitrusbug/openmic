import React, { Component } from 'react';
import TopMenu from '../../components/TopMenu';
import MiddleContainer from '../../components/MiddleContainer';
import ListContainer from '../../components/ListContainer';
import Footer from '../../components/Footer';
import HeaderDashboard from '../../components/HeaderDashboard/index';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Cookies from '../../components/Cookie/Cookie';

class MainPage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			userData: JSON.parse(sessionStorage.getItem('user')),
		};
		
	};

	componentDidUpdate(prevProps) {
		if (prevProps.authenticated !== this.props.authenticated) {
			if (this.props.authenticated === false) {
				this.setState({ userData: ''});
			}
		}
	}

	render() {
		return (
			<div className="wrapper">
				{(this.state.userData && this.state.userData.token != '') ? 
					<HeaderDashboard />
					: 
					<TopMenu/>
				}
				<Cookies/>
				<ToastContainer/>
				<MiddleContainer />
				<Footer/>	
	</div>
		);
	}
}

const mapStateToProps = state => ({
	authenticated: state.authUser.authenticated,
});


export default withRouter(
	connect(
		mapStateToProps,
		null
	)(MainPage),
);