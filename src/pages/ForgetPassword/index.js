import React, { Component } from 'react';
import TopMenu from '../../components/TopMenu';
import FPassword from '../../components/ForgetPassword/forgetPassword';
import Footer from '../../components/Footer';

class forgetPassword extends Component {
  render() {
    return (
    	<div className="wrapper">
				<TopMenu />
        <FPassword/>
    		<Footer/>
    	</div>
    );
  }
}

export default forgetPassword;