import * as types from './actionTypes';

export const userSignUp = (signUpData) => ({
  type: types.SET_SIGNUP_DATA,
  payload: signUpData,
});

export const userLogin = (userLoginData) => ({
  type: types.SET_LOGIN_DATA,
  payload: userLoginData,
});

export const setAuthenticationError = error => ({
  type: types.SET_AUTHENTICATION_ERROR,
  error,
});

export const setAuthenticatedStatus = status => ({
  type: types.SET_AUTHENTICATED_STATUS,
  status,
});

export const setNewUserData = userDetails => ({
  type: types.NEW_USER_DATA,
  payload:userDetails,
});

