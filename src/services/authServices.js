import { dispatch } from './Redux';
import { setAuthenticatedStatus} from '../actions/authActions';

class AuthService {
  constructor() {
  }
  
  logout = () => {
    sessionStorage.removeItem('token');
    dispatch(setAuthenticatedStatus(false));
  };
  
  checkAuthentication = () => {
    const token = sessionStorage.getItem('token');
    if (token !== null) {
      dispatch(setAuthenticatedStatus(true));
    } else {
      dispatch(setAuthenticatedStatus(false));
    }
  }
}
export default new AuthService();