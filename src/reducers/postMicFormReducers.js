import * as types from '../actions/actionTypes';

const initialState = {
  accountDetails: {},
  count: 0,
  selectedDate: [],
  describeData: {},
  costType: {},
  additionalData: {},
  micData: {},
  renderLogin: false,
};

const accountDetails = (state = initialState, action) => {
  switch (action.type) {
    case types.ADD_ACCOUNT_DETAILS:
      return {
        ...state,
        accountDetails: action.payload
      };
    case types.SET_STEP_COUNT:
      return {
        ...state,
        count: action.payload
      };
    case types.ADD_DATE_TIME:
      return {
        ...state,
        selectedDate: action.payload
      };
    case types.ADD_DESCRIBE_REQ:
      return {
        ...state,
        describeData: action.payload
      };
    case types.ADD_COST_TYPE:
      return {
        ...state,
        costType: action.payload
      };
    case types.ADD_ADDITIONAL_INFO:
      return {
        ...state,
        additionalData: action.payload
      };
    case types.ADD_MIC_DETAILS:
    return {
      ...state,
      micData: action.payload
    };
    case types.RENDER_LOGIN_MODAL:
      return {
        ...state,
        renderLogin: action.payload
      };
      default:
      return state;
  }
};

export default accountDetails;