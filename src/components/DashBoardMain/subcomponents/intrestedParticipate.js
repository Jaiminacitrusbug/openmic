import React, { Component } from 'react';
//import profile from '../../../assets/images/profile.svg';
import moment from 'moment';
import {Modal} from 'react-bootstrap';
import { getParticipant, getParticipantDetails } from '../../../api/participantAPI';
import blackLogo from '../../../assets/images/Size/Logoforwhite2.png';
import contact from '../../../assets/images/icon/contact.png';
import contactHover from '../../../assets/images/icon/contact-hover.png';
import email from '../../../assets/images/icon/email.png';
import emailHover from '../../../assets/images/icon/email-hover.png';
//import internet from '../../../assets/images/icon/internet.png';
//import internetHover from '../../../assets/images/icon/internet-hover.png';
import ReactPaginate from 'react-paginate';


class IntrestedParticipate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      participant: [],
      renderModal: false,
      userData: JSON.parse(sessionStorage.getItem('user')),
      participantId: '',
      participantDetails: {},
      page:1,
      totalDealsCount:1,
      pageCount:0,
    };
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handleOpenModal = this.handleOpenModal.bind(this);
  }

  componentDidMount() {
    this.getPerformer();
  }

  getPerformer() {
    getParticipant(this.state.userData.id, this.state.page).then((result) => {
      if (result) {
        if (result.code === 200 && result.status === true ) {
          let participant = [];
        result.data && result.data.map((data) => {
          participant.push({
          img: data.profile_image,
          name: data.first_name + ' ' + data.last_name,
          description: data.comment,
          date: moment(data.created_at).format('MM/DD/YYYY'),
          time: moment(data.created_at).format('H:m a'),
          id: data.id,
          });
        });
      this.setState({ participant, totalDealsCount: result.total_count, pageCount: (result.total_count / result.page_size) });  
      }
      }
    })
  }

  handleCloseModal() {
    this.setState({ renderModal: false });
  }

  handleOpenModal(index) {
    getParticipantDetails(index).then((result) => {
      if (result) {
        if (result.code === 200 && result.status === true ) {
          this.setState({ participantId: index, renderModal: true, participantDetails: result.data });
        }
      }
    })
  }

  renderPagination() {
    if (this.state.totalDealsCount > 8) {
      return (
        <ReactPaginate previousLabel={"previous"}
        nextLabel={"next"}
        breakLabel={<a href="javascript:void(0)">...</a>}
        breakClassName={"break-me"}
        pageCount={this.state.pageCount}
        onPageChange={this.handlePageClick}
        containerClassName={"pagination"}
        pageClassName={"page-item"}
        pageLinkClassName={"page-link"}
        previousClassName={"page-item"}
        nextClassName={"page-item"}
        previousLinkClassName={"page-link"}
        nextLinkClassName={"page-link"}
        // subContainerClassName={"pages pagination"}
        activeClassName={"active"}
        disabledClassName={"page-item disabled"}
        />
      )
    }
    else {
      return null
    }
  }

  handlePageClick = (data) => {
    let selected = data.selected;
    let offset
    if (selected !== 0) {
      offset = Math.ceil(selected * this.state.perPage);
    }
    else {
      offset = Math.ceil(1 * this.state.perPage);
    }
    this.setState({ page: selected + 1, offset: offset, loaded: false }, () => {
      this.getPerformer();
    }); 
  }

  renderParticipant(participant) {
    return <div className="row">
    <div className="col-lg-12">

    </div>
    {participant && participant.length === 0 ? <div className="grid-list col-lg-12 col-md-12 col-sm-12" style={{ margin: "10px", fontSize: "24px" }}>No Participant Found</div> : null}
    {participant.length > 0 && participant.map((data, index) => {
      return <div key={index} className="col-lg-6">
        <a href="#" onClick={() => this.handleOpenModal(data.id)} className="contact-detail-box" data-toggle="modal" data-target="#modal-intrestedto">
          <div className="contact-inner ">
            <div className="contact-profile">
              <img src={data.img} alt="" />
            </div>
            <div className="contact-text">
              <h3>{data.name}</h3>
              <div className="group-text">
                <p>{data.description}</p>
                <span className="contact-link">Contact</span>
              </div>

              <hr className="line-1" />
              <div className="group-text footer-contact">
                <p>{data.date}</p>
                <span>{data.time}</span>
              </div>
            </div>
          </div>
        </a>
      </div>
    })}
  </div>
  }


  render() {
    const { participant, participantDetails } = this.state;
    return (
      <div id="intrested-participate" className="tab-pane ">
        <div className="intrested-to">
          {this.renderParticipant(participant)}
        </div>
        <div className="container">
        <div className="pagination-main">{this.renderPagination()}</div></div>
        <Modal id="modal-intrestedto" className="modal fade modal-custom  modal-contact" role="dialog" show={this.state.renderModal} onHide={this.handleCloseModal}>
        <div className="modal-header">
						<h4 className=""></h4>
						<button onClick={this.handleCloseModal} type="button" className="close" data-dismiss="modal">&times;</button>
					</div>
					<div className="modal-body">
						<div className="modal-logo">
							<a href="#">
								<img src={blackLogo} alt="logo"/>
							</a>
						</div>
						<div className="modal-heading dotted-gradient">
							<h3>Interested participate</h3>
						</div>
						<div className="Interestedto">
							<div className="profile-photo">
								<img src={participantDetails.profile_image} alt="pic" />
							</div>
							<div className="profile-content">
								<h3>{participantDetails.first_name + ' '+ participantDetails.last_name}</h3>
								<h4 style={{whiteSpace: 'pre-line',width:'100%'}}>{participantDetails.comment}</h4>
								<hr className="line-1" />
								<div className="contact-info">
									<ul>
										<li><a href="#"><img className="img-block" src={contact} alt="pic"/> <img className="img-hover" src={contactHover} alt="pic"/>{participantDetails.contact}</a></li>

										<li><a href="mailto:susan.jockowich@gmail.com"><img className="img-block" src={email} alt="pic"/><img className="img-hover" src={emailHover} alt="pic"  
                    />{participantDetails.email}</a></li>

                    {/* <li><a href="https://susan.jockowich.net" target="_new"><img className="img-block" src={internet} alt="pic" /><img className="img-hover" src={internetHover} 
                    alt="pic" />susan.jockowich.net</a></li> */}
									</ul>
								</div>
							</div>
						
						</div>
						{/* <div className="Interestedto-content">
							<p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p>
						</div> */}
	
						</div>
    
        </Modal>
      </div>
    );
  }
}

export default IntrestedParticipate;