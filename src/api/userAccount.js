import axios from 'axios'
import {errorHandle } from './errorHandling'
import {APP_CONFIG} from '../constants/config';
//import {header}from './header';

export function userAccount(producerId) {
  try {
    return axios.get(APP_CONFIG.API_URL+'account-update/' + producerId, {
      headers:{
        'Content-Type': 'application/json',
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}

export function userAccountUpdate(producerId, producerData) {
  try {
    return axios.put(APP_CONFIG.API_URL+'account-update/' + producerId, producerData, {
      headers:{
        'Content-Type': 'application/json',
        'authorization': sessionStorage.getItem('token'),
      },
      // params: {
      //   first_name:producerData.first_name,
      //   last_name:producerData.last_name,
      //   contact:producerData.contact,
      //   profile_avtar:producerData.profile_avtar,
      // }
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}

export function getProfileAvatar() {
  try {
    return axios.get(APP_CONFIG.API_URL+'get-avtar', {
      headers:{
        'Content-Type': 'application/json',
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}


