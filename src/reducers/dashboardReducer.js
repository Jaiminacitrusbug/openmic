import * as types from '../actions/actionTypes';
const initialState = {
  tabName:'',
  micData: '',
  account: false,
};

const dashboard = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_SUGGESTED_TAB:
      return {
        ...state,
        tabName: action.payload
      };
    case types.ADD_MIC_DATA:
      return {
        ...state,
        micData: action.payload
      };
    case types.IS_ACCOUT_UPDATED:
      return {
        ...state,
        account: action.payload
      };
    default:
      return state;
    }
  }

  export default dashboard;
