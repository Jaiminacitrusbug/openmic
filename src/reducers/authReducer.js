import * as types from '../actions/actionTypes';
const initialState = {
  signUpData:'',
  userLoginData: '',
  authenticationError: '',
  authenticated: false,
  userDetails: {},
};

const authUser = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_SIGNUP_DATA:
      return {
        ...state,
        signUpData: action.payload
      };
    case types.SET_LOGIN_DATA:
      return {
        ...state,
        userLoginData: action.payload
      };
    case types.SET_AUTHENTICATION_ERROR:
    return {
      ...state,
      authenticationError: action.error,
    };
    case types.SET_AUTHENTICATED_STATUS:
    return {
      ...state,
      authenticated: action.status,
    };
    case types.NEW_USER_DATA:
    return {
      ...state,
      userDetails: action.status,
    };
    default:
      return state;
    }
  }

  export default authUser;