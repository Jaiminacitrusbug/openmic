import * as types from '../actions/actionTypes';
const initialState = {
  postMicData:'',
  regionData:'',
  feesData:'',
  micData: '',
};

const postMic = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_MIC_DATA:
      return {
        ...state,
        postMicData: action.payload
      };
    case types.GET_REGION:
      return {
        ...state,
        regionData: action.payload
      };
    case types.GET_FEES:
      return {
        ...state,
        feesData: action.payload
      };
    case types.SET_SELECTED_MIC:
      return {
        ...state,
        micData: action.payload
      };
    default:
      return state;
    }
  }

  export default postMic;