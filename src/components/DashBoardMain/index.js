import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import MyMic from './subcomponents/myMic';
import AccountSettings from './subcomponents/accountSettings';
import IntrestedParticipate from './subcomponents/intrestedParticipate';
import { getMyMic } from '../../api/getMicApi';
import { userAccount } from '../../api/userAccount';
import { getpostMicData } from '../../actions/postMicActions';
class DashBoardMain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabIndex: 0,
      userdata: JSON.parse(sessionStorage.getItem('user')),
      contact: '',
    }
  }

  componentDidMount() {

    this.getMic();
    this.getUserDetails();
  }


  getUserDetails() {
    userAccount(this.state.userdata.id).then((result) => {
      if (result) {
        if (result.code === 200 && result.status === true) {
        this.setState({ contact: result.data.contact })
        }
      }
    })
  }

  getMic() {
    const myMicId = this.state.userdata.id;
    // getMyMic(myMicId).then((result) => {
    //   let micData = [];
    //   if (result && result.code === 200 && result.status === true) {
    //     this.props.getpostMicData(result.data);
    //   } else {

    //   }
    // })
  }

  componentDidUpdate(prevProps){
    if (prevProps.tabName !== this.props.tabName) {
      if (this.props.tabName === 'accountSettings') {
         this.setState({ tabIndex: 2 });
      } else if(this.props.tabName === 'myMic') {
         this.setState({ tabIndex: 1 });
      } else if(this.props.tabName === 'intrested') {
         this.setState({ tabIndex: 3 });
      }
    }
  }
 
  handleSelect(index) {
    this.setState({ tabIndex: index });
  };



  render() {
    return (
      <section className="grid-section grid-profile">
				<div className="grid-div-header">
					<div className="container">
          <Tabs className="tabs-header" selectedIndex={this.state.tabIndex} onSelect={this.handleSelect.bind(this)}>
          <div className="row">
								<div className="col-lg-12">
									<div className="grid-div-result">
                    <TabList className="nav nav-tabs">
                      <Tab><span>Draft</span></Tab>
                      <Tab eventkey="myMic" ><span>My mics</span></Tab>
                      <Tab eventkey="accountSettings" ><span>Account settings</span></Tab>
                      <Tab eventkey="intrested" ><span>Interested to participate</span></Tab>
                    </TabList>
                  </div>
                  <div className="tab-content">
                  <TabPanel eventkey="draft">
                    <MyMic tabIndex={this.state.tabIndex}/>
                  </TabPanel>
                  <TabPanel eventkey="myMic">
                    <MyMic tabIndex={this.state.tabIndex}/>
                  </TabPanel>
                  <TabPanel eventkey="accountSettings">
                    <AccountSettings contact={this.state.contact}/>
                  </TabPanel>
                  <TabPanel eventkey="intrested">
                    <IntrestedParticipate/>
                  </TabPanel>
                 
                  
                  </div>
                </div>
            </div>
            </Tabs>
          </div>
        </div>
      </section>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  getpostMicData: data => dispatch(getpostMicData(data)),
});

const mapStateToProps = state => ({
  tabName: state.dashboard.tabName,
});
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(DashBoardMain),
);
