import React, { Component } from 'react';
import TopMenu from '../../components/TopMenu';
import Footer from '../../components/Footer';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import HeaderDashboard from '../../components/HeaderDashboard';
import PaymentComponent from '../../components/Payment';
class Payment extends Component {
  constructor(props) {
		super(props);
		this.state = {
			userData: JSON.parse(sessionStorage.getItem('user')),
		};
		
	};

	componentDidUpdate(prevProps) {
		if (prevProps.authenticated !== this.props.authenticated) {
			if (this.props.authenticated === false) {
				this.setState({ userData: ''});
			}
		}
	}
  render() {
    return (
      <div>
       {(this.state.userData && this.state.userData.token != '') ? 
					<HeaderDashboard />
					: 
					<TopMenu/>
				}
        {/* <EventDetails/> */}
		<PaymentComponent />
        <Footer/>
      </div>
    );
  }
}


const mapStateToProps = state => ({
	authenticated: state.authUser.authenticated,
});


export default withRouter(
	connect(
		mapStateToProps,
		null
	)(Payment),
);