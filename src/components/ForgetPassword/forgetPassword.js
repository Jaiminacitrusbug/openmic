import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from 'yup';
import history from '../../services/BrowserHistory';
import { forgetPassword } from '../../api/loginAPI';
import { ToastContainer, toast } from 'react-toastify';
class FPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  } 



  handleSubmit(values) {
    const emailId = {
      email: values.email
    };
    this.setState({ loader: true });
    forgetPassword(emailId).then((result) => {
      if (result) {
        if (result.code === 200 && result.status === true) {
          toast.info(result.message);
          this.setState({ loader: false });
        } else if (result.code === 200 && result.status === false) {
          toast.error(result.message);
          this.setState({ loader: false });
        }
      }
    })
  }
 
  render() {
    return (
      <div className="middle-container">
        <ToastContainer/>
        <section className="postmic-secttion main">
          <div className="container">
            <div className="post-mic-form post-mic-form-steps">

              <div className="col-lg-12">
                <div className="post-mic-heading">
                  <h3>Reset Password</h3>
                  <p>​​Forgot Your password, don't worry. Just share your email address we will share reset password link.</p>
                </div>
              </div>
              <Formik
            initialValues={{
              email: '',
            }}
            validationSchema={Yup.object().shape({
              email: Yup.string().trim().strict(false)
                .email('Email is invalid')
                .required('Email is required'),
            })}
            onSubmit={this.handleSubmit}
          >
            {(values) => (
              <Form>
                    <div className="col-lg-12">
                      <div className="form-group">
                        <label className="has-float-label">
                          <Field
                            type="text"
                            name="email"
                            placeholder="Email Address"
                            className="form-control"
                          />
                          <span>Email Address *</span>
                        </label>
                        <ErrorMessage name="email" component="span" className="error" />
                      </div>
                    </div>
                    <div className="col-lg-12 text-right">
                      <div className="btn-groups">
                        <button disabled={this.state.loader} type="submit" className="btn btn-login">Submit {this.state.loader === true ? <span className="spinner-border spinner-border-sm"></span> : null}</button>
                      </div>
                    </div>
                    </Form>
            )}
            </Formik>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default FPassword;