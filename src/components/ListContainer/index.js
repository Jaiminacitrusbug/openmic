import React, { Component } from 'react';
import GridPost from './gridPost';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { getRegions } from '../../api/getMicApi';
import { getAllMicDayANDSearch, getAllMicFilter, getAdvertisement } from '../../api/allMicAPI';
import { getAdvertise } from '../../actions/filterMicActions'; 
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ReactPaginate from 'react-paginate';


class ListContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabIndex: 0,
      weekDaysVisible: 'none',
      weekButtonTittle: 'View weekend days',
      postData: {},
      styleForList: 'btn-view gridView',
      styleforGrid: 'btn-view listView active',
      layout: 0,
      regionValue: '',
      feesValue: '',
      advertiseData: [],
      festival: this.props.match.path === '/comedyFestival'? 1: '',
      page:1,
      totalDealsCount:1,
      pageCount:1,
      regionData: {},
    };
    this.handleOtherWeek = this.handleOtherWeek.bind(this);
    this.handleGridLayout = this.handleGridLayout.bind(this);
    this.handleListLayout = this.handleListLayout.bind(this);
  }

  componentDidMount() {
    this.getRegionsOptions();
    const dayValue = {
      0:'1',
      1:'2',
      2:'3',
      3:'4',
      4:'5',
      5:'6',
      6:'7',
    };

   const day_filter = parseInt(dayValue[this.state.tabIndex]);
   const region_filter = 1;
   const fees_filter = this.state.feesValue;
   const featured_filter = false;
   const search_filter = this.props.search;
   const category_filter = this.state.festival;
   this.getADD(region_filter);
  this.getMicWithRegion(region_filter, fees_filter, featured_filter, day_filter, search_filter, category_filter, this.state.page);

  }

  componentDidUpdate(prevProps) {
    const { search, region, regionData, feesValue, featuredValue } = this.props;
      const dayValue = {
      0:'1',
      1:'2',
      2:'3',
      3:'4',
      4:'5',
      5:'6',
      6:'7',
    };
    if (prevProps.search !== search) {
      if (search.length >= 3) {
        const day_filter = parseInt(dayValue[this.state.tabIndex]);
        const region_filter = this.state.regionValue && this.state.regionValue !== ''? this.state.regionValue: 1;
        const fees_filter = this.state.feesValue;
        const featured_filter = false;
        const search_filter = this.props.search;
        const category_filter = this.state.festival;
        this.getADD(region_filter);
        this.getMicWithRegion(region_filter, fees_filter, featured_filter, day_filter, search_filter, category_filter, this.state.page);
      } else {
        const day_filter = parseInt(dayValue[this.state.tabIndex]);
        const region_filter = this.state.regionValue && this.state.regionValue !== ''? this.state.regionValue: 1;
        const fees_filter = this.state.feesValue;
        const featured_filter = false;
        const search_filter = '';
        const category_filter = this.state.festival;
        this.getADD(region_filter);
        this.getMicWithRegion(region_filter, fees_filter, featured_filter, day_filter, search_filter, category_filter, this.state.page);
      }
    }
    if (prevProps.region !== region) {
      if (regionData.length > 0) {
        var regionValue = '';
        regionData.map((data) => {
          if (data.region === region) {
            regionValue = data.id;
            this.setState({ regionValue: regionValue, regionData: data}, () => {
              // this.getADD(this.state.regionData.id);
              this.props.getAdvertise([]);
            });
          }
        })
       if (regionValue !== '') {
        const day_filter = parseInt(dayValue[this.state.tabIndex]);
        const region_filter = regionValue;
        const fees_filter = this.state.feesValue;
        const featured_filter = false;
        const search_filter = this.props.search;
        const category_filter = this.state.festival;
        this.getADD(region_filter);
        this.getMicWithRegion(region_filter, fees_filter, featured_filter, day_filter, search_filter, category_filter, this.state.page);
       }
      }
    }
    if (prevProps.feesValue !== feesValue) {
      const day_filter = parseInt(dayValue[this.state.tabIndex]);
      const region_filter = this.state.regionValue && this.state.regionValue !== ''? this.state.regionValue: 1;
      const fees_filter = feesValue.value;
      const featured_filter = false;
      const search_filter = this.props.search;
      this.setState({ feesValue: feesValue.value });
      const category_filter = this.state.festival;
      this.getADD(region_filter);
      this.getMicWithRegion(region_filter, fees_filter, featured_filter, day_filter, search_filter, category_filter, this.state.page);
    }
    if (prevProps.featuredValue !== featuredValue) {
      var featured_filter = false;
      if (featuredValue.value === "All Mic") {
        featured_filter = false;
      } else {
        featured_filter = true;
      } 
      const day_filter = parseInt(dayValue[this.state.tabIndex]);
      const region_filter = this.state.regionValue && this.state.regionValue !== ''? this.state.regionValue: 1;
      const fees_filter = this.state.feesValue;
      const search_filter = this.props.search;
      const category_filter = this.state.festival;
      this.getADD(region_filter);
      this.getMicWithRegion(region_filter, fees_filter, featured_filter, day_filter, search_filter, category_filter, this.state.page);
    }
  }

  getADD(id) {
    getAdvertisement(id).then((result) => {
      if (result) {
        if (result.code === 200 && result.status === true) {
          this.setState({ advertiseData: result.data }, () => {
            this.props.getAdvertise(result.data);
          });
        }
      }
    })
  }

  getRegionsOptions() {
      const dayValue = {
      0:'1',
      1:'2',
      2:'3',
      3:'4',
      4:'5',
      5:'6',
      6:'7',
    };
		getRegions().then((result, index) => {
		if (result && result.code === 200 && result.status === true) {
					let region = [];
			result.data.map((item, index) => {
				region.push({
					name: item.region,
					id: item.id,
					url:item.region_image_url,
				});
      });
      if (result.length > 0) {
      const day_filter = parseInt(dayValue[this.state.tabIndex]);
      const region_filter = region && region[0].id;
      const fees_filter = this.state.feesValue;
      const featured_filter = false;
      const search_filter = this.props.search;
      this.setState({ regionValue: region_filter, regionData: region }, () => {
      });
      const category_filter = this.state.festival;
      this.getADD(region_filter);
      this.getMicWithRegion(region_filter, fees_filter, featured_filter, day_filter, search_filter, category_filter, this.state.page);
      }
     // this.getMicWithRegion(region_filter, fees_filter, featured_filter, day_filter, search_filter);
  	} else {
		}
		
		})
	}

  getMicWithRegion(region_filter, fees_filter, featured_filter, day_filter, search_filter, category_filter, page){
    getAllMicFilter(region_filter, fees_filter, featured_filter, day_filter, search_filter, category_filter, page).then((result) => {
      if (result && result.code === 200 && result.status === true) {
        this.setState({ postData: result.data, totalDealsCount: result.total_count, pageCount: (result.total_count / result.page_size)});
      } else {

      }
    })
  }


  handleSelect(index) {
      const dayValue = {
      0:'1',
      1:'2',
      2:'3',
      3:'4',
      4:'5',
      5:'6',
      6:'7',
    };
    this.setState({ tabIndex: index, postData: [] }, () => {
      const day_filter = parseInt(dayValue[this.state.tabIndex]);
      const region_filter = this.state.regionValue && this.state.regionValue !== ''? this.state.regionValue: 1;
      const fees_filter = this.state.feesValue;
      const featured_filter = false;
      const search_filter = this.props.search;
      this.setState({ regionValue: region_filter });
      const category_filter = this.state.festival;
      this.getADD(region_filter);
      this.getMicWithRegion(region_filter, fees_filter, featured_filter, day_filter, search_filter, category_filter, this.state.page);
    });
  };

  handleOtherWeek() {
    const { weekDaysVisible } = this.state;
    if (weekDaysVisible === 'none') {
      this.setState({ weekDaysVisible: 'inline-block', weekButtonTittle: 'Hide weekend days' });
    } else {
      this.setState({ weekDaysVisible: 'none', weekButtonTittle: 'View  weekend days' });
    }
  }

  handleGridLayout() {
    this.setState({ styleforGrid: 'btn-view listView active', styleForList: 'btn-view gridView', layout: 0});
  }

  handleListLayout() {
    this.setState({ styleforGrid: 'btn-view listView', styleForList: 'btn-view gridView active', layout: 1});
  }

  renderPagination() {
    if (this.state.totalDealsCount > 8) {
      return (
        <ReactPaginate previousLabel={"previous"}
          nextLabel={"next"}
          breakLabel={<a href="javascript:void(0)">...</a>}
          breakClassName={"break-me"}
          pageCount={this.state.pageCount}
          onPageChange={this.handlePageClick}
          containerClassName={"pagination"}
          pageClassName={"page-item"}
          pageLinkClassName={"page-link"}
          previousClassName={"page-item"}
          nextClassName={"page-item"}
          previousLinkClassName={"page-link"}
          nextLinkClassName={"page-link"}
          // subContainerClassName={"pages pagination"}
          activeClassName={"active"}
          disabledClassName={"page-item disabled"}
          />
      )
    }
    else {
      return null
    }
  }

  handlePageClick = (data) => {
    let selected = data.selected;
    let offset
    if (selected !== 0) {
      offset = Math.ceil(selected * this.state.perPage);
    }
    else {
      offset = Math.ceil(1 * this.state.perPage);
    }
    this.setState({ page: selected + 1, offset: offset, loaded: false }, () => {
      const dayValue = {
        0:'6',
        1:'7',
        2:'1',
        3:'2',
        4:'3',
        5:'4',
        6:'5',
      };
        const day_filter = parseInt(dayValue[this.state.tabIndex]);
        const region_filter = this.state.regionValue && this.state.regionValue !== ''? this.state.regionValue: 1;
        const fees_filter = this.state.feesValue;
        const featured_filter = false;
        const search_filter = this.props.search;
        this.setState({ regionValue: region_filter });
        const category_filter = this.state.festival;
        this.getADD(region_filter);
        this.getMicWithRegion(region_filter, fees_filter, featured_filter, day_filter, search_filter, category_filter, this.state.page);
    });
  }

  renderPostMic(tabIndex, weekDaysVisible, weekButtonTittle, styleforGrid, styleForList, postData, layout, advertiseData ) {
    return (<div className="container">
    <Tabs className="tabs-weeks" selectedIndex={tabIndex} onSelect={this.handleSelect.bind(this)}>
      <div className="row">
        <div className="col-lg-7">
          <TabList className="nav nav-tabs">
            <div className="days days-mobile">
              <div className="weekend">
                <Tab eventkey="monday"><span>Monday</span></Tab>
                <Tab eventkey="tuesday"><span>Tuesday</span></Tab>
                <Tab eventkey="wednesday"><span>Wednesday</span></Tab>
                <Tab eventkey="thursday" ><span>Thursday</span></Tab>
                <Tab eventkey="friday"><span>Friday</span></Tab>
                <Tab style={{ display: weekDaysVisible }}><span>Saturday</span></Tab>
                <Tab eventkey="sunday" style={{ display: weekDaysVisible }}><span>Sunday</span></Tab>
              </div>
              <div className="btn-mobile-week">
                <a className="btn btn-primary btn-blue btn-sm" onClick={this.handleOtherWeek}>{weekButtonTittle}</a>
              </div>
            </div>
          </TabList>
        </div>

        <div className="col-lg-5">
          <div className="btn-group ">
            <div className="btn-view-othersday">
              <a className="btn btn-primary btn-blue" onClick={this.handleOtherWeek}>{weekButtonTittle}</a>
            </div>
            <div className="btn-icons" id="btn-icons">
              <a className={styleforGrid} onClick={this.handleGridLayout}><i className="material-icons">view_module</i></a>
              <a className={styleForList} onClick={this.handleListLayout}><i className="material-icons">view_list</i></a>
            </div>
          </div>
        </div>
      </div>
    <div className="row">
    <div className="col-lg-12">
    
      <TabPanel eventkey="saturday">
        <GridPost postData={postData} layout={layout} advertise={advertiseData} />
    
      </TabPanel>

      <TabPanel eventkey="sunday">
      <GridPost postData={postData} layout={layout} advertise={advertiseData}/>
      </TabPanel>

      <TabPanel eventkey="monday">
      <GridPost postData={postData} layout={layout} advertise={advertiseData}/>
       </TabPanel>

      <TabPanel eventkey="tuesday">
      <GridPost postData={postData} layout={layout} advertise={advertiseData}/>
      </TabPanel>

      <TabPanel eventkey="wednesday">
      <GridPost postData={postData} layout={layout} advertise={advertiseData}/>
      </TabPanel>

      <TabPanel eventkey="thursday">
      <GridPost postData={postData} layout={layout} advertise={advertiseData}/>
      </TabPanel>

      <TabPanel eventkey="friday">
      <GridPost postData={postData} layout={layout} advertise={advertiseData}/>
      </TabPanel>
      </div>
</div>
    </Tabs>


  </div>);
  }

  render() {
    const { tabIndex, weekDaysVisible, weekButtonTittle, styleforGrid, styleForList, postData, layout, advertiseData } = this.state;
    return (
      <section className="grid-section">
        <div className="grid-div-header">
          {this.renderPostMic( tabIndex, weekDaysVisible, weekButtonTittle, styleforGrid, styleForList, postData, layout, advertiseData )}
        </div>
        <div className="container">
        <div className="pagination-main">{this.renderPagination()}</div></div>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  search: state.filterMic.search,
  regionData: state.postMic.regionData,
  region: state.filterMic.region,
  feesValue: state.filterMic.feesValue,
  featuredValue: state.filterMic.featuredValue,
  advertise: state.filterMic.advertise,
});

const mapDispatchToProps = dispatch => ({
//getRegion: data => dispatch(getRegion(data)),
//setSearchValue: search => dispatch(setSearchValue(search)),
  getAdvertise: data => dispatch(getAdvertise(data))
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ListContainer),
);