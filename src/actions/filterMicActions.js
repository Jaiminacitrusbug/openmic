import * as types from './actionTypes';

export const setSearchValue = (search) => ({
  type: types.SET_SEARCH_VALUE,
  payload: search,
});

export const setRegionValue = (region) => ({
  type: types.SET_REGION_VALUE,
  payload: region,
});

export const setFeesValue = (feesValue) => ({
  type: types.SET_FEES_VALUE,
  payload: feesValue,
});

export const setFeaturedValue = (featuredValue) => ({
  type: types.SET_FEATURED_VALUE,
  payload: featuredValue,
});

export const getAdvertise = (advertise) => ({
  type: types.GET_ADVERTISE,
  payload: advertise,
});

