import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import logoBlack from '../../assets/images/Size/Logoforwhite2.png';
import history from '../../services/BrowserHistory';
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from 'yup';
import { addParticipant } from '../../api/participantAPI';
import { toast, ToastContainer } from 'react-toastify';

class RequestToParticipateWithPayment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        };
    }

    render() {
        const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
        return (
            <Modal id="modal-request" className="modal fade modal-custom  modal-contact" role="dialog" show={this.props.display}>
                <div className="modal-header">
                </div>
                <div className="modal-body">
                    <Formik
                        initialValues={{
                            name: '',
                            lname: '',
                            email: '',
                            cellNumber: '',
                            comment: '',
                        }}
                        validationSchema={Yup.object().shape({
                            name: Yup.string()
                                .required('First name is required'),
                            lname: Yup.string()
                                .required('Last name is required'),
                            email: Yup.string()
                                .email('Email is invalid')
                                .required('Email is required'),
                            cellNumber: Yup.string()
                                .matches(phoneRegExp, 'Cell number is not valid')
                                .required('Cell number is required'),
                        })}
                        onSubmit={fields => {
                            const participant = {
                                first_name: fields.name,
                                last_name: fields.lname,
                                contact: fields.cellNumber,
                                email: fields.email,
                                profile_avatar: 1,
                                comment: fields.comment,
                                mic: this.props.micId,
                            };
                            this.setState({ loading: true });
                            this.props.participantData(participant);
                            this.props.hide();
                            // addParticipant(participant).then((result) => {
                            //     if (result) {
                            //         if (result.code === 201 && result.status === true) {
                            //             this.setState({ loading: false });
                            //             toast.info(result.message);
                                        
                            //         } else if (result.code === 200 && result.status === false) {
                            //             this.setState({ loading: false });
                            //             toast.error(result.message);
                            //         }
                            //     }
                            // })

                        }}
                        render={({ errors, status, touched }) => (
                            <Form>
                                <div className="modal-logo">
                                    <a href="#">
                                        <img src={logoBlack} alt="logo" />
                                    </a>
                                </div>
                                <div className="modal-heading dotted-gradient">
                                    <h3>Send request to producer</h3>
                                </div>
                                <form action="thanks.html">
                                    <div className="Contact-form send-request login-form">
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="form-group">
                                                    <label className="has-float-label">
                                                        {/* <input className="form-control" type="text" placeholder="Name" required/> */}
                                                        <Field
                                                            type="text"
                                                            name="name"
                                                            placeholder="First name"
                                                            className="form-control"
                                                        />
                                                        <span>First name *</span>
                                                    </label>
                                                    <ErrorMessage name="name" component="span" className="error" />
                                                </div>
                                            </div>
                                            <div className="col-lg-12">
                                                <div className="form-group">
                                                    <label className="has-float-label">
                                                        {/* <input className="form-control" type="text" placeholder="Name" required/> */}
                                                        <Field
                                                            type="text"
                                                            name="lname"
                                                            placeholder="Last name"
                                                            className="form-control"
                                                        />
                                                        <span>Last name *</span>
                                                    </label>
                                                    <ErrorMessage name="lname" component="span" className="error" />
                                                </div>
                                            </div>
                                            <div className="col-lg-12">
                                                <div className="form-group">
                                                    <label className="has-float-label">
                                                        {/* <input className="form-control" type="number" placeholder="Cell Number" required/> */}
                                                        <Field
                                                            type="text"
                                                            name="cellNumber"
                                                            placeholder="Cell Number"
                                                            className="form-control"
                                                        />
                                                        <span>Cell Number *</span>
                                                    </label>
                                                    <ErrorMessage name="cellNumber" component="span" className="error" />
                                                </div>
                                            </div>
                                            <div className="col-lg-12">
                                                <div className="form-group">
                                                    <label className="has-float-label">
                                                        {/* <input className="form-control" type="email" placeholder="Email" required/> */}
                                                        <Field
                                                            type="text"
                                                            name="email"
                                                            placeholder="Email"
                                                            className="form-control"
                                                        />
                                                        <span>Email *</span>
                                                    </label>
                                                    <ErrorMessage name="email" component="span" className="error" />
                                                </div>
                                            </div>

                                            <div className="col-lg-12">
                                                <div className="form-group">
                                                    <label className="has-float-label" htmlFor="second">
                                                        {/* <textarea className="form-control" rows="4"></textarea> */}
                                                        <Field
                                                            type="textarea"
                                                            rows="4"
                                                            name="comment"
                                                            placeholder="Comment"
                                                            className="form-control"
                                                        />
                                                        <span>Comment</span></label>
                                                </div>
                                            </div>

                                            <div className="col-lg-12 text-right ">
                                                <div className="btn-groups">
                                                    <a href='/Home' className="btn">Not Now</a>
                                                    <button disabled={this.state.loading} type="submit" value="submit" href="" className="btn btn-login">Next {this.state.loading === true ? <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                  </div> : null}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <ToastContainer autoClose={2000} />
                            </Form>
                        )}
                    />
                </div>
            </Modal>
        );
    }
}

export default RequestToParticipateWithPayment;