import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from 'yup';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { addAccountDetails, setStepCount, addDateTime } from '../../../actions/postMicFormActions';
import { signup, resendSignUpMail } from '../../../api/signUpApi';
import { setNewUserData } from '../../../actions/authActions'; 
class AccountDetails_s1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorForEmail: '',
      loading: false,
    }
  }

  componentDidUpdate(prevProps) {

    const { date } = this.props;
    if (prevProps.date !== date) {
      this.props.addDateTime(date);
    }
  }

  render() {
    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
    return (
      <fieldset>
        <Formik
          initialValues={{
            firstName: this.props.accountDetails && this.props.accountDetails.firstName !== '' ? this.props.accountDetails.firstName : '',
            lastName: this.props.accountDetails && this.props.accountDetails.lastName !== '' ? this.props.accountDetails.lastName : '',
            email: this.props.accountDetails && this.props.accountDetails.email !== '' ? this.props.accountDetails.email : '',
            cellNumber: this.props.accountDetails && this.props.accountDetails.cellNumber !== '' ? this.props.accountDetails.cellNumber : '',
            password: this.props.accountDetails && this.props.accountDetails.password !== '' ? this.props.accountDetails.password : '',
            cPassword: this.props.accountDetails && this.props.accountDetails.cPassword !== '' ? this.props.accountDetails.cPassword : '',
          }}
          validationSchema={Yup.object().shape({
            firstName: Yup.string().trim().strict(false)
              .required('First Name is required'),
            lastName: Yup.string().trim().strict(false)
              .required('Last Name is required'),
            email: Yup.string().trim().strict(false)
              .email('Email is invalid')
              .required('Email is required'),
            cellNumber: Yup.string().trim().strict(false)
              .matches(phoneRegExp, 'Cell number is not valid')
              .required('Cell number is required'),
            password: Yup.string().trim().strict(false)
              .min(6, 'Password must be at least 6 characters')
              .required('Password is required'),
            cPassword: Yup.string().trim().strict(false)
              .oneOf([Yup.ref('password'), null], 'Passwords must match')
              .required('Confirm Password is required'),
          })}
          onSubmit={fields => {
            const { addAccountDetails, setStepCount } = this.props;
            const RegisterData = {
              first_name: fields.firstName,
              last_name: fields.lastName,
              email: fields.email,
              contact: fields.cellNumber,
              password: fields.password,
              profile_avtar: 1,
            }
            this.setState({loading: true})
            signup(RegisterData).then((result) => {
              if (result) {
                if (result.code === 200 && result.status === true) {
                    addAccountDetails(result.data);
                    const count = 1;
                    setStepCount(count);
                    this.setState({loading: false})
                    this.setState({ errorForEmail: ''})
                  }
                   else if (result.code === 200 && result.status === false) {
                     this.setState({ errorForEmail: result.message})
                     this.setState({loading: false})
                   }
              } else {
                this.setState({loading: false})
              }
            })

          }}
          render={({ errors, status, touched }) => (
            <Form>
              <div className="row mt20">

                <div className="col-lg-12">
                  <div className="step-title">
                    <h4>Step 1 of 7</h4>
                    <p>Account Details </p>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="form-group">
                    <label className="has-float-label">
                      {/* <input type="text" className="form-control" name="first_name" id="first_name" placeholder="First Name" /> */}
                      <Field
                        type="text"
                        name="firstName"
                        placeholder="First Name"
                        className="form-control"
                      />
                      <span className="">First Name *</span>
                    </label>
                    <ErrorMessage name="firstName" component="span" className="error" />
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group">
                    <label className="has-float-label">
                      {/* <input type="text" className="form-control"  name="last_name" id="last_name" placeholder="Last Name" /> */}
                      <Field
                        type="text"
                        name="lastName"
                        placeholder="Last Name"
                        className="form-control"
                      />
                      <span className="">Last Name *</span>
                    </label>
                    <ErrorMessage name="lastName" component="span" className="error" />
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group">
                    <label className="has-float-label">
                      {/* <input type="email" className="form-control"  name="email" id="email" placeholder="Email" /> */}
                      <Field
                        type="text"
                        name="email"
                        placeholder="Email Address"
                        className="form-control"
                      />
                      <span className="">Email *</span>
                    </label>
                    <label className='error'>{this.state.errorForEmail}</label>
                    <ErrorMessage name="email" component="span" className="error" />
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="form-group">
                    <label className="has-float-label">
                      {/* <input type="Number" className="form-control"  name="Cell" id="Cell" placeholder="Cell Number" /> */}
                      <Field
                        type="text"
                        name="cellNumber"
                        placeholder="Cell Number"
                        className="form-control"
                      />
                      <span className="">Cell Number *</span>
                    </label>
                    <ErrorMessage name="cellNumber" component="span" className="error" />
                    <span className="note">will not post on this site unless you request us to do so.</span>
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="form-group">
                    <label className="has-float-label">
                      {/* <input type="password" className="form-control"  name="password" id="password" placeholder="password" /> */}
                      <Field
                        type="password"
                        name="password"
                        placeholder="Password"
                        className="form-control"
                      />
                      <span className="">Password *</span>
                    </label>
                    <ErrorMessage name="password" component="span" className="error" />
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className="form-group">
                    <label className="has-float-label">
                      {/* <input type="password" className="form-control"  name="password" id="password" placeholder="Confirm password" /> */}
                      <Field
                        type="password"
                        name="cPassword"
                        placeholder="Confirm password"
                        className="form-control"
                      />
                      <span className="">Confirm password *</span>
                    </label>
                    <ErrorMessage name="cPassword" component="span" className="error" />
                  </div>
                </div>

              </div>
              <div className="row">
                <div className="col-lg-12 text-right">
                  <div className="btn-groups">
                    {/* <a href="/postMicForm" className="btn btn-login">Agree</a> */}
                    <button disabled={this.state.loading} type="submit" className="btn btn-login">Next {this.state.loading === true ?<div className="spinner-border" role="status">
              <span className="sr-only">Loading...</span>
            </div> : null }</button>
                  </div>
                </div>
              </div>
            </Form>
          )}
        />
      </fieldset>
    );
  }
}

//export default AccountDetails_s1;

const mapDispatchToProps = dispatch => ({
  addAccountDetails: fields => dispatch(addAccountDetails(fields)),
  setStepCount: count => dispatch(setStepCount(count)),
  addDateTime: date => dispatch(addDateTime(date)),
  setNewUserData: userDetails => dispatch(setNewUserData(userDetails)),
});
const mapStateToProps = state => ({
  accountDetails: state.accountDetails.accountDetails,
  count: state.accountDetails.count,
  selectedDate: state.accountDetails.selectedDate,
});
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(AccountDetails_s1),
);
