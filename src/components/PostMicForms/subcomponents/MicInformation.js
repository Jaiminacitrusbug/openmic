import React, { Component } from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import * as Yup from 'yup'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import {
  addMicDetails,
  setStepCount,
  addDateTime
} from '../../../actions/postMicFormActions'
import { getRegions, getFees } from '../../../api/getMicApi'
import { setRegionValue } from '../../../actions/filterMicActions'
import { getRegion } from '../../../actions/postMicActions'
import Select from 'react-select'
import { event } from 'jquery'

class MicInformation extends Component {
  constructor (props) {
    super(props)
    this.state = {
      micName: '',
      micVenue: '',
      address: '',
      about: '',
      errorAbout: '',
      location: [],
      errorRegion: '',
      errorAll: ''
    }
    this.handlePreviousMic = this.handlePreviousMic.bind(this)
    this.handleRegion = this.handleRegion.bind(this)
    this.handleAbout = this.handleAbout.bind(this)
  }

  componentDidMount () {
    this.getRegionsOptions()
  }

  componentDidUpdate (prevProps) {
    const { date } = this.props
    if (prevProps.date !== date) {
      this.props.addDateTime(date)
    }

    if (prevProps.micData !== this.props.micData) {
    } else {
      if (this.state.regionValue === undefined) {
        if (this.props.micData && this.props.micData.region) {
          this.setState({ regionValue: this.props.micData.region }, () => {})
        }
      }
    }
  }

  getRegionsOptions () {
    const { getRegion, setRegionValue } = this.props
    getRegions().then((result, index) => {
      if (result && result.code === 200 && result.status === true) {
        let region = []
        result.data.map((item, index) => {
          region.push({
            label: item.region,
            id: item.id
          })
        })
        this.setState(
          { location: region, defaultLocation: region[0].name },
          () => {
            setRegionValue(region[0].name)
          }
        )
        getRegion(result.data)
      } else {
      }
    })
  }

  handleRegion (regionValue) {
    this.setState({ regionValue, errorRegion: '' }, () => {})
  }

  handlePreviousMic (values) {
    const { addMicDetails, setStepCount } = this.props
    values.region = this.state.regionValue
    values.aboutShow=this.state.about
    if (
      values.nameOfMic !== '' &&
      values.address !== '' &&
      values.venue !== '' &&
      values.aboutShow !== '' && values.aboutShow.length<=150 &&
      values.region !== ''
    ) {
      this.setState({
        errorAll: ''
      })
      addMicDetails(values)
      const count = 3
      setStepCount(count)
    } else {
      this.setState({
        errorAll: ''
      })
    }
  }
  handleAbout (event) {
    const aboutVal =event.target.value
    //alert(value)
    this.setState({ about: aboutVal})
    if (aboutVal.length > 150) {
      this.setState({
        errorAbout: 'About The Mic must be less than 150 Character'
      })
    } else {
      this.setState({ errorAbout: '' })
    }
  }
  render () {
    const { regionValue } = this.state

    return (
      <fieldset>
        <Formik
          initialValues={{
            nameOfMic:
              this.props.micData && this.props.micData.nameOfMic !== ''
                ? this.props.micData.nameOfMic
                : '',
            address:
              this.props.micData && this.props.micData.address !== ''
                ? this.props.micData.address
                : '',
            venue:
              this.props.micData && this.props.micData.venue !== ''
                ? this.props.micData.venue
                : '',
            aboutShow:
              this.props.micData && this.props.micData.aboutShow !== ''
                ? this.props.micData.aboutShow
                : '',
            //  capacity: this.props.micData && this.props.micData.capacity !== '' ? this.props.micData.capacity : '',
            region: {}
          }}
          validationSchema={Yup.object().shape({
            nameOfMic: Yup.string()
              .trim()
              .strict(true)
              .required('Name of Mic is required'),
            address: Yup.string()
              .trim()
              .strict(true)
              .required('Address is required'),
            venue: Yup.string()
              .trim()
              .strict(true)
              .required('Venue is required')
            // aboutShow: Yup.string().trim().strict(false)
            // .test('len', 'Only 600 characters is allowed', val => val.length <= 600)
            // .required('About the Mic is required')
            // .max(150,'Only 150 characters is allowed')
            // .min(0,''),
            // capacity: Yup.number().strict(false)
            //   .required('Number Of participant is required'),
          })}
          onSubmit={fields => {
            const { addMicDetails, setStepCount } = this.props

            if (this.state.regionValue === undefined) {
              this.setState({ errorRegion: 'Region is required' })
            } else {
              this.setState({ errorRegion: '' })
              fields.region = this.state.regionValue

              //console.log("filed about",this.state.about);
              if (this.state.about.trim() === '') {
                this.setState({
                  ...this.state,
                  errorAbout: 'About The Mic is required'
                })
              } else {
                if (this.state.about.trim().length > 150) {
                  this.setState({
                    ...this.state,
                    errorAbout: 'About The Mic must be less than 150 character'
                  })
                } else {
                  this.setState({ ...this.state, errorAbout: '' });
                  fields.aboutShow = this.state.about;
                  addMicDetails(fields);
                  const count = 5;
                  setStepCount(count);
                }
              }
            }
          }}
          render={({ errors, status, touched, values }) => (
            <Form>
              <div className='row mt20'>
                <div className='col-lg-12'>
                  <div className='step-title'>
                    {this.props.authenticated === false ? (
                      <h4>Step 5 of 7</h4>
                    ) : (
                      <h4>Step 4 of 6</h4>
                    )}
                    <p>Mic Details </p>
                  </div>
                </div>

                <div className='col-lg-6'>
                  <div className='form-group'>
                    <label className='has-float-label'>
                      {/* <input type="Text" className="form-control"  name="Venue" id="Venue" placeholder="Venue" /> */}
                      <Field
                        type='text'
                        //value={this.state.micName}
                        //onChange={event=>this.setState({micName: event.target.value})}
                        name='nameOfMic'
                        placeholder='Name Of Mic *'
                        className='form-control'
                      />
                      <span className=''>Name Of Mic *</span>
                    </label>
                    <ErrorMessage
                      name='nameOfMic'
                      component='span'
                      className='error'
                    />
                  </div>
                </div>

                <div className='col-lg-6'>
                  <span className='has-float-label'>
                    <div className='select2-div'>
                      {this.state.regionValue === undefined ? (
                        <Field
                          component={Select}
                          name='region'
                          placeholder='Region'
                          onChange={this.handleRegion}
                          options={
                            this.state.location.length > 0 &&
                            this.state.location
                          }
                        />
                      ) : (
                        <Select
                          value={this.state.regionValue}
                          onChange={this.handleRegion}
                          options={
                            this.state.location.length > 0 &&
                            this.state.location
                          }
                          className='select-multiple'
                        />
                      )}
                    </div>
                    <label htmlFor='second'>Select Region *</label>
                  </span>
                  <label className='error'>{this.state.errorRegion}</label>
                </div>

                <div className='col-lg-6'>
                  <div className='form-group'>
                    <label className='has-float-label'>
                      {/* <input type="Text" className="form-control"  name="Venue" id="Venue" placeholder="Venue" /> */}
                      <Field
                        type='text'
                        name='venue'
                        placeholder='Venue'
                        className='form-control'
                        // value={this.state.micVenue}
                        // onChange={event=>this.setState({micVenue: event.target.value})}
                      />
                      <span className=''>Venue *</span>
                    </label>
                    <ErrorMessage
                      name='venue'
                      component='span'
                      className='error'
                    />
                  </div>
                </div>

                <div className='col-lg-6'>
                  <div className='form-group'>
                    <label className='has-float-label'>
                      {/* <textarea className="form-control text-area" rows="4" name="about" id="about" placeholder="About show"></textarea> */}
                      <Field
                        component='textarea'
                        name='address'
                        placeholder='Address *'
                        className='form-control'
                        // value={this.state.micAddress}
                        // onChange={event=>this.setState({micAddress: event.target.value})}
                        rows='4'
                      />
                      <span>Address *</span>
                    </label>
                    <ErrorMessage
                      name='address'
                      component='span'
                      className='error'
                    />
                  </div>
                </div>

                {/* <div className="col-lg-6">
                  <div className="form-group">
                    <label className="has-float-label">
                     
                      <Field
                       
                        name="capacity"
                        placeholder="Number Of Participant To Allow *"
                        className="form-control"
                        
                        type="number"
                      />
                      <span>Number Of Participant To Allow *</span>
                    </label>
                    <ErrorMessage name="capacity" component="span" className="error" />
                  </div>
                </div> */}

                <div className='col-lg-12'>
                  <div className='form-group'>
                    <label className='has-float-label'>
                      <textarea
                        className='form-control text-area'
                        value={this.state.about}
                        onChange={this.handleAbout}
                        rows='4'
                        name='aboutShow'
                        id='about'
                        placeholder='About the Mic(Maximum 150 Charcater)'
                      
                      ></textarea>
                      {/* { <Field
                        component="textarea"
                        name="aboutShow"
                        placeholder="About the Mic"
                        className="form-control"

                        rows="4"
                        value={this.state.about}
                        onChange={event=>this.setState({about: event.target.value})}
                      /> } */}

                      <span>About The Mic *</span>
                    </label>
                    <label className='error'>{this.state.errorAbout}</label>
                    {/* <ErrorMessage name="aboutShow" component="span" className="error" /> */}
                  </div>
                </div>
              </div>

              <label className='error'>{this.state.errorAll}</label>

              <div className='row'>
                <div className='col-lg-12 '>
                  <div className='btn-groups-steps'>
                    {/* <a href="/postMicForm" className="btn btn-login">Agree</a> */}
                    <button
                      type='submit'
                      onClick={() => this.handlePreviousMic(values)}
                      className='btn btn-prev'
                    >
                      Previous
                    </button>
                    <button type='submit' className='btn btn-next'>
                      Next
                    </button>
                  </div>
                </div>
              </div>
            </Form>
          )}
        />
      </fieldset>
    )
  }
}

//export default micData_s1;

const mapDispatchToProps = dispatch => ({
  addMicDetails: fields => dispatch(addMicDetails(fields)),
  setStepCount: count => dispatch(setStepCount(count)),
  addDateTime: date => dispatch(addDateTime(date)),
  getRegion: data => dispatch(getRegion(data)),
  setRegionValue: region => dispatch(setRegionValue(region))
})
const mapStateToProps = state => ({
  micData: state.accountDetails.micData,
  count: state.accountDetails.count,
  selectedDate: state.accountDetails.selectedDate,
  authenticated: state.authUser.authenticated
})
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(MicInformation)
)
