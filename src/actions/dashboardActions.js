import * as types from './actionTypes';

export const getSuggestedTab = (tabName) => ({
  type: types.GET_SUGGESTED_TAB,
  payload: tabName,
});

export const addMicData = (micData) => ({
  type: types.ADD_MIC_DATA,
  payload: micData,
});

export const isAccoutUpdated = (account) => ({
  type: types.IS_ACCOUT_UPDATED,
  payload: account
}) 