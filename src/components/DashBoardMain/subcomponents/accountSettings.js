import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from 'yup';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { userAccountUpdate, getProfileAvatar, userAccount } from '../../../api/userAccount';
import { changeCurrentPasswrod } from '../../../api/loginAPI';
import { isAccoutUpdated } from '../../../actions/dashboardActions';
import male1 from '../../../assets/images/m1.png';
import male2 from '../../../assets/images/m2.png';
import male3 from '../../../assets/images/m3.png';
import male4 from '../../../assets/images/m4.png';
import male5 from '../../../assets/images/m5.png';
import { toast } from 'react-toastify';

class AccountSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardOneStyle: 'card',
      collapseOneStyle: 'collapse',
      iconOneStyle: 'fa fa-plus',
      cardTwoStyle: 'card',
      collapseTwoStyle: 'collapse',
      iconTwoStyle: 'fa fa-plus',
      userData: JSON.parse(sessionStorage.getItem('user')),
      profileAvatar: [],
      contact: '',
      profileAvatarId:'',
    };
    this.handleButtonPlusOne = this.handleButtonPlusOne.bind(this);
    this.handleButtonPlusTwo = this.handleButtonPlusTwo.bind(this);
  }

  componentDidMount() {
    this.getProfileImages();
   this.getUserDetails();
    this.setState({ profileAvatarId: this.state.userData.profile_avtar })
  }

  getUserDetails() {
    userAccount(this.state.userData.id).then((result) => {
      if (result) {
        if (result.code === 200 && result.status === true) {
          this.setState({ contact: result.data.contact });
        }
      }
    })
  }

  getProfileImages() {
    getProfileAvatar().then((result) => {
      if (result) {
        if (result.code === 200 && result.status === true) {
          this.setState({ profileAvatar: result.data });
        }
      }
    })
  }

  handleButtonPlusOne() {
    const { cardOneStyle, cardTwoStyle, iconOneStyle, iconTwoStyle, collapseOneStyle, collapseTwoStyle } = this.state;
    if (cardOneStyle === 'card') {
      this.setState({
        cardOneStyle: 'card active',
        collapseOneStyle: 'collapse show',
        iconOneStyle: 'fa fa-minus',
        cardTwoStyle: 'card',
        collapseTwoStyle: 'collapse',
        iconTwoStyle: 'fa fa-plus',
      });
    } else {
      this.setState({
        cardOneStyle: 'card',
        collapseOneStyle: 'collapse',
        iconOneStyle: 'fa fa-plus',
        cardTwoStyle: 'card',
        collapseTwoStyle: 'collapse',
        iconTwoStyle: 'fa fa-plus',
      });
    }
  }

  handleButtonPlusTwo() {
    const { cardOneStyle, cardTwoStyle, iconOneStyle, iconTwoStyle, collapseOneStyle, collapseTwoStyle } = this.state;
    if (cardTwoStyle === 'card') {
      this.setState({
        cardOneStyle: 'card',
        collapseOneStyle: 'collapse',
        iconOneStyle: 'fa fa-plus',
        cardTwoStyle: 'card active',
        collapseTwoStyle: 'collapse show',
        iconTwoStyle: 'fa fa-minus',
      });
    } else {
      this.setState({
        cardOneStyle: 'card',
        collapseOneStyle: 'collapse',
        iconOneStyle: 'fa fa-plus',
        cardTwoStyle: 'card',
        collapseTwoStyle: 'collapse',
        iconTwoStyle: 'fa fa-plus',
      });
    }
  }

  handleProfileAvatar(index) {
   this.setState({ profileAvatarId: index });
  }


  render() {
    const { profileAvatar, profileAvatarId } = this.state;
    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
    return (
      <div id="account-setting" className="tab-pane tab-form">
        <div className="grid-layout">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-6">
              <div className="bs-example accordian-openmic">
                <div className="accordion" id="accordionExample">
                  <div className={this.state.cardOneStyle}>
                    <div className="card-header" id="headingOne">
                      <h2 className="mb-0">
                        <button type="button"
                          className="btn btn-link"
                          data-toggle="collapse"
                          data-target="#collapseOne"
                          onClick={this.handleButtonPlusOne}>Change password<i className={this.state.iconOneStyle}></i></button>
                      </h2>
                    </div>
                    {/* First */}
                    <div id="collapseOne" className={this.state.collapseOneStyle} aria-labelledby="headingOne" data-parent="#accordionExample">
                      <Formik
                        initialValues={{
                          oldPassword: '',
                          newPassword: '',
                          CNewPassword: '',
                        }}
                        validationSchema={Yup.object().shape({
                          oldPassword: Yup.string()
                            .required('Password is required'),
                          newPassword: Yup.string()
                            .min(6, 'Password must be at least 6 characters')
                            .required('Password is required'),
                          CNewPassword: Yup.string()
                            .oneOf([Yup.ref('newPassword'), null], 'Passwords must match')
                            .required('Password is required'),
                        })}
                        onSubmit={fields => {
                          const passwordData = {
                            password: fields.oldPassword,
                            new_password: fields.newPassword,
                          }
                          changeCurrentPasswrod(this.state.userData.id, passwordData).then((result) => {
                            if (result) {
                              if (result.code === 200 && result.status === true) {
                                toast.info(result.message);
                                this.setState({ cardOneStyle: ''}, () => {
                                  this.handleButtonPlusOne();
                                fields.oldPassword = '';
                                fields.newPassword = '';
                                fields.CNewPassword = '';
                                })
                                
                              } else if (result.code === 200 && result.status === false) {
                                toast.error(result.message);
                              }
                            }
                          });
                        }}
                        render={({ errors, status, touched }) => (
                          <Form>
                            <div className="card-body">
                              <div className="update-account-form">
                                <div className="row justify-content-center">
                                  <div className="col-lg-12">
                                    <div className="form-group">
                                      <label className="has-float-label">
                                        {/* <input className="form-control" type="text" placeholder="Old Password"/> */}
                                        <Field
                                          type="password"
                                          name="oldPassword"
                                          placeholder="Old Password"
                                          className="form-control"
                                        />
                                        <span>Old Password *</span>
                                      </label>
                                      <ErrorMessage name="oldPassword" component="span" className="error" />
                                    </div>
                                  </div>
                                  <div className="col-lg-6">
                                    <div className="form-group">
                                      <label className="has-float-label">
                                        {/* <input className="form-control" type="text" placeholder="New Password"/> */}
                                        <Field
                                          type="password"
                                          name="newPassword"
                                          placeholder="New Password"
                                          className="form-control"
                                        />
                                        <span>New Password *</span>
                                      </label>
                                      <ErrorMessage name="newPassword" component="span" className="error" />
                                    </div>
                                  </div>
                                  <div className="col-lg-6">
                                    <div className="form-group">
                                      <label className="has-float-label">
                                        {/* <input className="form-control" type="text" placeholder="Confirm Password"/> */}
                                        <Field
                                          type="password"
                                          name="CNewPassword"
                                          placeholder="Confirm Password"
                                          className="form-control"
                                        />
                                        <span>Confirm Password</span>
                                      </label>
                                      <ErrorMessage name="CNewPassword" component="span" className="error" />
                                    </div>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-lg-12 text-right tab-form-footer">
                                    <div className="btn-groups">
                                      <button type="submit" className="btn btn-login">Save</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </Form>
                        )}
                      />
                    </div>
                  </div>
                  <div className={this.state.cardTwoStyle}>
                    <div className="card-header" id="headingTwo">
                      <h2 className="mb-0">
                        <button type="button" className="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" onClick={this.handleButtonPlusTwo}>Update Account Details <i className={this.state.iconTwoStyle}></i></button>
                      </h2>
                    </div>

                    {/* second */}
                    <div id="collapseTwo" className={this.state.collapseTwoStyle} aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <Formik
                        initialValues={{
                          firstName: this.state.userData && this.state.userData.first_name !== null ? this.state.userData.first_name : '',
                          lastName: this.state.userData && this.state.userData.last_name !== null ? this.state.userData.last_name : '',
                          contact: this.props.contact !== undefined || this.props.contact !== null || this.props.contact !== '' ? this.props.contact : '',
                        }}
                        validationSchema={Yup.object().shape({
                          firstName: Yup.string()
                            .required('First Name is required'),
                          lastName: Yup.string()
                            .required('Last Name is required'),
                          contact: Yup.string()
                            .matches(phoneRegExp, 'Contact number is not valid')
                            .required('Contact Number is required'),
                        })}
                        onSubmit={fields => {

                          const updateUser = {
                            first_name: fields.firstName,
                            last_name: fields.lastName,
                            contact: fields.contact,
                            profile_avtar: this.state.profileAvatarId,
                          }
                          userAccountUpdate(this.state.userData.id, updateUser).then((result) => {
                            if (result) {
                              if (result.code === 200 && result.status === true) {
                                this.props.isAccoutUpdated(true);
                                sessionStorage.setItem('user', JSON.stringify(result.data))
                                toast.info(result.message);
                              }
                            }
                          });

                        }}
                        render={({ errors, status, touched }) => (
                          <Form>
                            <div className="card-body ">
                              <div className="update-account-form">
                                <div className="row">
                                  <div className="col-lg-5">
                                    <div className="account-profile">
                                    <h2>Select Avatar</h2>
                                      <ul>
                                      {profileAvatar && profileAvatar.map((data,index)=> {
                                         return  <li className={profileAvatarId === data.id? 'active' : ''} key={index} ><img src={data.profile_avtar_url} 
                                         onClick={()=> this.handleProfileAvatar(data.id)} /></li>
                                      })}
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                                {/* <div>
                        <ul>
                          {profileAvatar && profileAvatar.map((data,index)=> {
                            return  <li key={index} ><img src={data.profile_avtar_url} /></li>
                          })}
                       </ul>
                            </div> */}


                                <div className="row">
                                  <div className="col-lg-6">
                                    <div className="form-group">
                                      <label className="has-float-label">
                                        <Field
                                          type="text"
                                          name="firstName"
                                          placeholder="First Name"
                                          className="form-control"
                                        />
                                        <span className="">First Name *</span>
                                      </label>
                                      <ErrorMessage name="firstName" component="span" className="error" />
                                    </div>
                                  </div>
                                  <div className="col-lg-6">
                                    <div className="form-group">
                                      <label className="has-float-label">
                                        <Field
                                          type="text"
                                          name="lastName"
                                          placeholder="Last Name"
                                          className="form-control"
                                        />
                                        <span className="">Last Name *</span>
                                      </label>
                                      <ErrorMessage name="lastName" component="span" className="error" />
                                    </div>
                                  </div>
                                  <div className="col-lg-6">
                                    <div className="form-group">
                                      <label className="has-float-label">
                                        <Field
                                          type="text"
                                          name="contact"
                                          placeholder="Contact Number"
                                          className="form-control"
                                        />
                                        <span className="">Contact Number *</span>
                                      </label>
                                      <ErrorMessage name="contact" component="span" className="error" />
                                    </div>
                                  </div>
                                  {/* <div className="col-lg-12">
                                <div className="input-group">
                                    <label className="has-float-label">
                                    <Field
                                      type="text"
                                      name="email"
                                      placeholder="Email Address"
                                      className="form-control"
                                      disabled
                                    />
                                    <span className="">Email *</span>
                                  </label>
                                    <div className="input-group-append">
                                      <button className="btn btn-primary btn-send" type="button">Send mail</button>
                                    </div>
                                </div>
                                <ErrorMessage name="email" component="span" className="error" />
                            </div> */}

                                  <div className="col-lg-12 text-right tab-form-footer">
                                    <div className="btn-groups">
                                      {/* <a href="#" className="btn">Not Now</a> */}
                                      <button type="submit" className="btn btn-login">Save</button>
                                    </div>
                                  </div>
                                </div>

                              </div>
                            </div>
                          </Form>
                        )}
                      />
                    </div>
                  </div>

                </div>
              </div>

            </div>
          </div>
        </div>
      </div>

    );
  }
}

const mapDispatchToProps = dispatch => ({
  isAccoutUpdated: account => dispatch(isAccoutUpdated(account)),
});

const mapStateToProps = state => ({
  userDetails: state.authUser.userDetails,
});
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(AccountSettings),
);