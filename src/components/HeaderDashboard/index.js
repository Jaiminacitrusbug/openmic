import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Select from 'react-select';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getSuggestedTab, isAccoutUpdated } from '../../actions/dashboardActions';
import logoMain from '../../assets/images/Size/Logoforblack@2x.png'
import theater from '../../assets/images/icon/theater.svg';
import profile from '../../assets/images/User.png';
import logoBlack from '../../assets/images/Size/Logoforwhite2.png';
import AuthService from '../../services/authServices';
import { setStepCount } from '../../actions/postMicFormActions';
import history from '../../services/BrowserHistory';
import { userAccountUpdate, getProfileAvatar } from '../../api/userAccount';
const options = [
	{ value: 'I can’t acces my account', label: 'I can’t acces my account' },
	{ value: 'I can’t link my phone number', label: 'I can’t link my phone number' },
];


class HeaderDashboard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			renderContactUsModal: false,
			accountType: '',
			styleForNav: 'cd-shadow-layer',
			navDiv: 'nav-div clearfix',
			authenticated: false,
			userData: JSON.parse(sessionStorage.getItem('user')),
			profileAvatar: [],
			profileAvatarUrl: profile,
			path:'',
		};
		this.handleContanctUsModal = this.handleContanctUsModal.bind(this);
		this.handleAccountType = this.handleAccountType.bind(this);
		this.handleCloseContanchUsModal = this.handleCloseContanchUsModal.bind(this);
		this.handleNavBar = this.handleNavBar.bind(this);
		this.handleTabClick = this.handleTabClick.bind(this);
		this.handleLogout = this.handleLogout.bind(this);
		this.postMic = this.postMic.bind(this);
	};

	componentDidMount() {
		this.setState({ path: this.props.match.url}, ()=> {
		})
		window.addEventListener('scroll', this.handleScroll.bind(this), true);
		if (this.props.authenticated === true) {
			this.setState({ authenticated: true })
		} else {
			this.setState({ authenticated: false })
		}
		this.getProfileImages();
		//localStorage.setItem('Bixex.profileSelectToken', '');
	}

	getProfileImages() {
		getProfileAvatar().then((result) => {
			if (result) {
				if (result.code === 200 && result.status === true) {
					this.setState({ profileAvatar: result.data, userData: JSON.parse(sessionStorage.getItem('user')) }, () => {
						const { profileAvatar, userData } = this.state;
						profileAvatar.map((data) => {
							if (data.id === userData.profile_avtar) {
								this.setState({ profileAvatarUrl: data.profile_avtar }, () => {
									this.props.isAccoutUpdated(false);
								})
							}
						})
					});
				}
			}
		})
	}

	componentDidUpdate(prevProps) {
		const { authenticated, account } = this.props;
		if (prevProps.authenticated !== authenticated) {
			if (this.props.authenticated === true) {
				this.setState({ authenticated: true })
			} else {
				this.setState({ authenticated: false })
			}
		}
		if (prevProps.account !== account) {
			if (account === true) {
				this.getProfileImages();
			}
		}
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this.handleScroll.bind(this), true);
	}

	handleScroll(event) {
		const winScroll = document.body.scrollTop || document.documentElement.scrollTop;
		let sticky = document.getElementsByClassName('header-div cbp-af-header clearfix');
		if (sticky.length > 0) {
			if (winScroll >= 70) {
				sticky[0].classList.add('cbp-af-header-shrink');
			} else {
				sticky[0].classList.remove('cbp-af-header-shrink');
			}
		}
	}

	handleContanctUsModal() {
		this.setState({ renderContactUsModal: true });
	}

	handleCloseContanchUsModal() {
		this.setState({ renderContactUsModal: false });
	}

	handleAccountType(accountType) {
		this.setState({ accountType });
	}

	handleNavBar() {
		if (this.state.styleForNav === 'cd-shadow-layer') {
			this.setState({ styleForNav: 'cd-shadow-layer displayblock', navDiv: 'nav-div clearfix width80' });
		} else {
			this.setState({ styleForNav: 'cd-shadow-layer', navDiv: 'nav-div clearfix' });
		}
	}

	handleTabClick(e) {
		const { name } = e.target;
		this.props.getSuggestedTab(name);
	}

	handleLogout() {
		sessionStorage.clear();
		AuthService.logout();
		history.push('/');
	}

	postMic() {
		history.push('/postYourMic');
	}

	render() {
		const { authenticated, userData, profileAvatar, profileAvatarUrl } = this.state;
		return (
			<header>
				<div className="header-div cbp-af-header clearfix">
					<div className="inner-top-header-div clearfix">
						<div className="container">
							<div className="row">
								<div className="col-sm-12 col-lg-12">
									<div className="logo-div">
										<Link className="logo_link clearfix" to="/">
											<img src={logoMain} className="logo-white" alt="I-mobile" />
										</Link>
										</div>
									<nav>
										<div className="top-nav1">
											<div className={this.state.styleForNav}></div>
											<div className="nav-m-bar">
												<a onClick={this.handleNavBar} className="opennav" data-placement="bottom" title="" data-original-title="Menu">
													<i className="menu-bars"></i></a>
											</div>
											{/* <div className="fest-icon-bar">
                    <Link to="/comedyFestival" ><img src={theater} alt="search"/><span> Comedy festivals</span></Link>
                </div> */}
											<div className={this.state.navDiv} id="mySidenav">
												<a href="#" onClick={this.handleNavBar} className="closebtn">&times;</a>


												<ul className="nav ullist-inline header-profile" id="nav-res">
													<li className="btn-signup mr-4"><a onClick={this.postMic} href="#" data-toggle="modal" data-target="#modal-conatct">Post Your Open Mic For Free</a></li>
													{/* <li><a href="#" onClick={this.handleContanctUsModal} data-toggle="modal" data-target="#modal-conatct">Contact us</a></li> */}
													<li className="btn-signup user-active dropdown ">
														<a href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															<img src={profileAvatarUrl} alt="img" /> {userData.first_name} {userData.last_name}</a>

														<div className="dropdown-menu dropdown-menu-xl-right dropdown-profile" aria-labelledby="dropdownMenuButton">
															<a className="dropdown-item" href="#"><h3>Hi, {userData.first_name} {userData.last_name}</h3></a>
															<a className="dropdown-item" name="dashboard"  href="/dashboard">
																Dashboard
															</a>
															{this.state.path === '/dashboard'?
															<div>
															<a href="/comedyFestival"><span> Comedy festivals</span></a>
															<a className="dropdown-item" onClick={this.handleTabClick} name="accountSettings" href="#">Account settings</a>
															<a className="dropdown-item" onClick={this.handleTabClick} name="myMic" href="#">My mics</a>
															<a className="dropdown-item" onClick={this.handleTabClick} name="intrested" href="#">Interested participate</a>
															</div>
															: null }
															<a className="dropdown-item log-out" href="#" onClick={this.handleLogout}><h3><i className="material-icons">input</i> Logout</h3></a>
														</div>

													</li>
												</ul>
											</div>

										</div>
									</nav>
								</div>
							</div>
						</div>
					</div>
					<Modal id="modal-conatct" className="modal fade modal-custom  modal-contact" role="dialog" show={this.state.renderContactUsModal} onHide={this.handleCloseContanchUsModal}>
						<div className="modal-header">
							<h4 className=""></h4>
							<button type="button" className="close" data-dismiss="modal" onClick={this.handleCloseContanchUsModal}>&times;</button>
						</div>
						<div className="modal-body">
							<div className="modal-logo">
								<a href="#">
									<img src={logoBlack} alt="logo" />
								</a>
							</div>
							<div className="modal-heading dotted-gradient">
								<h3>Contact to openmiclist</h3>
							</div>
							<div className="Contact-form">
								<div className="row">
									<div className="col-lg-12">
										<div className="contact-content">
											<p>Choose a option and get the action</p>
										</div>
									</div>

									<div className="col-lg-12">
										<div className="form-group">
											<span className="has-float-label">
												<div className="select2-div">
													<Select
														value={this.state.accountType}
														onChange={this.handleAccountType}
														options={options}
														className="select-multiple"
													/>
													{/* <select className="select-multiple">
													<option>I can’t acces my account</option>
													<option>I can't link my phone number</option>
												</select> */}
												</div>
												<label htmlFor="second">Select option to contact</label>
											</span>
										</div>
									</div>
								</div>
							</div>

						</div>
					</Modal>
				</div>
			</header>
		);
	}
}


const mapDispatchToProps = dispatch => ({
	getSuggestedTab: name => dispatch(getSuggestedTab(name)),
	setStepCount: count => dispatch(setStepCount(count)),
	isAccoutUpdated: account => dispatch(isAccoutUpdated(account))
});

const mapStateToProps = state => ({
	authenticated: state.authUser.authenticated,
	userLoginData: state.authUser.userLoginData,
	account: state.dashboard.account,
});


export default withRouter(
	connect(
		mapStateToProps,
		mapDispatchToProps
	)(HeaderDashboard),
);
