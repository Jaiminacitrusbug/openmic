import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import draftImg from '../../../assets/images/grid-images/img-1.svg';
import { Modal } from 'react-bootstrap';
import blackLogo from '../../../assets/images/logo-black.svg';
import Flatpickr from 'react-flatpickr';
import 'flatpickr/dist/themes/material_blue.css';
import Select from 'react-select';
import Mic from '../../../assets/images/mic.png';
import { getFees, getMicDetails, editMicDetails, getMyMic } from '../../../api/getMicApi';
import moment from 'moment';
import { getpostMicData } from '../../../actions/postMicActions';
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';
import { toast } from 'react-toastify';
import { NONAME } from 'dns';
const options = [
  { value: 'FREE', label: 'Completly Free' },
  { value: 'PAID', label: 'Paid' },
];

const optionsForDrink = [
  { value: 'No Drink Minimum', label: 'No Drink Minimum' },
  { value: 'Drink Minimum', label: 'Drink Minimum' },
];

class EditPostModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: '',
      costType: '',
      DrinkType: '',
      selectedRadio: '',
      displayText: 'none',
      describe: '',
      imagePreviewUrl: '',
      file: '',
      Description: '',
      nameOfMic: '',
      venue: '',
      address: '',
      aboutShow: '',
      optionForDrink: [],
      country: '',
      featured: '',
      id: '',
      pincode: '',
      region: '',
      state_province: '',
      mic_status: '',
      errorNameOfMic: '',
      errorVenue: '',
      errorAddress: '',
      errorAboutShow: '',
      errorForCostType: '',
      errorForDrinkType: '',
      errorRadio: '',
      errorDescribe: '',
      errorFile: '',
      errorDate: '',
      userdata: JSON.parse(sessionStorage.getItem('user')),
      loading: false,
      displayTextCost: 'none',
      errorCost: '',
      cost: '',
      removedImage: false,
    }
    this.onChangeDate = this.onChangeDate.bind(this);
    this.handleCostType = this.handleCostType.bind(this);
    this.handleDrinkType = this.handleDrinkType.bind(this);
    this.handleOnRadioChange = this.handleOnRadioChange.bind(this);
    this.handleTextarea = this.handleTextarea.bind(this);
    this._handleImageChange = this._handleImageChange.bind(this);
    this.handleChangeTextarea = this.handleChangeTextarea.bind(this);
    this.handleTextChange = this.handleTextChange.bind(this);
    this.handleTextAreaChange = this.handleTextAreaChange.bind(this);
    this.handleSavePost = this.handleSavePost.bind(this);
    this.handleCost = this.handleCost.bind(this);
    this.radioOption = [
      { name: "Yes", value: "Yes" },
      { name: "No", value: "No" },
    ];
  }

  componentDidMount() {
    this.getMicType();
    this.setState({ removedImage: false })
  }

  componentDidUpdate(prevProps) {
    const { editId } = this.props;
    if (prevProps.editId !== editId) {
      this.getEditMic(editId);
      this.setState({ removedImage: false })
    }
  }

  getEditMic(editId) {
    getMicDetails(editId).then((result, index) => {
      if (result) {
        if (result.code === 200 && result.status === true) {
          const costType = {
            label: result.data.mic_cost_type,
            value: result.data.mic_cost_type,
          };
          const drinkType = {
            label: result.data.mic_fees.fees,
            value: result.data.mic_fees.id
          }
          this.setState({
            nameOfMic: result.data.mic_name,
            venue: result.data.mic_venue,
            costType: costType,
            displayTextCost: costType.value === "PAID"? 'block' : 'none',
            cost: result.data.mic_cost,
            DrinkType: drinkType,
            // date: moment(result.data.mic_datetime).format(), 
            selectedRadio: result.data.open_to_all === true ? 'Yes' : 'No',
            displayText: result.data.open_to_all === true ? 'none' : 'block',
            describe: result.data.open_to_all === true ? '' : result.data.performance_requirements,
            address: result.data.address,
            aboutShow: result.data.about_mic,
            imagePreviewUrl: result.data.poster_image,
            Description: result.data.additional_description !== undefined || result.data.additional_description !== null || result.data.additional_description !== '' ? result.data.additional_description : '',
            country: result.data.country,
            featured: result.data.featured,
            id: result.data.id,
            pincode: result.data.pincode,
            region: result.data.region,
            state_province: result.data.state_province,
            mic_status: result.data.mic_status
          });
        }
      }
    })
  }

  getMicType() {
    getFees().then((result) => {
      if (result && result.code === 200 && result.status === true) {
        let fees = [];
        result.data.map((item) => {
          fees.push({
            value: item.id,
            label: item.fees,
          });
        });
        this.setState({ optionForDrink: fees });
      }
    })
  }

  onChangeDate(date) {
    this.setState({ date: date[0], errorDate: '' }, () => {
    })
  }

  handleCostType(costType) {
    this.setState({ costType, errorForCostType: '' }, () => {
      if (costType.value === "PAID") {
        this.setState({ displayTextCost: 'block' })
      } else {
        this.setState({ displayTextCost: 'none' })
      }
    });
  }

  handleCost(e) {
    this.setState({ [e.target.name]: e.target.value }, () => {
      const regx = /^-?\d*(\.\d+)?$/
      var valid = regx.test(this.state.cost);
      if (valid === false) {
        this.setState({ errorCost: 'Enter valid cost' })
      } else {
        this.setState({ errorCost: '' })
      }
    });
  }

  handleDrinkType(DrinkType) {
    this.setState({ DrinkType, errorForDrinkType: '' }, () => {
    });
  }

  handleOnRadioChange(e) {
    const { value } = e.target;
    this.setState({ selectedRadio: value, errorRadio: '' }, () => {
      if (value === 'Yes') {
        this.setState({ displayText: 'none' })
      } else {
        this.setState({ displayText: 'block' })
      }
    })
  }

  handleTextarea(e) {
    const { value } = e.target;
    this.setState({ describe: value, errorDescribe: '' });
  }

  handleTextChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value }, () => {
      if (name === "nameOfMic" && this.state.nameOfMic !== '') {
        this.setState({ errorNameOfMic: '' });
      } else {
        if (name === "nameOfMic") {
          this.setState({ errorNameOfMic: 'Enter Name Of Your Mic' });
        }
      }
      if (name === "venue" && this.state.venue !== '') {
        this.setState({ errorVenue: '' });
      } else {
        if (name === "venue") {
          this.setState({ errorVenue: 'Enter Venue' });
        }
      }

    });
  }

  handleTextAreaChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value }, () => {
      if (name === "address" && this.state.address !== '') {
        this.setState({ errorAddress: '' });
      } else {
        if (name === "address") {
          this.setState({ errorAddress: 'Enter Address' });
        }
      }
      if (name === "aboutShow" && this.state.aboutShow !== '') {
        if (this.state.aboutShow.length > 600) {
          this.setState({ errorAboutShow: 'Only 600 characters is allowed' });
        } else {
          this.setState({ errorAboutShow: '' });
        }
      } else {
        if (name === "aboutShow") {
          this.setState({ errorAboutShow: 'Enter About the Mic' });
        }
      }
    });
  }

  _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];
    if (file) {
      if (!file.type.match('image.*')) {
      this.setState({
        errorFile: 'Upload your file only in image formate', file: [],
        imagePreviewUrl: undefined,
      });
    } else {
      reader.onload = (e) => {
        var img = document.createElement("img");
        img.onload = () => {
          var canvas = document.createElement('canvas');
          var ctx = canvas.getContext("2d");
          ctx.drawImage(img, 0, 0);
    
          var MAX_WIDTH = 300;
          var MAX_HEIGHT = 300;
          var width = img.width;
          var height = img.height;
    
          if (width > height) {
            if (width > MAX_WIDTH) {
              height *= MAX_WIDTH / width;
              width = MAX_WIDTH;
            }
          } else {
            if (height > MAX_HEIGHT) {
              width *= MAX_HEIGHT / height;
              height = MAX_HEIGHT;
            }
          }
          canvas.width = width;
          canvas.height = height;
          var ctx = canvas.getContext("2d");
          ctx.drawImage(img, 0, 0, width, height);
          var dataurl = canvas.toDataURL("image/png");
          this.setState({imagePreviewUrl: dataurl, file: file, errorFile: ''});
        }
        img.src = e.target.result;
      }
      // reader.onloadend = () => {
      //   this.setState({
      //     file: file,
      //     imagePreviewUrl: reader.result,
      //     errorFile: '',
      //   });
      // }
   reader.readAsDataURL(file)
    }
}
  }

  handleChangeTextarea(e) {
    const { value } = e.target;
    this.setState({ Description: value });
  }

  handleSavePost() {
    const { nameOfMic, venue, date, costType, DrinkType, selectedRadio, describe, address, aboutShow, Description, imagePreviewUrl, file, region, country, state_province, id, featured, mic_status, cost, errorCost } = this.state;
    if (nameOfMic === '') {
      this.setState({ errorNameOfMic: 'Enter Name Of Your Mic' });
      return;
    }
    if (venue === '') {
      this.setState({ errorVenue: 'Enter Venue' });
      return;
    }
    if (selectedRadio === '') {
      this.setState({ errorRadio: 'Select One Option' });
      return;
    } else {
      if (selectedRadio === 'No') {
        if (describe === '') {
          this.setState({ errorDescribe: 'Enter Description' });
          return;
        }
      }
    }
    if (address === '') {
      this.setState({ errorAddress: 'Enter Address' });
      return;
    }
    if (aboutShow === '') {
      this.setState({ errorAboutShow: 'Enter About the Mic' });
      return;
    }

    if (DrinkType === '') {
      this.setState({ errorForDrinkType: 'Select Drink Type' })
      return;
    }
    if (imagePreviewUrl === '') {
      this.setState({ errorFile: 'Upload Poster' });
      return;
    }
    if (errorCost !== '') {
      return;
    } 

    if (this.state.removedImage === true) {
      this.getImage().then((value) => {
   
        const fileName = this.state.file.name;
        var fileNew = new File([value], fileName, {lastModified: new Date(), type: 'image/*'});
          if (nameOfMic !== '' && venue !== '' && selectedRadio !== '' && address !== '' && aboutShow !== ''  && DrinkType !== '' && imagePreviewUrl !== '' && aboutShow.length < 600) {
            var formData = new FormData();
            formData.append('mic_name', nameOfMic);
            formData.append('producer', this.state.userdata.id);
            formData.append('mic_venue', venue);
            formData.append('address', address);
            formData.append('region', region.id);
            formData.append('state_province', state_province);
            // formData.append('mic_datetime', moment(date).format("YYYY-MM-DD[T]HH:mm:ss"));
            formData.append('mic_status', mic_status);
            formData.append('country', country);
            formData.append('about_mic', aboutShow);
            //formData.append('mic_cost_type', 'FREE');
            formData.append('mic_cost_type', '');
            formData.append('mic_cost', cost === null || cost === ""? 0 : cost)
            formData.append('open_to_all', selectedRadio === "Yes" ? true : false);
            formData.append('featured', featured);
            // formData.append('mic_entry_type', parseInt(costType.value));
            formData.append('mic_fees', DrinkType.value);
            formData.append('id', id);
            formData.append('poster_image', fileNew);
            formData.append('additional_description', Description);
            this.setState({ loading: true })
            editMicDetails(id, formData).then((result) => {
              if (result) {
                if (result.code === 200 && result.status === true) {
                  this.setState({ loading: false, removedImage: false })
                  this.props.hideModal();
                  this.getMic();
                  toast.info(result.message)
                } else if (result.code === 200 && result.status === false) {
                  this.setState({ loading: false });
                  toast.error(result.message)
                }
              }
            })
          }
        
            });      
    } else {
      if (nameOfMic !== '' && venue !== '' && selectedRadio !== '' && address !== '' && aboutShow !== '' && costType !== '' && DrinkType !== '' && imagePreviewUrl !== '' && aboutShow.length < 600) {
        var formData = new FormData();
        formData.append('mic_name', nameOfMic);
        formData.append('producer', this.state.userdata.id);
        formData.append('mic_venue', venue);
        formData.append('address', address);
        formData.append('region', region.id);
        formData.append('state_province', state_province);
        // formData.append('mic_datetime', moment(date).format("YYYY-MM-DD[T]HH:mm:ss"));
        formData.append('mic_status', mic_status);
        formData.append('country', country);
        formData.append('about_mic', aboutShow);
        //formData.append('mic_cost_type', 'FREE');
        formData.append('mic_cost_type', '');
        formData.append('mic_cost', cost === null || cost === "" ? 0  : cost)
        formData.append('open_to_all', selectedRadio === "Yes" ? true : false);
        formData.append('featured', featured);
        // formData.append('mic_entry_type', parseInt(costType.value));
        formData.append('mic_fees', DrinkType.value);
        formData.append('id', id);
        // formData.append('poster_image', imagePreviewUrl);
        formData.append('additional_description', Description);
        this.setState({ loading: true })
        editMicDetails(id, formData).then((result) => {
          if (result) {
            if (result.code === 200 && result.status === true) {
              this.setState({ loading: false, removedImage: false })
              this.props.hideModal();
              this.getMic();
              toast.info(result.message)
            } else if (result.code === 200 && result.status === false) {
              this.setState({ loading: false });
              toast.error(result.message)
            }
          }
        })
       }
    }

// var url = (window.webkitURL || window.URL).createObjectURL(blob);
// window.location.href = url;


  }

async getImage() {
  const croped = await this.cropper.getCroppedCanvas().toDataURL();
  // var base64Icon = `data:image/png;base64,${croped}`;
  var block = croped.split(";");
// Get the content type of the image
var contentType = block[0].split(":")[1];
  // var blob = this.b64toBlob(croped, contentType);
  var byteCharacters = atob(croped.replace(/^data:image\/(png|jpeg|jpg);base64,/, ''));
var byteNumbers = new Array(byteCharacters.length);
for (var i = 0; i < byteCharacters.length; i++) {
byteNumbers[i] = byteCharacters.charCodeAt(i);
}

var byteArray = new Uint8Array(byteNumbers);
var blob = new Blob([ byteArray ],{
 type : contentType,
});
return blob;
}


  getMic() {
    const myMicId = this.state.userdata.id;
    const { tabIndex } = this.props;
    var mic_status = '';
    if (this.props.tabIndex === 0) {
      mic_status = 'SAVED'
    } else if (this.props.tabIndex === 1) {
      mic_status = 'ALL'
    }
    getMyMic(myMicId, 1, mic_status).then((result) => {
      let micData = [];
      if (result && result.code === 200 && result.status === true) {
        this.props.getpostMicData(result.data);
      } else {

      }
    })
  }

  _crop() {
    // image in dataUrl
    if (typeof this.cropper.getCroppedCanvas() === "undefined") {
      return;
    }
    this.setState({
      imagePreviewUrl: this.cropper.getCroppedCanvas() && this.cropper.getCroppedCanvas().toDataURL()
    });
  }

  _remove() {
    this.setState({ imagePreviewUrl: null, file: '', removedImage: true })
  }
 render() {
    const { imagePreviewUrl, errorFile } = this.state;
    return (
      <Modal className="modal fade modal-custom" role="dialog" show={this.props.display} onHide={this.props.hideModal} >
        <div className="modal-header">
          <h4 className=""></h4>
          <button onClick={this.props.hideModal} type="button" className="close" data-dismiss="modal">&times;</button>
        </div>
        <div className="modal-body">
          <div className="modal-logo">
            <a href="#">
              <img src={blackLogo} alt="logo" />
            </a>
          </div>
          <div className="modal-heading dotted-gradient">
            <h3>Edit Your Mic</h3>
          </div>
          <div className="post-mic-form">
            <div className="row">

              <div className="col-lg-6">
                <div className="form-group">
                  <label className="has-float-label">
                    <input name="nameOfMic" value={this.state.nameOfMic} onChange={this.handleTextChange} type="text" placeholder="Name Of Mic" className="form-control" />
                    <span>Name Of Mic</span>
                  </label>
                  <label className="error">{this.state.errorNameOfMic}</label>
                </div>
              </div>

              <div className="col-lg-6">
                <div className="form-group">
                  <label className="has-float-label">
                    <input name="venue" value={this.state.venue} onChange={this.handleTextChange} type="text" placeholder="Venue" className="form-control" />
                    <span>Venue</span>
                  </label>
                  <label className="error">{this.state.errorVenue}</label>
                </div>
              </div>

              {/* <div className="col-lg-6">
                <div className="form-group">
                  <label className="has-float-label">
                    <div className="select2-div">
                      <Select
                        value={this.state.costType}
                        onChange={this.handleCostType}
                        options={options}
                        className="select-multiple"
                      />
                    </div>
                    <span>Cost Type</span>
                  </label>
                  <label className="error">{this.state.errorForCostType}</label>
                </div>
              </div> */}

              <div className="col-lg-6">
                <div className="form-group radio-checked">
                  <label className="has-float-label">
                    <input type="text" className="form-control text-area-100" name="cost" id="cost" placeholder="Enter Cost."
                      value={this.state.cost} onChange={this.handleCost} />
                    <span className="">Enter Cost</span>
                  </label>
                  <label className="error">{this.state.errorCost}</label>
                </div>
              </div>

              <div className="col-lg-6">
                <div className="form-group">
                  <label className="has-float-label">
                    <div className="select2-div">
                      <Select
                        value={this.state.DrinkType}
                        onChange={this.handleDrinkType}
                        options={this.state.optionForDrink}
                        className="select-multiple"
                      />
                    </div>
                    <span>Select Mic Type</span>
                  </label>
                  <label className="error">{this.state.errorForDrinkType}</label>
                </div>
              </div>

              {/* <div className="col-lg-12">
                <div className="form-group">
                  <label className="has-float-label">
                    <Flatpickr data-enable-time
                      options={{ dateFormat: 'H:i m/d/Y', minDate: new Date(), static: true }}
                      onChange={this.onChangeDate}
                      placeholder="Select Date"
                      className="form-control"
                      value={this.state.date}
                    />
                    <span>Date</span>
                  </label>
                  <label className="error">{this.state.errorDate}</label>
                </div>
              </div> */}

              <div className="col-lg-12">
                <div className="step-title">

                  <p>Is this mic open to all performers? </p>
                </div>
              </div>

              <div className="col-lg-12">
                <div className="radio-check">
                  {this.radioOption.map((data, index) => {
                    return <label key={index} className="container-radio">{data.name},{data.name === "No" ? <span>This mic is open only certian demographics.</span> : null}
                      <input type="radio" className="radio-no" name="radio" value={data.name}
                        checked={this.state.selectedRadio === data.name}
                        onChange={this.handleOnRadioChange}
                      />
                      <span className="checkmark"></span>
                    </label>
                  })}
                  <label className="error">{this.state.errorRadio}</label>
                </div>
              </div>

              <div className="col-lg-12">
                <div className="form-group radio-checked" style={{ display: this.state.displayText }}>
                  <label className="has-float-label">
                    <textarea className="form-control text-area-100" name="describe" rows="4" id="describe" placeholder="please describe requirments to perform."
                      value={this.state.describe} onChange={this.handleTextarea} > </textarea>
                    <span className="">Describe requirments</span>
                  </label>
                  <label className="error">{this.state.errorDescribe}</label>
                </div>
              </div>


              <div className="col-lg-12">
                <div className="form-group">
                  <label className="has-float-label">
                    <textarea name="address" value={this.state.address} onChange={this.handleTextAreaChange} className="form-control" rows="4" placeholder="Address" aria-invalid="false"></textarea>
                    <span>Address</span>
                  </label>
                  <label className="error">{this.state.errorAddress}</label>
                </div>
              </div>

              <div className="col-lg-12">
                <div className="form-group">
                  <label className="has-float-label">
                    <textarea name="aboutShow" value={this.state.aboutShow} onChange={this.handleTextAreaChange} className="form-control" rows="4" placeholder="About the Mic" aria-invalid="false"></textarea>
                    <span>About the Mic</span>
                  </label>
                  <label className="error">{this.state.errorAboutShow}</label>
                </div>
              </div>

              <div className="col-lg-3 col-md-3 col-sm-6">
                <div className="form-file">
                  {imagePreviewUrl === null ?
                    <input type="file" className="inputfile" name="your_picture" id="your_picture"
                      accept="image/*" onChange={(e) => this._handleImageChange(e)} />
                    : null}
                  <label htmlFor="your_picture">
                    <figure>

                      {this.state.removedImage === true ? <Cropper
                        ref={cropper => {
                          this.cropper = cropper;
                        }}
                        src={imagePreviewUrl !== null ? imagePreviewUrl : Mic}
                        style={{ width: 200, height: 130 }}
                        guides={true}
                        aspectRatio={17/9}
                      /> :
                        <img src={imagePreviewUrl !== null ? imagePreviewUrl : Mic} alt="" style={{ width: 180, height: 130 }} className="your_picture_image" />}
                    </figure>

                    {imagePreviewUrl === null ?
                      <span className="file-button">Upload Poster</span>
                      :
                      this.state.removedImage === true ?
                        <div className="crop-img-div">

                          <span className="file-button file-input50 cropimg-button" onClick={this._crop.bind(this)}>Crop Image</span>
                          <span className="file-button file-input50 remove-button" onClick={this._remove.bind(this)}>Remove</span>
                        </div>
                        :
                        <div className="crop-img-div">
                          <span className="file-button file-input100 remove-button" onClick={this._remove.bind(this)}>Remove</span>
                        </div>
                    }


                  </label>
                </div>
                <label className="error">{errorFile}</label>
              </div>
              <div className="col-lg-9  col-md-9 col-sm-6">
                <div className="form-group">
                  <label className="has-float-label">
                    <textarea value={this.state.Description} onChange={this.handleChangeTextarea} className="form-control text-area mt-0" rows="4" name="Description" id="Description" placeholder="Additional description"
                      style={{ minHeight: "160px" }}></textarea>
                    <span>Additional description</span>
                  </label>
                </div>
              </div>

     


              <div className="col-lg-12 text-right">
                <div className="btn-groups">
                  <button onClick={this.handleSavePost} className="btn btn-login">Save Post {this.state.loading === true ? <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                  </div> : null}</button>
                </div>
              </div>

            </div>
          </div>
        </div>
      </Modal>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  getpostMicData: data => dispatch(getpostMicData(data)),
});

// const mapStateToProps = state => ({
//   tabName: state.dashboard.tabName,
// });
export default withRouter(
  connect(
    null,
    mapDispatchToProps,
  )(EditPostModal),
);