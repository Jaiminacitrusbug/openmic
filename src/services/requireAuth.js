import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import history from './BrowserHistory';

export default function (ComposedComponent) {
  class Authentication extends Component {
    componentWillUpdate(nextProps) {
      const {
        authUser: { authenticated },
      } = nextProps;
      if (!authenticated) {
        history.push('/');
      }
    }

    render() {
      const {
        authUser: { authenticated },
      } = this.props;
      
      if (!authenticated) {
        return null;
      }
      return <ComposedComponent {...this.props} />;
    }
  }

  Authentication.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    authUser: PropTypes.object.isRequired,
  };

  function mapStateToProps({ authUser }) {
    return { authUser };
  }

  return connect(mapStateToProps)(Authentication);
}
