import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from 'yup';
import { signup, resendSignUpMail } from '../../api/signUpApi';
import { ToastContainer, toast } from 'react-toastify';
import { Modal } from 'react-bootstrap';
import email from '../../assets/images/email.png';
import { connect } from 'react-redux';
import { renderLoginModal } from './../../actions/postMicFormActions';
import { withRouter } from 'react-router-dom';


class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      renderReSendMailModal: false,
      loader: false,
      email: '',
      loaderMail: false,
    }
    this.handleRenderSendMail = this.handleRenderSendMail.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.handleLoginModal = this.handleLoginModal.bind(this);
  }

  handleRenderSendMail() {
    this.setState({ renderReSendMailModal: false });
  }

  handleEmail() {
    const { email } = this.state;
    if (email !== '') {
      this.setState({ loaderMail: true});
      var formData = new FormData();
      formData.append('email', email);
      resendSignUpMail(formData).then((result) => {
        if (result) {
          if (result.code === 200 && result.status === true) {
            toast.success(result.message);
            this.setState({ loaderMail: false});
          } else if (result.code === 200 && result.status === false) {
            toast.error(result.message);
            this.setState({ loaderMail: false});
          }
        } else {
          this.setState({ loaderMail: false});
        }
      })
    } 
    
  
  }
  handleLoginModal() {
    const renderLogin = true;
    this.props.renderLoginModal(renderLogin);
  }

  render() {
    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
    return (
      <div className="middle-container">
        <section className="postmic-secttion">
          <Formik
            initialValues={{
              firstName: '',
              lastName: '',
              email: '',
              password: '',
              fieldName: ''
            }}
            validationSchema={Yup.object().shape({
              firstName: Yup.string().trim().strict(false)
                .required('First Name is required'),
              lastName: Yup.string().trim().strict(false)
                .required('Last Name is required'),
              email: Yup.string().trim().strict(false)
                .email('Email is invalid')
                .required('Email is required'),
              contactNo: Yup.string().trim().strict(false)
                .matches(phoneRegExp, 'Contact number is not valid')
                .required('Contact number is required'),
              password: Yup.string().trim().strict(false)
                .min(6, 'Password must be at least 6 characters')
                .required('Password is required'),
              cPassword: Yup.string().trim().strict(false)
                .oneOf([Yup.ref('password'), null], 'Passwords and Confirm Password must match')
                .required('Confirm Password is required'),
            })}
            onSubmit={fields => {
              const registerData = {
                first_name: fields.firstName,
                last_name: fields.lastName,
                email: fields.email,
                password: fields.password,
                profile_avtar: 1,
                contact: fields.contactNo,
              };
              this.setState({ loader: true, email: fields.email });
              signup(registerData).then((result) => {
                if (result) {
                  if (result.code === 200 && result.status === true) {
                    toast.success(result.message);
                    this.setState({ renderReSendMailModal: true, loader: false });
                  } else if (result.code === 200 && result.status === false) {
                    toast.error(result.message);
                    this.setState({ loader: false });
                  }
                }
              })
            }}
            render={({ errors, status, touched }) => (
              <Form>
                <ToastContainer/>
                <div className="container">
                  <div className="post-mic-form">
                    <div className="row">
                      <div className="col-lg-12">
                        <div className="post-mic-heading">

                          <h3 >Sign up</h3>
                          {/* <p>Create free account delete your mic when the venue goes out of business.</p> */}
                          <p>First, you need to open a free account so can you post, edit and manage the details of your Open Mic. If you have already Have account  <a href="#" onClick={this.handleLoginModal} data-toggle="modal" data-target="#login-post-mic" className="login-txt">LOGIN</a></p>
                        </div>
                      </div>

                      <div className="col-lg-6">
                        <div className="form-group">
                          <label className="has-float-label">
                            {/* <input className="form-control" type="text" placeholder="Fist Name" /> */}
                            <Field
                              type="text"
                              name="firstName"
                              placeholder="First Name"
                              className="form-control"
                            />
                            <span>Fist Name *</span>
                          </label>
                          <ErrorMessage name="firstName" component="span" className="error" />
                        </div>
                      </div>
                      <div className="col-lg-6">
                        <div className="form-group">
                          <label className="has-float-label">
                            {/* <input className="form-control" type="text" placeholder="Last Name" /> */}
                            <Field
                              type="text"
                              name="lastName"
                              placeholder="Last Name"
                              className="form-control"
                            />
                            <span>Last Name *</span>
                          </label>
                          <ErrorMessage name="lastName" component="span" className="error" />
                        </div>
                      </div>

                      <div className="col-lg-6">
                        <div className="form-group">
                          <label className="has-float-label">
                            {/* <input className="form-control" type="text" placeholder="Email Addres" /> */}
                            <Field
                              type="text"
                              name="email"
                              placeholder="Email Address"
                              className="form-control"
                            />
                            <span>Email Address *</span>
                          </label>
                          <ErrorMessage name="email" component="span" className="error" />
                        </div>
                      </div>

                      <div className="col-lg-6">
                        <div className="form-group">
                          <label className="has-float-label">
                            {/* <input className="form-control" type="text" placeholder="Filed Name" /> */}
                            <Field
                              type="text"
                              name="contactNo"
                              placeholder="Contact No"
                              className="form-control"
                            />
                            <span>Contact No *</span>
                          </label>
                          <ErrorMessage name="contactNo" component="span" className="error" />
                        </div>
                      </div>

                      <div className="col-lg-6">
                        <div className="form-group">
                          <label className="has-float-label">
                            {/* <input className="form-control" type="password" placeholder="Password" /> */}
                            <Field
                              type="password"
                              name="password"
                              placeholder="Password"
                              className="form-control"
                            />
                            <span>Password *</span>
                          </label>
                          <ErrorMessage name="password" component="span" className="error" />
                        </div>
                      </div>
                      <div className="col-lg-6">
                        <div className="form-group">
                          <label className="has-float-label">
                            {/* <input className="form-control" type="password" placeholder="Password" /> */}
                            <Field
                        type="password"
                        name="cPassword"
                        placeholder="Confirm password"
                        className="form-control"
                      />
                            <span>Confirm Password *</span>
                          </label>
                          <ErrorMessage name="cPassword" component="span" className="error" />
                        </div>
                      </div>


                    </div>

                    <div className="login-form-footer">
                      <div className="row">

                        <div className="col-lg-12 text-right">
                          <div className="btn-groups">
                            {/* <a href="#" className="btn">Not Now</a> */}
                            <button type="submit" disabled={this.state.loader} className="btn btn-login" data-toggle="modal" data-target="#login-welcome">
                              Sign Up {this.state.loader === true ? <span className="spinner-border spinner-border-sm"></span> : null}</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Form>
            )}
          />
        </section>
        <Modal className="modal fade modal-custom" show={this.state.renderReSendMailModal} hide={this.handleRenderSendMail}>
          <div className="modal-header">
            <button type="button" className="close" data-dismiss="modal" onClick={this.handleRenderSendMail}>&times;</button>
          </div>
          <div className="modal-body">
            <div className="modal-logo">
            
                <img src={email} alt="logo" />
            </div>
            <div className="row">
              <div className="col-lg-12">
                <div className="resend-mail">
                  <p>Congratulations!” Welcome to the Open Mic List.</p>
                  <p>We sent you a link to <span style={{ color: '#000000', fontWeight: '600' }}>Confirm your email address.</span></p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12 text-center text-top">
                <div className="btn-groups">
                  <button type="submit" disabled={this.state.loaderMail} className="btn btn-login" onClick={this.handleEmail}>Resend Mail {this.state.loaderMail === true ? <span className="spinner-border spinner-border-sm"></span> : null}</button>
                </div>
              </div>
              <div className="col-lg-12 text-center text-top">
                <div className="login-form account-text">
                  <p><a href="/">Go to home</a></p>
                </div>
              </div>
            </div>
          </div>
          <div className="modal-footer" style={{ justifyContent: 'center' }}>
            <div className="row">
              <div className="col-lg-12 text-center">
                {/* <div className="resend-mail"> */}
                <div className="account-text text-center">
                  <p>If you never received an email from us, there’s a few places the email may have landed:<br />
                    Please check your Spam, junk, and bulk mail folders: Sometimes our emails are blocked or filtered as<br /> spam by certain email providers or internet service providers (ISPs). You may also click the Resend Mail button above.</p>
                </div>
                {/* </div> */}
              </div>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  renderLoginModal: renderLogin => dispatch(renderLoginModal(renderLogin))
});

export default withRouter(
  connect(
    null,
    mapDispatchToProps
  )(SignUp));
