import React, { Component } from 'react';
import Select from 'react-select';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { setStepCount, addCostType } from '../../../actions/postMicFormActions';
import { getFees, getMicCategoty, getTimeSlot } from '../../../api/getMicApi'; 
const options = [
  { value: 'FREE', label: 'Completly Free' },
  { value: 'PAID', label: 'Paid' },
];

class CostOfPerformence extends Component {
  constructor(props) {
    super(props);
    this.state = {
      costType: this.props.costType && this.props.costType.cost !== ''? this.props.costType.cost  : '',
      errorForCostType: '',
      DrinkType: this.props.costType && this.props.costType.drink !== ''? this.props.costType.drink  : '',
      errorForDrinkType: '',
      optionForDrink: [],
     // optionForTimeSlot: [],
      category: [],
      selectedCategory: this.props.costType && this.props.costType.selectedCategory !== ''?  this.props.costType.selectedCategory : false,
      displayText: this.props.costType && this.props.costType.cost && this.props.costType.cost.value === "PAID"? 'block' : 'none',
      errorCost: '',
      cost: this.props.costType && this.props.costType.costAmount && this.props.costType.costAmount !== ''? this.props.costType.costAmount : 0,
     // timeSlot: this.props.costType && this.props.costType.timeSlot !== ''? this.props.costType.timeSlot  : '',
      //errorTimeSlot: ''
    };

    this.handleCostType = this.handleCostType.bind(this);
    this.handleDrinkType = this.handleDrinkType.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handlePrevious = this.handlePrevious.bind(this);
    this.handleCategory = this.handleCategory.bind(this);
    this.handleCost = this.handleCost.bind(this);
   // this.handleTimeSlot = this.handleTimeSlot.bind(this);
  }

  
	componentDidMount() {
    this.getMicType();
    //this.getMicTimeSlot();
    this.getCategory();
  }
  
  getMicType() {
		getFees().then((result) => {
			if (result && result.code === 200 && result.status === true) {
				let fees = [];
			result.data.map((item) => {
				fees.push({
					value:item.id,
					label: item.fees,
				});
			});
			this.setState({ optionForDrink: fees });
			}
		})
  }

  // getMicTimeSlot() {
  //   getTimeSlot().then((result) => {
	// 		if (result && result.code === 200 && result.status === true) {
	// 			let timeSlot = [];
	// 		result.data.map((item) => {
	// 			timeSlot.push({
	// 				value:item.id,
	// 				label: item.name,
	// 			});
	// 		});
	// 		this.setState({ optionForTimeSlot: timeSlot });
	// 		}
	// 	})
  // }
  
  getCategory() {
    getMicCategoty().then((result) => {
      if (result && result.code === 200 && result.status === true) {
       let categoryOption = [];
       result.data.map((item) => {
        categoryOption.push({
          value: item.id,
          label: item.category,
        });
       });
       this.setState({ category: categoryOption });
      }
    })
  }

  handleCostType(costType) {
    this.setState({ costType, errorForCostType: '' }, () => {
      if (costType.value === "PAID") {
        this.setState({ displayText: 'block'})
      } else {
        this.setState({ displayText: 'none'})
      }
    });
  }


  handleDrinkType(DrinkType) {
    this.setState({ DrinkType, errorForDrinkType: '' }, () => {

    });
  }

  // handleTimeSlot(timeSlot) {
  //   this.setState({ timeSlot, errorTimeSlot: ''})
  // }

  handleSubmit() {
    if(this.state.DrinkType === undefined || this.state.DrinkType === '' || this.state.DrinkType === null) {
      this.setState({ errorForDrinkType: 'Select one option.'})
      return;
    }
    // if(this.state.timeSlot === undefined || this.state.timeSlot === '' || this.state.timeSlot === null) {
    //   this.setState({ errorTimeSlot: 'Select one option.'})
    //   return;
    // }
  
        const count = 4;
        this.props.setStepCount(count);
        const costType = {
          cost: this.state.costType,
          drink: this.state.DrinkType,
         // timeSlot: this.state.timeSlot,
          selectedCategory: this.state.selectedCategory,
          costAmount: this.state.cost
        };
        this.props.addCostType(costType);
  }

  handlePrevious() {
    if(this.state.DrinkType === undefined || this.state.DrinkType === '' || this.state.DrinkType === null) {
      this.setState({ errorForDrinkType: 'Select one option.'})
      return;
    }
    // if(this.state.timeSlot === undefined || this.state.timeSlot === '' || this.state.timeSlot === null) {
    //   this.setState({ errorTimeSlot: 'Select one option.'})
    //   return;
    // }
    const costType = {
        cost: this.state.costType,
        drink: this.state.DrinkType,
        //timeSlot: this.state.timeSlot,
        selectedCategory: this.state.selectedCategory,
        costAmount: this.state.cost
      };
      this.props.addCostType(costType);
      const count = 2;
      this.props.setStepCount(count);
  }

  handleCategory(event) {
    this.setState({ selectedCategory: event.target.checked });
  }

  handleCost(e) {
    this.setState({ [e.target.name]: e.target.value }, () => {
      const regx = /^-?\d*(\.\d+)?$/
      var valid = regx.test(this.state.cost);
      if (valid === false) {
        this.setState({ errorCost: 'Enter valid cost'})
      } else {
        this.setState({ errorCost: ''})
      }
    });
  }



  render() {
    return (
      <fieldset>
        <div className="row">
          <div className="col-lg-12">
            <div className="step-title">
            {this.props.authenticated === false?
                 <h4>Step 4 of 7</h4>
                :
                <h4>Step 3 of 6</h4>
              }
              <p>Cost of Performance? </p>
            </div>
          </div>
            </div>
            <div className="row">
          <div className="col-lg-6">
            <div className="form-group">
              <label className="has-float-label">
                <div className="select2-div">
                  <Select
                    value={this.state.DrinkType}
                    onChange={this.handleDrinkType}
                    options={this.state.optionForDrink}
                    className="select-multiple"
                    placeholder="Select Mic Drink Type *"
                  />
                </div>
                {/* <input type="text" placeholder="Cost Type" className="form-control" /> */}
                <span>Select Mic Drink Type*</span>
              </label>
  
              <label className="error">{this.state.errorForDrinkType}</label>
            </div>
          </div>
          </div>
         
          {/* <div className="row">
       <div className="col-lg-6">
            <div className="form-group">
              <label className="has-float-label">
                <div className="select2-div">
                  <Select
                    value={this.state.timeSlot}
                    onChange={this.handleTimeSlot}
                    options={this.state.optionForTimeSlot}
                    className="select-multiple"
                    placeholder="Select Time Slot *"
                  />
                </div>
                
                <span>Select Mic Time Slot*</span>
              </label>
  
              <label className="error">{this.state.errorTimeSlot}</label>
            </div>
          </div>
          </div> */}
               
        <div className="row">
        <div className="col-lg-6">
            <div className="form-group radio-checked">
          
              <label className="has-float-label">
              <span className="dollar-sign">$</span>
             <input type="text" className="form-control text-area-100 dollar-sign-pad-left" name="cost" id="cost" placeholder="Enter Cost."
                  value={this.state.cost} onChange={this.handleCost} />
                <span className="dollar-sign-pad-20">Enter Cost</span>
              </label>
              <label className="error">{this.state.errorCost}</label>
            </div>
          </div>
          </div>
      
        <div className="row">
      <div className="col-lg-12">
          <div className="btn-groups-steps">
            {/* <a href="/postMicForm" className="btn btn-login">Agree</a> */}
            <button onClick={this.handlePrevious} className="btn btn-prev">Previous</button>
            <button onClick={this.handleSubmit} className="btn btn-next">Next</button>
          </div>
          </div>
      </div>
      </fieldset>
    );
  }
}


const mapDispatchToProps = dispatch => ({
  setStepCount: count => dispatch(setStepCount(count)),
  addCostType: costType => dispatch(addCostType(costType)),
});
const mapStateToProps = state => ({
  count: state.accountDetails.count,
  costType: state.accountDetails.costType,
  authenticated: state.authUser.authenticated,
});
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(CostOfPerformence),
);
