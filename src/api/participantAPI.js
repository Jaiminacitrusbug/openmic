import axios from 'axios'
import {errorHandle } from './errorHandling'
import {APP_CONFIG} from '../constants/config';
//import {header}from './header';

export function addParticipant(participantData) {
  try {
    return axios.post(APP_CONFIG.API_URL+'performer', participantData, {
      headers:{
        'Content-Type': 'application/json',
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}

export function getParticipant(producerId, page) {
  try {
    return axios.get(APP_CONFIG.API_URL+'performer/' + producerId + '?page=' + page, {
      headers:{
        'Content-Type': 'application/json',
        'authorization': sessionStorage.getItem('token'),
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}

export function getParticipantDetails(participantId) {
  try {
    return axios.get(APP_CONFIG.API_URL+'performer-detail/' + participantId, {
      headers:{
        'Content-Type': 'application/json',
        'authorization': sessionStorage.getItem('token'),
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}

export function addPayment(paymentData) {
  try {
    return axios.post(APP_CONFIG.API_URL+'payment/', paymentData, {
      headers:{
        'Content-Type': 'application/json',
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}