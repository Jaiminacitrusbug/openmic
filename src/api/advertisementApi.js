import axios from 'axios'
import {errorHandle } from './errorHandling'
import {APP_CONFIG} from '../constants/config';

export function getAdvertisement(type) {
    try {
      return axios.get(APP_CONFIG.API_URL+'advertisement_link/?type=' + type, {
        headers:{
          'Content-Type': 'application/json',
        },
    }).then(response => {
            return response.data;
        }).catch(error => {
            return errorHandle(error);
        });
    } catch (err) { console.log('Err::: ', err); return err }
  }

  export function getBanner(type) {
    try {
      return axios.get(APP_CONFIG.API_URL+'banner_link/?type=' + type, {
        headers:{
          'Content-Type': 'application/json',
        },
    }).then(response => {
            return response.data;
        }).catch(error => {
            return errorHandle(error);
        });
    } catch (err) { console.log('Err::: ', err); return err }
  }