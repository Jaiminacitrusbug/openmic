import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from 'yup';
import history from '../../services/BrowserHistory';
import { forgetPasswordValidation, changePasswrod } from '../../api/loginAPI';
import { ToastContainer, toast } from 'react-toastify';
class SetNewPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: '',
      message: '',
      userId: '',
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.setState({ token: this.props.match.params.token },() => {
      forgetPasswordValidation(this.state.token).then((result) => {
        if (result) {
          if (result.code === 200 && result.status === true) {
            this.setState({ message: result.message, userId: result.data });
          }  else if (result.code === 200 && result.status === false) {
            toast.error(result.message);
            this.setState({ message: result.message });
          }
        }
      })
    });
  }

  handleSubmit(values) {
    const { userId } = this.state;
    const passowrdData = {
      new_password: values.password,
    };
    changePasswrod(userId, passowrdData).then((result) => {
      if (result) {
        if (result.code === 200 && result.status === true) {
          toast.info(result.message);
          setTimeout(() => {
            history.push('/');
          }, 1000);
        } else if(result.code === 200 && result.status === false) {
          toast.error(result.message);
        }
      }
    });
  }

  render() {
    return (
      <div className="middle-container">
        <ToastContainer/>
        <section className="postmic-secttion main">
          <div className="container">
            <div className="post-mic-form post-mic-form-steps">

              <div className="col-lg-12">
                <div className="post-mic-heading">
                  <h3>{this.state.message}</h3>
                </div>
              </div>
              <Formik
                initialValues={{
                  password: '',
                  cPassword: '',
                }}
                validationSchema={Yup.object().shape({
                  password: Yup.string()
                    .required('Password is required'),
                  cPassword: Yup.string()
                  .oneOf([Yup.ref('password'), null], 'Passwords must match')
                    .required('Confirm Password is required'),
                })}
                onSubmit={this.handleSubmit}
              >
                {(values) => (
                  <Form>
                     <div className="col-lg-12">
                      <div className="form-group">
                        <label className="has-float-label">
                          <Field
                            type="password"
                            name="password"
                            placeholder="Password *"
                            className="form-control"
                          />
                          <span>Password *</span>
                        </label>
                        <ErrorMessage name="password" component="span" className="error" />
                      </div>
                    </div>
                    <div className="col-lg-12">
                      <div className="form-group">
                        <label className="has-float-label">
                          <Field
                            type="password"
                            name="cPassword"
                            placeholder="Confirm Password *"
                            className="form-control"
                          />
                          <span>Confirm Password *</span>
                        </label>
                        <ErrorMessage name="cPassword" component="span" className="error" />
                      </div>
                    </div>
                    <div className="col-lg-12 text-right">
                      <div className="btn-groups">
                        <button type="submit" className="btn btn-login">Submit</button>
                      </div>
                    </div>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  });
  const mapStateToProps = state => ({
  });
  
  export default withRouter(
   connect(
     mapStateToProps,
     mapDispatchToProps,
   )(SetNewPassword),
  );