import React, { Fragment } from 'react';
import { Router, Route, Redirect, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import history from './services/BrowserHistory';
import { store } from './services/Redux';
import Main from './pages/Main';
import PostDetails from './pages/PostDetails';
import ComedyFestival from './pages/ComedyFestivals/ComedyFestivals';
import Signup from './pages/Signup';
import Thanks from './pages/Thanks';
import PostYourMic from './pages/PostYourMic';
import PostMicForm from './pages/PostMicForm';
import Dashboard from './pages/DashBoard/DashBoard';
import emailConfirmation from './pages/EmailConfirmation';
import forgetPassword from './pages/ForgetPassword';
import setPassword from './pages/SetNewPassword';
import ContactUs from './pages/ContactUs';
import AuthService from './services/authServices';
import requireAuth from './services/requireAuth';
import Payment from './pages/Payment/Payment';

class App extends React.Component {
  componentDidMount() {
    AuthService.checkAuthentication();
   }
  render() {
    return (
      <Provider store={store}>
        <Fragment>
          <Router history={history}>
            <Switch>
              <Route exact path="/" component={Main} />
              <Route exact path="/postDetails" component={PostDetails} />
              <Route exact path='/comedyFestival' component={ComedyFestival} />
              <Route exact path='/signup' component={Signup} />
              <Route exact path='/thanks' component={Thanks} />
              <Route exact path='/postYourMic' component={PostYourMic} />
              <Route exact path='/post-mic/form' component={PostMicForm} />
              <Route exact path='/dashboard' component={requireAuth(Dashboard)} />
              <Route exact path='/email-confirmation/:token' component={emailConfirmation} />
              <Route exact path='/fpassword' component={forgetPassword} />
              <Route exact path='/forgetPassword/:token' component={setPassword} />
              <Route exact path='/contactUs' component={ContactUs} />
              <Route exact path='/payment' component={Payment} />
              <Route render={() => <Redirect to="/" />} />
            </Switch>
          </Router>
        </Fragment>
      </Provider>
    );
  }
}

export default App;
