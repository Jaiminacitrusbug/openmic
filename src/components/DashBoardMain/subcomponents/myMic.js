import React, { Component } from 'react';
//import draftImg from '../../../assets/images/grid-images/img-3.svg';
import EditPostModal from './EditPostModal';
import DeletePostModal from './DeletePostModal';
import { getMyMic, changeMicStatus} from '../../../api/getMicApi';
//import moment from 'moment';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Mic from '../../../assets/images/micBig.png'
import { toast } from 'react-toastify';
import ReactPaginate from 'react-paginate';

class MyMic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editId: '',
      myMic: [],
      renderEditModal: false,
      renderDeleteModal: false,
      userdata: JSON.parse(sessionStorage.getItem('user')),
      page:1,
      totalDealsCount:1,
      pageCount:1,
    }
    this.handleEditModal = this.handleEditModal.bind(this);
    this.handleCloseEditModal = this.handleCloseEditModal.bind(this);
    this.handleDeleteModal = this.handleDeleteModal.bind(this);
    this.handleCloseDeleteModal = this.handleCloseDeleteModal.bind(this);
    this.handlePublishButton = this.handlePublishButton.bind(this);
    this.handleUnPublishButton = this.handleUnPublishButton.bind(this);
  }

  componentDidMount() {
    this.getMic();
  }

  componentDidUpdate(prevProps) {
    
    const { postMicData } = this.props;
    if(prevProps.postMicData !== postMicData) {
      const myMicId = this.state.userdata.id;
      let micData = [];
      postMicData && postMicData.map((item) => {

        micData.push({
          poster_image_url: item.poster_image_url,
          mic_name: item.mic_name,
          mic_datetime: item.mic_datetime,
          mic_entry_type: item.mic_entry_type !== null ? item.mic_entry_type.entry_type : '',
          address: item.address,
          about_mic: item.about_mic,
          mic_venue: item.mic_venue,
          id: item.id,
          mic_status: item.mic_status,
          mic_time: item.mic_time,
          region: item.region.region,
          mic_day_name: item.mic_day_name,
          mic_cost_type: item.mic_cost_type,
          mic_cost: item.mic_cost,
          timeSlot: item.timeslot !== null ? item.timeslot.name : '',
        });
      });
      this.setState({ myMic: micData }, () => {
      });
    }
  }

  getMic() {
    const myMicId = this.state.userdata.id;
    var mic_status = '';
    if (this.props.tabIndex === 0) {
      mic_status = 'SAVED'
    } else if (this.props.tabIndex === 1) {
      mic_status='ALL'
    }
    getMyMic(myMicId, this.state.page, mic_status).then((result) => {
      let micData = [];
      if (result && result.code === 200 && result.status === true) {
        result.data && result.data.map((item) => {
          console.log("time in my mics..",item.mic_time);
          micData.push({
            poster_image_url: item.poster_image_url,
            mic_name: item.mic_name,
            mic_datetime: item.mic_datetime,
            mic_entry_type: item.mic_entry_type !== null ? item.mic_entry_type.entry_type : '',
            address: item.address,
            about_mic: item.about_mic,
            mic_venue: item.mic_venue,
            id: item.id,
            mic_status: item.mic_status,
            mic_time: item.mic_time,
            region: item.region.region,
            mic_day_name: item.mic_day_name,
            mic_cost_type: item.mic_cost_type,
            mic_cost: item.mic_cost,
            timeSlot: item.timeslot !== null ? item.timeslot.name : '',
          });
        });
        this.setState({ myMic: micData, myMicDetailsData: result.data, totalDealsCount: result.total_count, pageCount: (result.total_count / result.page_size) });
      } else {

      }
    })
  }

  handleEditModal(index) {
    this.setState({ renderEditModal: true, editId: index });
  }

  handleCloseEditModal() {
    this.setState({ renderEditModal: false });
  }

  handleDeleteModal(index) {
    this.setState({ renderDeleteModal: true, editId: index });
  }

  handleCloseDeleteModal() {
    this.setState({ renderDeleteModal: false });
  }

  handlePublishButton(index) {
    const status = {
      status_value: 'ACTIVE'
    };
    changeMicStatus(index,status).then((result) => {
      if (result) {
        if (result.code === 200 && result.status === true) {
          toast.info(result.message);
          this.getMic();
        }
      }
    }); 
  }

  handleUnPublishButton(index) {
    const status = {
      status_value: 'SAVED'
    };
    changeMicStatus(index,status).then((result) => {
      if (result) {
        if (result.code === 200 && result.status === true) {
          toast.info(result.message);
          this.getMic();
        }
      }
    }); 
  }


  renderPagination() {
     if (this.state.totalDealsCount > 8) {
      return (
        <ReactPaginate previousLabel={"previous"}
        nextLabel={"next"}
        breakLabel={<a href="#">...</a>}
        breakClassName={"break-me"}
        pageCount={this.state.pageCount}
        onPageChange={this.handlePageClick}
        containerClassName={"pagination"}
        pageClassName={"page-item"}
        pageLinkClassName={"page-link"}
        previousClassName={"page-item"}
        nextClassName={"page-item"}
        previousLinkClassName={"page-link"}
        nextLinkClassName={"page-link"}
        // subContainerClassName={"pages pagination"}
        activeClassName={"active"}
        disabledClassName={"page-item disabled"}
        />
      )
    }
    else {
      return null
    }
  }

  handlePageClick = (data) => {
    let selected = data.selected;
    let offset
    if (selected !== 0) {
      offset = Math.ceil(selected * this.state.perPage);
    }
    else {
      offset = Math.ceil(1 * this.state.perPage);
    }
    this.setState({ page: selected + 1, offset: offset, loaded: false }, () => {
      this.getMic();
    }); 
  }

  renderPostMic(myMic) {
  return  <div className="grid-layout">
      <div className="row">
      {myMic && myMic.length === 0 ? <div className="grid-list col-lg-12 col-md-12 col-sm-12" style={{ margin: "10px", fontSize: "24px" }}>No Event Found</div> : null}
        {myMic && myMic.map((data, index) => {
          if (this.props.tabIndex === 0 ) {
            if (data.mic_status === "SAVED") {
              return <div key={index} className=" col-lg-4 col-md-6 col-sm-6">
              <div className="grid-box ">
                <a href="#">
                  <div className="img-box">
                    <img src={data.poster_image_url !== null? data.poster_image_url : Mic} alt="product-1" />
                  </div>
                  <div className="content-box">
                    <h3>{data.mic_name}</h3>
                    <p><i className="material-icons"> access_time </i>{data.mic_time}, {data.timeSlot}, {data.mic_day_name}</p>
                    <p><i className="material-icons"> location_on </i>{data.mic_venue}, {data.region}</p>
                    <p className="address">{data.address}</p>
                    <p><i className="material-icons">payment</i>{data.mic_cost_type}{data.mic_cost_type === "PAID"? ',' : ''}{data.mic_cost_type === "PAID"? '$' : ''}{data.mic_cost}</p>
                    
                  </div>
                  <div className="content-box2">
                    <h4>Best About the Mic</h4>
                    <p>{data.about_mic}</p>
                  </div>
                </a>
                <div className="gridbox-footer">
                  <div className="publish-mic btn-div">
                   {data.mic_status === "SAVED"?
                    <a href="#" className="btn btn-text" onClick={() => this.handlePublishButton(data.id)} >Publish</a>
                    : 
                    <a href="#" className="btn btn-text" onClick={() => this.handleUnPublishButton(data.id)} >Unpublish</a>
                   }
                  </div>
                  <div className="edit-mic btn-div">
                    <a href="#" onClick={() => this.handleEditModal(data.id)} className="btn btn-text" >Edit mic</a>
                  </div>
                  <div className="delete-mic btn-div">
                    <a href="#" className="btn btn-text" onClick={() => this.handleDeleteModal(data.id)} >Delete</a>
                  </div>
                </div>
              </div>
            </div>
            }
          }  else {
            return <div key={index} className=" col-lg-4 col-md-6 col-sm-6">
            <div className="grid-box ">
              <a href="#">
                <div className="img-box">
                  <img src={data.poster_image_url !== null? data.poster_image_url : Mic} alt="product-1" />
                </div>
                <div className="content-box">
                  <h3>{data.mic_name}</h3>
                  <p><i className="material-icons"> access_time </i>{data.mic_time}, {data.timeSlot}, {data.mic_day_name}</p>
                  <p><i className="material-icons"> location_on </i>{data.mic_venue}, {data.region}</p>
                  <p className="address">{data.address}</p>
                  <p><i className="material-icons">payment</i>{data.mic_cost_type}{data.mic_cost_type === "PAID"? ',' : ''}{data.mic_cost_type === "PAID"? '$' : ''}{data.mic_cost}</p>
                  
                </div>
                <div className="content-box2">
                  <h4>Best About The Mic</h4>
                  <p>{data.about_mic}</p>
                </div>
              </a>
              <div className="gridbox-footer">
                <div className="publish-mic btn-div">
                 {data.mic_status === "SAVED"?
                  <a href="#" className="btn btn-text" onClick={() => this.handlePublishButton(data.id)} >Publish</a>
                  : 
                  <a href="#" className="btn btn-text" onClick={() => this.handleUnPublishButton(data.id)} >Unpublish</a>
                 }
                </div>
                <div className="edit-mic btn-div">
                  <a href="#" onClick={() => this.handleEditModal(data.id)} className="btn btn-text" >Edit mic</a>
                </div>
                <div className="delete-mic btn-div">
                  <a href="#" className="btn btn-text" onClick={() => this.handleDeleteModal(data.id)} >Delete</a>
                </div>
              </div>
            </div>
          </div>
          }
         
        })}
      </div>
    </div>

  }

  render() {
    const { myMic } = this.state;
    return (
      <div className="tab-content">
         <div id="draft" className="tab-pane active">
       {this.renderPostMic(myMic)}
       </div>
       <div className="container">
        <div className="pagination-main">{this.renderPagination()}</div></div>
        <EditPostModal display={this.state.renderEditModal} hideModal={this.handleCloseEditModal} 
        editId={this.state.editId} tabIndex={this.props.tabIndex} />
        <DeletePostModal display={this.state.renderDeleteModal} hideModal={this.handleCloseDeleteModal} 
        editId={this.state.editId} tabIndex={this.props.tabIndex} />
      </div>
    );
  }
}




const mapStateToProps = state => ({
  postMicData: state.postMic.postMicData,
});
export default withRouter(
  connect(
    mapStateToProps,
    null,
  )(MyMic),
);