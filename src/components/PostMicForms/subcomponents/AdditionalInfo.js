import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import {
  setStepCount,
  addAddtionalInfo
} from '../../../actions/postMicFormActions'
import Mic from '../../../assets/images/mic.png'
import Cropper from 'react-cropper'
import 'cropperjs/dist/cropper.css'

class AdditionalInfo extends Component {
  constructor (props) {
    super(props)
    this.state = {
      imagePreviewUrl:
        this.props.additionalData &&
        this.props.additionalData.imagePreviewUrl !== ''
          ? this.props.additionalData.imagePreviewUrl
          : '',
      file: this.props.additionalData ? this.props.additionalData.file : [],
      errorFile: '',
      errorDescription: '',
      Description:
        this.props.additionalData &&
        this.props.additionalData.Description !== ''
          ? this.props.additionalData.Description
          : '',
      imageCroped:
        this.props.additionalData && this.props.additionalData.imageCroped
          ? this.props.additionalData.imageCroped
          : false
    }
    this._handleImageChange = this._handleImageChange.bind(this)
    this.handleChangeTextarea = this.handleChangeTextarea.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handlePrevious = this.handlePrevious.bind(this)
  }

  // handleLoadAvatar(e) {
  //   var file = e.target.files[0];
  //   var reader = new FileReader();
  //   reader.onload = (e) => {
  //     var img = document.createElement("img");
  //     img.onload = () => {
  //       var canvas = document.createElement('canvas');
  //       var ctx = canvas.getContext("2d");
  //       ctx.drawImage(img, 0, 0);

  //       var MAX_WIDTH = 300;
  //       var MAX_HEIGHT = 300;
  //       var width = img.width;
  //       var height = img.height;

  //       if (width > height) {
  //         if (width > MAX_WIDTH) {
  //           height *= MAX_WIDTH / width;
  //           width = MAX_WIDTH;
  //         }
  //       } else {
  //         if (height > MAX_HEIGHT) {
  //           width *= MAX_HEIGHT / height;
  //           height = MAX_HEIGHT;
  //         }
  //       }
  //       canvas.width = width;
  //       canvas.height = height;
  //       var ctx = canvas.getContext("2d");
  //       ctx.drawImage(img, 0, 0, width, height);
  //       var dataurl = canvas.toDataURL("image/png");
  //       this.setState({previewSrc: dataurl});
  //     }
  //     img.src = e.target.result;
  //   }
  //   reader.readAsDataURL(file);
  // }

  _handleImageChange (e) {
    e.preventDefault()

    let reader = new FileReader()

    let file = e.target.files[0]
    if (file) {
      if (!file.type.match('image.*')) {
        this.setState({
          errorFile: 'Upload your file only in image formate',
          file: [],
          imagePreviewUrl: undefined
        })
      } else {
        reader.onload = e => {
          var img = document.createElement('img')
          img.onload = () => {
            var canvas = document.createElement('canvas')
            var ctx = canvas.getContext('2d')
            ctx.drawImage(img, 0, 0)

            var MAX_WIDTH = 300
            var MAX_HEIGHT = 300
            var width = img.width
            var height = img.height

            if (width > height) {
              if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width
                width = MAX_WIDTH
              }
            } else {
              if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height
                height = MAX_HEIGHT
              }
            }
            canvas.width = width
            canvas.height = height
            var ctx = canvas.getContext('2d')
            ctx.drawImage(img, 0, 0, width, height)
            var dataurl = canvas.toDataURL('image/png')
            this.setState({
              imagePreviewUrl: dataurl,
              file: file,
              errorFile: ''
            })
          }
          img.src = e.target.result
        }
        // reader.onloadend = () => {
        //   this.setState({
        //     file: file,
        //     imagePreviewUrl: reader.result,
        //     errorFile: '',
        //   });
        // }
        reader.readAsDataURL(file)
      }
    }
  }

  async getImage () {
    if (this.state.imageCroped === true) {
      const croped = await this.state.imagePreviewUrl
      // var base64Icon = `data:image/png;base64,${croped}`;
      var block = croped.split(';')
      // Get the content type of the image
      var contentType = block[0].split(':')[1]
      // var blob = this.b64toBlob(croped, contentType);
      var byteCharacters = atob(
        croped.replace(/^data:image\/(png|jpeg|jpg);base64,/, '')
      )
      var byteNumbers = new Array(byteCharacters.length)
      for (var i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i)
      }

      var byteArray = new Uint8Array(byteNumbers)
      var blob = new Blob([byteArray], {
        type: contentType
      })
      return blob
    } else {
      const croped = await this.cropper.getCroppedCanvas().toDataURL()
      // var base64Icon = `data:image/png;base64,${croped}`;
      var block = croped.split(';')
      // Get the content type of the image
      var contentType = block[0].split(':')[1]
      // var blob = this.b64toBlob(croped, contentType);
      var byteCharacters = atob(
        croped.replace(/^data:image\/(png|jpeg|jpg);base64,/, '')
      )
      var byteNumbers = new Array(byteCharacters.length)
      for (var i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i)
      }

      var byteArray = new Uint8Array(byteNumbers)
      var blob = new Blob([byteArray], {
        type: contentType
      })
      return blob
    }
  }

  handleSubmit () {
    const { file, Description, imageCroped } = this.state
    const { setStepCount, addAddtionalInfo } = this.props
    if (this.state.file) {
      this.getImage().then(value => {
        const fileName = this.state.file.name
        var fileNew = new File([value], fileName, {
          lastModified: new Date(),
          type: 'image/*'
        })
        this.setState(
          {
            file: fileNew
          },
          () => {
            const { imagePreviewUrl } = this.state
            // if (file === undefined) {
            // this.setState({
            //   // errorFile: 'Upload Image',
            //   imagePreviewUrl: undefined
            // });
            // } else {
            // const additionalData = {
            //   file: fileNew,
            //   imagePreviewUrl: imagePreviewUrl,
            //   Description: Description !== undefined ||  Description !== null ||  Description !== ''? Description : '',
            //   imageCroped: imageCroped,
            // };
            // addAddtionalInfo(additionalData);
            // const count = 6;
            // setStepCount(count);
          }
        )
      })
    } else {
      const { imagePreviewUrl } = this.state
      const additionalData = {
        file: file,
        imagePreviewUrl: imagePreviewUrl,
        Description:
          Description !== undefined ||
          Description !== null ||
          Description !== ''
            ? Description
            : '',
        imageCroped: imageCroped
        //  Description:Description
      }
      addAddtionalInfo(additionalData)
      const count = 6
      setStepCount(count)
    }
    //validation of description

    if (this.state.Description) {
      const Description = this.state.Description
      if (Description.length > 15) {
        this.setState({
          errorDescription: 'Description must be less than 15 Character'
        })
      } else {
        this.setState({ errorDescription: '' })

        const additionalData = {
          file: file,
          imagePreviewUrl: this.state.imagePreviewUrl,
          //  Description: Description !== undefined ||  Description !== null ||  Description !== ''? Description : '',
          imageCroped: imageCroped,
          Description: Description
        }
        addAddtionalInfo(additionalData)
        const count = 6
        setStepCount(count)
      }
    } else {
      const additionalData = {
        file: file,
        imagePreviewUrl: this.state.imagePreviewUrl,
        Description:
          Description !== undefined ||
          Description !== null ||
          Description !== ''
            ? Description
            : '',
        imageCroped: imageCroped
        //Description:this.state.Description
      }
      addAddtionalInfo(additionalData)
      const count = 6
      setStepCount(count)
    }
  }

  handlePrevious () {
    if (this.state.imageCroped === true) {
      const { imagePreviewUrl, file, Description, imageCroped } = this.state
      const { setStepCount, addAddtionalInfo } = this.props
      const additionalData = {
        file: file,
        imagePreviewUrl: imagePreviewUrl,
        Description: Description,
        imageCroped: imageCroped
      }
      addAddtionalInfo(additionalData)
      const count = 4
      setStepCount(count)
    } else {
      if (typeof this.cropper.getCroppedCanvas() === 'undefined') {
        return
      }
      this.setState(
        {
          imagePreviewUrl:
            this.cropper.getCroppedCanvas().toDataURL() === null
              ? ''
              : this.cropper.getCroppedCanvas().toDataURL()
        },
        () => {}
      )
      const { imagePreviewUrl, file, Description, imageCroped } = this.state
      const { setStepCount, addAddtionalInfo } = this.props

      // if (file === undefined) {
      this.setState({
        // errorFile: 'Upload Image',
        imagePreviewUrl: undefined
      })
      // } else {
      // const additionalData = {
      //   file: file,
      //   imagePreviewUrl: imagePreviewUrl,
      //   Description: Description,
      //   imageCroped: imageCroped
      // }
      // addAddtionalInfo(additionalData)
      // const count = 4
      // setStepCount(count)
      // }
    }

    //validation of description

    if (this.state.Description) {
      const { file, Description, imageCroped } = this.state
      const { setStepCount, addAddtionalInfo } = this.props

      //const Description=this.state.Description
      if (Description.length > 15) {
        alert("error")
        this.setState({
          errorDescription: 'Description must be less than 15 Character'
        })
        const count = 5
        setStepCount(count)
      } else {
        this.setState({ errorDescription: '' })

        const additionalData = {
          file: file,
          imagePreviewUrl: this.state.imagePreviewUrl,
          //  Description: Description !== undefined ||  Description !== null ||  Description !== ''? Description : '',
          imageCroped: imageCroped,
          Description: Description
        }
        addAddtionalInfo(additionalData)
        const count = 4
        setStepCount(count)
      }
    } else {
      const { file, Description, imageCroped } = this.state
      const { setStepCount, addAddtionalInfo } = this.props
      const additionalData = {
        file: file,
        imagePreviewUrl: this.state.imagePreviewUrl,
        Description:
          Description !== undefined ||
          Description !== null ||
          Description !== ''
            ? Description
            : '',
        imageCroped: imageCroped
        //Description:this.state.Description
      }
      addAddtionalInfo(additionalData)
      const count = 4
      setStepCount(count)
    }
  }

  ///new changes
  handleChangeTextarea (event) {
    const value = event.target.value
    //alert(value)
    if (value !== undefined) {
      //alert(value)
      this.setState({ Description: value })
      if (value.length > 15) {
        this.setState({
          errorDescription: 'Description must be less than 15 Character'
        })
      } else {
        this.setState({ errorDescription: '' })
      }
    }
  }

  _crop () {
    // image in dataUrl
    if (typeof this.cropper.getCroppedCanvas() === 'undefined') {
      return
    }
    this.setState(
      {
        imagePreviewUrl: this.cropper.getCroppedCanvas().toDataURL(),
        imageCroped: true
      },
      () => {}
    )
  }

  _remove () {
    this.setState({ imagePreviewUrl: undefined, file: '', imageCroped: false })
  }

  render () {
    const { imagePreviewUrl, errorFile, errorDescription } = this.state
    return (
      <fieldset>
        <div className='row'>
          <div className='col-lg-6'>
            <div className='step-title'>
              {this.props.authenticated === false ? (
                <h4>Step 6 of 7</h4>
              ) : (
                <h4>Step 5 of 6</h4>
              )}
              <p>Upload Image(Optional) Ideal dimensions are (300 X 300)” </p>
            </div>
          </div>
        </div>
        <div className='row'>
          <div className='col-lg-2 col-md-3 col-sm-6'>
            <div className='form-file'>
              {imagePreviewUrl === undefined ? (
                <input
                  type='file'
                  className='inputfile'
                  name='your_picture'
                  id='your_picture'
                  accept='image/*'
                  onChange={e => this._handleImageChange(e)}
                />
              ) : null}
              <label htmlFor='your_picture'>
                <figure>
                  {this.state.imageCroped === false ? (
                    <Cropper
                      ref={cropper => {
                        this.cropper = cropper
                      }}
                      src={
                        imagePreviewUrl !== undefined ? imagePreviewUrl : Mic
                      }
                      style={{ width: 200, height: 130, overflow: 'hidden' }}
                      guides={true}
                      aspectRatio={17 / 9}
                      cropBoxResizable={true}
                    />
                  ) : (
                    <img
                      src={
                        imagePreviewUrl !== undefined ? imagePreviewUrl : Mic
                      }
                      alt=''
                      style={{ width: 200, height: 130, overflow: 'hidden' }}
                    />
                  )}
                </figure>
                {imagePreviewUrl === undefined ? (
                  <span className='file-button'>Upload Poster</span>
                ) : this.state.imageCroped === true ? (
                  <div className='crop-img-div'>
                    <span
                      className='file-button file-input100 remove-button'
                      onClick={this._remove.bind(this)}
                    >
                      Remove
                    </span>
                  </div>
                ) : (
                  <div className='crop-img-div'>
                    <span
                      className='file-button file-input50 cropimg-button'
                      onClick={this._crop.bind(this)}
                    >
                      Crop Image
                    </span>
                    <span
                      className='file-button file-input50 remove-button'
                      onClick={this._remove.bind(this)}
                    >
                      Remove
                    </span>
                  </div>
                )}
              </label>
            </div>
            <label className='error'>{errorFile}</label>
          </div>
          <div className='col-lg-10  col-md-9 col-sm-6'>
            <div className='form-group'>
              <label className='has-float-label'>
                <textarea
                  value={this.state.Description}
                  placeholder='How to sign-up for the Mic (Maximum 15 character)'
                  onChange={this.handleChangeTextarea}
                  className='form-control text-area mt-0'
                  rows='4'
                  name='Description'
                  id='Description'
                  placeholder='How to sign-up for the Mic'
                ></textarea>
                <span>How to sign-up for the Mic</span>
              </label>
            </div>
            <label className='error'>{errorDescription}</label>
          </div>
        </div>
        <div className='row'>
          <div className='col-lg-12 '>
            <div className='btn-groups-steps'>
              {/* <a href="/postMicForm" className="btn btn-login">Agree</a> */}
              <button onClick={this.handlePrevious} className='btn btn-prev'>
                Previous
              </button>
              <button onClick={this.handleSubmit} className='btn btn-next'>
                Next
              </button>
            </div>
          </div>
        </div>
      </fieldset>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  setStepCount: count => dispatch(setStepCount(count)),
  addAddtionalInfo: additionalData => dispatch(addAddtionalInfo(additionalData))
})
const mapStateToProps = state => ({
  count: state.accountDetails.count,
  additionalData: state.accountDetails.additionalData,
  authenticated: state.authUser.authenticated
})
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(AdditionalInfo)
)
