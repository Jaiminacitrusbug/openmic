import axios from 'axios'
import {errorHandle } from './errorHandling'
import {APP_CONFIG} from '../constants/config';


export function contactUs(contactData) {
  try {
    return axios.post(APP_CONFIG.API_URL+'send-feedback', contactData, {
      headers:{
        'Content-Type': 'application/json',
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}