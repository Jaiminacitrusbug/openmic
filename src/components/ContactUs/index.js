import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from 'yup';
import history from '../../services/BrowserHistory';
import { ToastContainer, toast } from 'react-toastify';
import { contactUs } from '../../api/contactUsAPI'; 
class ContactAdmin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(values) {
    const feedback = {
      name: values.name,
      email: values.email,
      comments: values.comment,
    };
    this.setState({ loader: true });
    contactUs(feedback).then((result) => {
      if (result) {
        if (result.code === 200 && result.status === true) {
          toast.info(result.message);
          setTimeout(() => {
            history.push('/');
            this.setState({ loader: false });
          }, 1000);
        } else if (result.code === 200 && result.state === false) {
          toast.error(result.message);
          this.setState({ loader: false });
        }
      }
    })
  }
   
  render() {
    return (
      <div className="middle-container">
        <ToastContainer />
        <section className="postmic-secttion main">
          <div className="container">
            <div className="post-mic-form post-mic-form-steps">

              <div className="col-lg-12">
                <div className="post-mic-heading">
                  <h3>CONTACT US</h3>
                </div>
              </div>
              <Formik
            initialValues={{
              name: '',
              email: '',
              comment: '',
            }}
            validationSchema={Yup.object().shape({
              name: Yup.string().trim().strict(false)
                .required('Name is required'),
              email: Yup.string().trim().strict(false)
                .email('Email is invalid')
                .required('Email is required'),
                comment: Yup.string().trim().strict(false)
              .required('Comment is required'),
            })}
            onSubmit={this.handleSubmit}
          >
            {(values) => (
              <Form>
                <div className="col-lg-12">
                  <div className="form-group">
                    <label className="has-float-label">
                      <Field
                        type="text"
                        name="name"
                        placeholder="Name"
                        className="form-control"
                      />
                      <span>Name *</span>
                    </label>
                    <ErrorMessage name="name" component="span" className="error" />
                  </div>
                </div>

                <div className="col-lg-12">
                  <div className="form-group">
                    <label className="has-float-label">
                      <Field
                        type="text"
                        name="email"
                        placeholder="Email Address"
                        className="form-control"
                      />
                      <span>Email Address *</span>
                    </label>
                    <ErrorMessage name="email" component="span" className="error" />
                  </div>
                </div>

                <div className="col-lg-12">
                  <div className="form-group">
                    <label className="has-float-label">
                      <Field
                        component="textarea"
                        name="comment"
                        placeholder="comment"
                        className="form-control"
                        rows={4}
                        />
                      <span>comment *</span>
                    </label>
                    <ErrorMessage name="comment" component="span" className="error" />
                  </div>
                </div>
                <div className="col-lg-12 text-right">
                      <div className="btn-groups">
                        <button disabled={this.state.loader} type="submit" className="btn btn-login">Submit {this.state.loader === true ? <span className="spinner-border spinner-border-sm"></span> : null}</button>
                      </div>
                    </div>
              </Form>
            )}
          </Formik>
            </div>
          </div>
        </section>
      </div>

    );
  }
}

export default ContactAdmin;