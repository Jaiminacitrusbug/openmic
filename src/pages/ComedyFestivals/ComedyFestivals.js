import React, { Component } from 'react';
import TopMenu from '../../components/TopMenu';
import Footer from '../../components/Footer';
import HeaderDashboard from '../../components/HeaderDashboard/index';
import ComedyFestivals from '../../components/ComedyFestivals/index';
import CookiesComedy from '../../components/Cookie/CookieComedy';
import 'react-toastify/dist/ReactToastify.css';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

class ComedyFestival extends Component {
	constructor(props) {
		super(props);
		this.state = {
			userData: JSON.parse(sessionStorage.getItem('user')),
		};

	};

	componentDidUpdate(prevProps) {
		if (prevProps.authenticated !== this.props.authenticated) {
			if (this.props.authenticated === false) {
				this.setState({ userData: '' });
			}
		}
	}

	render() {
		return (
			<div className="wrapper">
				{(this.state.userData && this.state.userData.token != '') ?
					<HeaderDashboard />
					:
					<TopMenu />
				}
				<CookiesComedy/>
				<ComedyFestivals />
				<Footer />
			</div>
		);
	}
}

const mapStateToProps = state => ({
	authenticated: state.authUser.authenticated,
});


export default withRouter(
	connect(
		mapStateToProps,
		null
	)(ComedyFestival),
);