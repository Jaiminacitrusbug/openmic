import { combineReducers } from 'redux';
import accountDetails from './postMicFormReducers';
import dashboard from './dashboardReducer';
import authUser from './authReducer';
import postMic from './postMicReducer';
import filterMic from './filterMicReducer';
const rootReducer = combineReducers({
  accountDetails,
  dashboard,
  authUser,
  postMic,
  filterMic,
});

export default rootReducer;
