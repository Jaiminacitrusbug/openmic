import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import poster from '../../assets/images/micBig.png';
import history from '../../services/BrowserHistory';
import { getAdvertise } from '../../actions/filterMicActions'; 
//import moment from 'moment';
import { getpostMicData, getSelectedMic } from '../../actions/postMicActions';
import add1 from '../../assets/images/add/add-two.png';
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";

class GridPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      postData: [],
      layout: 0,
      advertise: [],
    };
    this.handleClickPost = this.handleClickPost.bind(this);
    this.handleAdd = this.handleAdd.bind(this);

  }

  componentDidMount() {
    const { layout, postData } = this.props;
    this.setState({ layout, postData, advertise: [] }, () => {
      const { getpostMicData } = this.props;
      getpostMicData(this.state.postData);
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.layout !== this.props.layout) {
      this.setState({ layout: this.props.layout });
    }
    if (prevProps.postData !== this.props.postData) {
      this.setState({ postData: this.props.postData }, () => {
        const { getpostMicData } = this.props;
        getpostMicData(this.state.postData);
      });
    }

    if (prevProps.advertise !== this.props.advertise) {
      this.setState({ advertise: this.props.advertise })
    }

  }

  componentWillUnmount() {
    this.setState({ advertise: []}, () => {
      this.props.getAdvertise(this.state.advertise)
    })
  }

  handleClickPost(data) {
    this.props.getSelectedMic(data);
    history.push({
      pathname: '/postDetails',
      state: { id: data }
    });
  }

  handleAdd(link) {
    window.open(link);
  }


  render() {
    return (
      <div>
        <div className="grid-layout"></div>
        <div className="row">
          {this.state.postData && this.state.postData.length === 0 ? <div className="grid-list col-lg-12 col-md-12 col-sm-12" style={{ margin: "10px", fontSize: "24px" }}>No Event Found</div> : null}
          {this.state.postData && this.state.postData.length > 0 && this.state.postData.map((data, index) => {
            return <div key={index} className={this.state.layout === 0 ? "grid-list col-lg-3 col-md-6 col-sm-6" : "grid-list col-lg-3 col-md-12 col-sm-6 col-lg-12 list-view"}>
              <div className="grid-box">
                {data.featured === true ?
                  <span className="featured-label">Featured</span>
                  : null}
                <a onClick={() => this.handleClickPost(data)}>
                  <div className="img-box">
                    <img src={data.poster_image_url !== null ? data.poster_image_url : poster} alt="product-1" />
                  </div>
                  <div className="content-box">
                  <div className="row flex-lg-wrap">
                    <div className="col-sm-8">
                    <h3>{data.mic_name}</h3>
                    <p className="oneline"><i className="material-icons"> location_on </i>{data.mic_venue}, {data.region.region}</p>
                    {/* <p>{data.eventAvailable}</p> */}
                    <p className="address">{data.address}</p>
                    </div>
                    <div class="col-sm-4">
                      {console.log("time is..............",data.mic_time)}
                    <p><i className="material-icons"> access_time </i>{data.mic_time},  {data.mic_day_name}</p>
                    </div>
                    </div>
                  </div>
                  <div className="content-box2">
                    <h4>About the Mic</h4>
                    <p>{data.about_mic}</p>
                  </div>
                </a>
              </div>
            </div>
          })}
          <div id="adds-banner" className="grid-list col-lg-6 col-md-6 col-sm-6">
            <div className="grid-box grid-bg-none">
              {this.state.advertise.length > 0 &&
            <Carousel 
             autoPlay={true}
             interval={2000}
             showArrows={false}
             showThumbs={false}
             showIndicators={false}
             infiniteLoop={true}
             showStatus={false}
          >
              {this.state.advertise.map((data, index) => {
                return <a key={index} href={data.advertise_url} target="_blank">
                <div className="img-add img-m0 add-one">
                  <img src={data.advertise_image_url} alt="add" />
                </div>
                </a>

              })}
            </Carousel>
              }
            </div>
          </div>

        </div>
      </div>
    );
  }
}


const mapStateToProps = state => ({
  advertise: state.filterMic.advertise,
});

const mapDispatchToProps = dispatch => ({
  getpostMicData: data => dispatch(getpostMicData(data)),
  getSelectedMic: data => dispatch(getSelectedMic(data)),
  getAdvertise: data => dispatch(getAdvertise(data))
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(GridPost),
);