import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import poster from '../../assets/images/post-img.png';
import beer from '../../assets/images/icon/beer-jar.png';
import RequestToParticipate from '../../components/RequestToParticipate';
import history from '../../services/BrowserHistory';


class EventDetails extends Component {
  constructor(props){
    super(props);
    this.state = {
      renderRequestModal: false,
      date: '',
      time: '',
      location: '',
      address: '',
      email: '',
      phoneno: '',
      drinkType: '',
      availability: '',
      aboutMic: '',
      image: '',
      name:'',
      region: '',
      mic_day_name: '',
      timeslot: '',
      is_payment_enable: false
    };
    this.handleRequestModal = this.handleRequestModal.bind(this);
    this.handleCloseRequestModal = this.handleCloseRequestModal.bind(this);
  }

  componentDidMount() {
    if (this.props.location) {
      this.setState({
      date: this.props.location && moment(this.props.location.state.id.mic_datetime).format('L'),
      time: this.props.location && this.props.location.state.id.mic_time,
      location: this.props.location && this.props.location.state.id.mic_venue,
      address: this.props.location && this.props.location.state.id.address,
      aboutMic: this.props.location && this.props.location.state.id.about_mic,
      image: this.props.location && this.props.location.state.id.poster_image_url,
      name: this.props.location && this.props.location.state.id.mic_name,
      email: this.props.location && this.props.location.state.id.producer.email,
      phoneno: this.props.location && this.props.location.state.id.producer.contact,
      drinkType: this.props.location && this.props.location.state.id.mic_fees.fees,
      availability: this.props.location && this.props.location.state.id.mic_cost_type,
      cost : this.props.location && this.props.location.state.id.mic_cost_type === "PAID" ? this.props.location && this.props.location.state.id.mic_cost : '',
      region: this.props.location && this.props.location.state.id.region.region,
      mic_day_name: this.props.location && this.props.location.state.id.mic_day_name, 
      timeslot: this.props.location && this.props.location.state.id.timeslot && this.props.location.state.id.timeslot.name || '',
      is_payment_enable: this.props.location && this.props.location.state.id.is_payment_enable
      })
    } 
  }

  handleRequestModal() {
    this.setState({ renderRequestModal: true });
  }

  handleCloseRequestModal() {
    this.setState({ renderRequestModal: false });
  }

  handlePayment = () => {
    history.push('payment', {micId: this.props.location &&  this.props.location.state &&this.props.location.state.id.id});
  }

  render() {
    const { date, time, location, address, email, phoneno, drinkType, availability, aboutMic, image, name, region, mic_day_name, cost, timeslot, is_payment_enable } = this.state;
  
    return (
      <div className="middle-container">
      <section className="postmic-detail-section">
        <div className="container">
          <div className="post-mic-detais">
            <div className="row justify-content-center">
                <div className="col-lg-9">
                  <div className="row">
                  <div className="col-lg-6 col-md-6 col-sm-6">
                    <div className="post-img">
                        <img src={image !== null? image: poster} className="img-responsive" alt="img" />
                    </div>
                  </div>
                  <div className="col-lg-6 col-md-6 col-sm-6">
                      <div className="post-content">
                        
                        <h3>{name}</h3>
                        <h4><i className="material-icons"> access_time </i>{time}, {timeslot}, {mic_day_name}</h4>
  
                        <h4><i className="material-icons"> location_on </i>{location}, {region}</h4>
                        <p>{address}</p>
  
                        <h4><i className="material-icons">contacts</i>Contact</h4>
                        <p>{email}
                        <span>{phoneno}</span></p>
  
                        <h4><i><img src={beer} /></i>{drinkType}</h4>
                        <h4><i className="material-icons">payment</i>{availability}{availability === "PAID"? ',' : ''}{availability === "PAID"? '$' : ''}{cost}</h4>
  
                      </div>
                  </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-12 col-md-12 col-sm-6">
                    <div className="post-about">
                        <h4>Best About The Mic</h4>
                        <p>{aboutMic}</p>
                        
                      </div>
                      {is_payment_enable === true && cost >! 0?
                      <div className="post-about-btn">
                        <a onClick={this.handlePayment} className="btn-blue-light" data-target="#modal-request" data-toggle="modal">Request to participate</a>
                      </div> :
                      <div className="post-about-btn">
                        <a onClick={this.handleRequestModal} className="btn-blue-light" data-target="#modal-request" data-toggle="modal">Request to participate</a>
                      </div>	
                      }
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </section>
     <RequestToParticipate display={this.state.renderRequestModal} hide={this.handleCloseRequestModal} micId={this.props.location &&  this.props.location.state &&this.props.location.state.id.id}/>

    </div>
    );
  }
}

const mapStateToProps = state => ({
  postMicData: state.postMic.postMicData,
  micData: state.postMic.micData,
});

// const mapDispatchToProps = dispatch => ({
//   getpostMicData: data => dispatch(getpostMicData(data)),
//   getSelectedMicId: micId => dispatch(getSelectedMicId(micId)),
// });

export default withRouter(
  connect(
    mapStateToProps,
    null,
  )(EventDetails),
);