import React, { Component } from 'react';
import { Link } from 'react-router-dom';
class PostMic extends Component {
  render() {
    return (
      <div className="middle-container">
        <section className="postmic-secttion">
          <div className="container">
            <div className="post-mic-form">
              <div className="row">
                <div className="col-lg-12">
                  <div className="post-mic-heading">

                    <h3 >Post your mic...</h3>

                    <p>Guide for Posting Your Open Mic</p>

                    <ul className="post-guide">
                      <li>Your mic must permit Stand Up Comedy. </li>
                      <li>You must not require performers to bring audience members.</li>
                      <li>You must list all fees (including drink minimums) that are required to perform.</li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="login-form-footer">
                <div className="row">

                  <div className="col-lg-12 text-right">
                    <div className="btn-groups">
                      <Link to="/post-mic/form" className="btn btn-login">Agree</Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default PostMic;