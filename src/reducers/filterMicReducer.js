import * as types from '../actions/actionTypes';

const initialState = {
  search:'',
  region:'',
  feesValue:'',
  featuredValue: '',
  advertise: [],
};

const filterMic = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_SEARCH_VALUE:
      return {
        ...state,
        search: action.payload
      };
    case types.SET_REGION_VALUE:
      return {
        ...state,
        region: action.payload
      };
    case types.SET_FEES_VALUE:
      return {
        ...state,
        feesValue: action.payload
      };
    case types.SET_FEATURED_VALUE:
      return {
        ...state,
        featuredValue: action.payload
      };
    case types.GET_ADVERTISE:
      return {
        ...state,
        advertise: action.payload
      };
    default:
      return state;
    }
  }

  export default filterMic;
