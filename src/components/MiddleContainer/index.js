import React, { Component } from 'react';
import Select from 'react-select';
import { Modal} from 'react-bootstrap';
import { getRegions, getFees } from '../../api/getMicApi';
import { getRegion } from '../../actions/postMicActions';
import { setSearchValue, setRegionValue, setFeesValue, setFeaturedValue } from '../../actions/filterMicActions';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import blackLogo from '../../assets/images/Size/Logoforwhite2.png';
import ListContainer from '../ListContainer/index';
import Banner from '../../assets/images/openmic-banner.jpg'
import { getBanner } from '../../api/advertisementApi';
class MiddleContainer extends Component {
	constructor(props) {
	super(props);
	this.state = {
		drinkType:'',
		sortBy:'',
		renderRegionModal: false,
		renderFilterApply: false,
		location:[],
		defaultLocation: '',
		optionForDrink: [],
		search:'',
		banner: undefined,
	};
	this.handleDrinkMinimum = this.handleDrinkMinimum.bind(this);	
	this.handleDrinkMinimumModal = this.handleDrinkMinimumModal.bind(this);	
	this.handleSortBy = this.handleSortBy.bind(this);
	this.handleSortByModal = this.handleSortByModal.bind(this);
	this.handleOpenModalRegion = this.handleOpenModalRegion.bind(this);
	this.handleCloseModalRegion = this.handleCloseModalRegion.bind(this);
	this.handleFilterModal = this.handleFilterModal.bind(this);
	this.handleClosefilterModal = this.handleClosefilterModal.bind(this);
	this.handleSelectedRegion = this.handleSelectedRegion.bind(this);
	this.handleSearch = this.handleSearch.bind(this);
	this.handleSearchModal = this.handleSearchModal.bind(this);
	this.submitFilterModal = this.submitFilterModal.bind(this);
	
	}

	componentDidMount() {
		this.getRegionsOptions();
		this.getMicType();
		this.getBannerImages();
	}

	getRegionsOptions() {
		const { getRegion, setRegionValue } = this.props;
		getRegions().then((result, index) => {
		if (result && result.code === 200 && result.status === true) {
					let region = [];
			result.data.map((item, index) => {
				region.push({
					name: item.region,
					id: item.id,
					url:item.region_image_url,
				});
			});
			if (region.length > 0) {

				this.setState({ location: region, defaultLocation: region[0].name }, () => {
					setRegionValue(this.state.defaultLocation);
				})
				getRegion(result.data);

			}
			} else {
		}
		
		})
	}

	getMicType() {
		getFees().then((result) => {
			if (result && result.code === 200 && result.status === true) {
				let fees = [];
			result.data.map((item) => {
				fees.push({
					value:item.id,
					label: item.fees,
				});
			});
			this.setState({ optionForDrink: fees });
			}
		})
	}

	handleDrinkMinimum(drinkType) {
		this.setState({ drinkType }, () => {
			const { setFeesValue } = this.props;
			setFeesValue(drinkType);
		});
	}	

	handleDrinkMinimumModal(drinkType) {
		this.setState({ drinkType });
	}

	handleSortBy(sortBy) {
		this.setState({ sortBy }, () => {
			const { setFeaturedValue} = this.props;
			setFeaturedValue(sortBy);
		});
	}	

	handleSortByModal(sortBy) {
		this.setState({ sortBy});
	} 

	handleOpenModalRegion() {
		this.setState({ renderRegionModal: true });
	}

	handleCloseModalRegion() {
		this.setState({ renderRegionModal: false});
	}

	handleFilterModal() {
		this.setState({ renderFilterApply: true });
	}

	handleClosefilterModal() {
		this.setState({ renderFilterApply: false });
}

	handleSelectedRegion(data) {	
		this.setState({ defaultLocation: data.name, renderRegionModal: false }, () => {
			const { setRegionValue } = this.props;
			setRegionValue(data.name);
		})
	}

	handleSearch(e) {
		const { value } = e.target;
		this.setState({ search: value }, () => {
			if (value.length >= 3) {
				this.props.setSearchValue(value);
			} else {
			this.props.setSearchValue('');
		}
		});
	}

	handleSearchModal(e) {
		const { value } = e.target;
		this.setState({ search: value });
	}

	submitFilterModal() {
		const {search, sortBy, drinkType } = this.state;
		const { setSearchValue, setFeaturedValue, setFeesValue} = this.props;
				setSearchValue(search);
			if (sortBy) {
				setFeaturedValue(sortBy);
			}
			if (drinkType) {
				setFeesValue(drinkType);
			}
			this.setState({ renderFilterApply: false });
	}	

	getBannerImages = () => {
		getBanner('main').then((res) => {
			if (res.code === 200 && res.status === true) {
				this.setState({banner: res.data});
			} else if (res.code === 200 && res.status === false) {
	
			} else if (res.code === 400) {
	
			}
		})
	}
	
  render() {
	  const { banner } = this.state;
    return (
			<div className="middle-container">
			<div className="min-hight">
			<section className="banner-section">
			<a href={banner!== undefined && banner.link} target="_blank">
				<img src={banner!== undefined && banner.image !== ''? banner.image : Banner} style={{width: '100%', height:'100%'}} />
				</a>
			</section>
			<section className="quick-search">
			<div className="quick-search-div">
				<div className="container">
					<div className="row">
						<div className="col-lg-4 col-md-4 col-6">
							<span className="has-float-label readyonly" >
								<input type="button" className="btn btn-modal" value={this.state.defaultLocation} data-toggle="modal" data-target="#change-region" onClick={this.handleOpenModalRegion}/>
								<label htmlFor="first">Change Region</label>
							</span>
						</div>
						<div className="col-lg-3 col-md-3 col-6 mobile-view ">
								<a href="#" onClick={this.handleFilterModal} className="btn btn-modal btn-filter" value={this.state.defaultLocation} data-toggle="modal" data-target="#moadl-filter">
									Apply Filter <i className="material-icons"> filter_list </i>
								</a>
						</div>


						<div className="col-lg-4 col-md-4 destop-view">
							<span className="has-float-label">
									<div className="select2-div">
									{/* <Typeahead
										onChange={(drinkType) => {
										this.setState({drinkType: drinkType});
										}}
										options={options}
										selected={this.state.drinkType}
										className="select-multiple"
										/> */}
									<Select
										value={this.state.drinkType}
										onChange={this.handleDrinkMinimum}
										options={this.state.optionForDrink}
										className="select-multiple"
									/>
										</div>
								<label htmlFor="second">Select Mic Type</label>
							</span>
						</div>
							{/* <div className="col-lg-3 col-md-3 destop-view">
							<span className="has-float-label">
										<div className="select2-div">
										<Select
										value={this.state.sortBy}
										onChange={this.handleSortBy}
										options={optionsSortBy}
										className="select-multiple"
									/>
										
										</div>
								<label htmlFor="third">Sort By</label>
							</span>
						</div> */}
						<div className="col-lg-4 col-md-4 destop-view">
							<div className="form-group search-bar">
								<input type="text" className="form-control search" onChange={this.handleSearch} 
								 value={this.state.search} name="search" placeholder="Quick Search"/>
								<i className="fa fa-search icon"></i>
							</div>
						</div>
						</div>

					</div>
				</div>
		</section>
		<ListContainer />
		</div>
		<Modal id="change-region" className="modal fade modal-custom" show={this.state.renderRegionModal} onHide={this.handleCloseModalRegion}>
		<div className="modal-header">
			<h4 className=""></h4>
			<button type="button" className="close" data-dismiss="modal" onClick={this.handleCloseModalRegion}>&times;</button>
		</div>
		<div className="modal-body">
			<div className="modal-logo">
				<a href="#">
					<img src={blackLogo} alt="logo"/>

				</a>
			</div>
			<div className="modal-heading dotted-gradient">
				<h3>Register and Post your mic...</h3>
			</div>

			<div className="row">
				{this.state.location.map((data, index) => {
					return <div key={index} className="col-lg-4 col-md-4 col-6">
					<a href="#" onClick={() => this.handleSelectedRegion(data)} className="modal-post">
						<div className="postbox">
							<img src={data.url} alt="" />
							<h2>{data.name}</h2>
						</div>
					</a>
				</div>
				})}
			</div>
		</div>

		</Modal>

		<Modal id="moadl-filter" className="modal fade modal-custom" role="dialog" show={this.state.renderFilterApply} onHide={this.handleClosefilterModal}>
					<div className="modal-header">
						<h4 className=""></h4>
						<button onClick={this.handleClosefilterModal} type="button" className="close" data-dismiss="modal">&times;</button>
					</div>
					<div className="modal-body post-mic-form">
						<div className="col-lg-6 col-md-12">
							<div className="form-group">
								<span className="has-float-label">
									<div className="select2-div">
									<Select
										value={this.state.drinkType}
										onChange={this.handleDrinkMinimumModal}
										options={this.state.optionForDrink}
										className="select-multiple"
									/>
										{/* <select className="select-multiple">
											<option>No Drink Minimum</option>
											<option>Drink Minimum</option>
										</select> */}
									</div>
									<label htmlFor="second">Select Mic Type</label>
								</span>
							</div>
						</div>
						{/* <div className="col-lg-6 col-md-12 ">
							<div className="form-group">
								<span className="has-float-label">
									<div className="select2-div">
											<Select
										value={this.state.sortBy}
										onChange={this.handleSortByModal}
										options={optionsSortBy}
										className="select-multiple"
									/>
									</div>
									<label htmlFor="third">Short By</label>
								</span>
							</div>
						</div> */}
						<div className="col-lg-6 col-md-12 ">
							<div className="form-group search-bar">
								<input type="text" className="form-control search" 
								 placeholder="Quick Search" 
								 onChange={this.handleSearchModal} 
								 value={this.state.search} 
								 name="search"
								/>
								<i className="fa fa-search icon"></i>
							</div>
						</div>
						<div className="col-lg-6 text-right">
								<div className="btn-groups">
									<a href="#" className="btn btn-login" onClick={this.submitFilterModal}>Apply</a>
								</div>
						</div>
					</div>
		</Modal>

		
    </div>
		);
  }
}


// const mapStateToProps = state => ({
//   accountDetails: state.accountDetails.accountDetails,
//   count: state.accountDetails.count,
//   selectedDate: state.accountDetails.selectedDate,
// });

const mapDispatchToProps = dispatch => ({
	getRegion: data => dispatch(getRegion(data)),
	setSearchValue: search => dispatch(setSearchValue(search)),
	setRegionValue: region => dispatch(setRegionValue(region)),
	setFeesValue: drinkType => dispatch(setFeesValue(drinkType)),
	setFeaturedValue: sortBy => dispatch(setFeaturedValue(sortBy)),
});

export default withRouter(
  connect(
    null,
    mapDispatchToProps,
  )(MiddleContainer),
);