export const ADD_ACCOUNT_DETAILS = 'ADD_ACCOUNT_DETAILS';
export const ADD_DATE_TIME = 'ADD_DATE_TIME';
export const SET_STEP_COUNT = 'SET_STEP_COUNT';
export const ADD_DESCRIBE_REQ = 'ADD_DESCRIBE_REQ';
export const ADD_COST_TYPE = 'ADD_COST_TYPE';
export const ADD_ADDITIONAL_INFO = 'ADD_ADDITIONAL_INFO';
export const ADD_MIC_DETAILS = 'ADD_MIC_DETAILS';

//dashboard

export const GET_SUGGESTED_TAB = 'GET_SUGGESTED_TAB';
export const ADD_MIC_DATA = 'ADD_MIC_DATA';
//Register and login 

export const SET_SIGNUP_DATA = 'SET_SIGNUP_DATA';
export const SET_LOGIN_DATA = 'SET_LOGIN_DATA';
export const SET_AUTHENTICATION_ERROR = 'SET_AUTHENTICATION_ERROR';
export const SET_AUTHENTICATED_STATUS = 'SET_AUTHENTICATED_STATUS';
export const RENDER_LOGIN_MODAL = 'RENDER_LOGIN_MODAL';
export const NEW_USER_DATA = 'NEW_USER_DATA';


//post actions

export const GET_MIC_DATA = 'GET_MIC_DATA';
export const GET_REGION = 'GET_REGION';
export const GET_FEES = 'GET_FEES';
export const SET_SELECTED_MIC = 'SET_SELECTED_MIC';

//filter all mic
export const SET_SEARCH_VALUE = 'SET_SEARCH_VALUE';
export const SET_REGION_VALUE = 'SET_REGION_VALUE';
export const SET_FEES_VALUE = 'SET_FEES_VALUE';
export const SET_FEATURED_VALUE = 'SET_FEATURED_VALUE';


//accout action
export const IS_ACCOUT_UPDATED = 'IS_ACCOUT_UPDATED';

// advertisement module

export const GET_ADVERTISE = 'GET_ADVERTISE';
