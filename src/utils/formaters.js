import _ from "lodash"

export function formatPhoneNumber(phone) {
  //normalize string and remove all unnecessary characters
  if (phone) {
    phone = phone.replace(/[^\d]/g, "")
    return phone.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3")
  }
  return null
}
//
export function formatZipNumber(zip) {
  //normalize string and remove all unnecessary characters
  if (zip) {
    zip = zip.replace(/[^\d]/g, "")
    return zip.replace(/(\d {5})(\d {4})/, "$1-$2")
  }
  return null
}
//

export function maskFormatIPAddress(ipAddress) {
  let ip = _.cloneDeep(ipAddress).split(".")
  let newIp = ""
  for (var i = 0; i < ip.length; i = i + 1) {
    for (var j = ip[i].length; j < 3; j = j + 1) {
      ip[i] = ip[i] + "_"
    }
    newIp = newIp + ip[i] + (i < ip.length - 1 ? "." : "")
  }
  return newIp
}
