import axios from 'axios'
import {errorHandle } from './errorHandling'
import {APP_CONFIG} from '../constants/config';
//import {header}from './header';

export function getMyMic(producerId, page, micStatus) {
  try {
    return axios.get(APP_CONFIG.API_URL+'my-mics/' + producerId + '?page=' + page + '&mic_status=' + micStatus, {
      headers:{
        'Content-Type': 'application/json',
        'authorization': sessionStorage.getItem('token'),
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}



export function getRegions() {
  try {
    return axios.get(APP_CONFIG.API_URL+'get-regions', {
      headers:{
        'Content-Type': 'application/json',
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}

export function getFees() {
  try {
    return axios.get(APP_CONFIG.API_URL+'get-fees', {
      headers:{
        'Content-Type': 'application/json',
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}

export function getAllMic() {
  try {
    return axios.get(APP_CONFIG.API_URL+'fetch-mic', {
      headers:{
        'Content-Type': 'application/json',
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}


export function getMicDetails(micId) {
  try {
    return axios.get(APP_CONFIG.API_URL+'mic-detail/' + micId, {
      headers:{
        'Content-Type': 'application/json',
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}

export function editMicDetails(micId, micData) {
  try {
    return axios.put(APP_CONFIG.API_URL+'mic-detail/' + micId, micData, {
      headers:{
       // 'Content-Type': 'application/json',
        'Content-Type' : 'multipart/form-data',
        'authorization': sessionStorage.getItem('token'),
        // 'Access-Control-Allow-Methods': 'PUT, POST, GET, DELETE, OPTIONS',
        // "Access-Control-Allow-Origin": "*",
        // "Access-Control-Allow-Headers":"Origin, X-Requested-With, Content-Type, Accept"
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}

export function getMicDelete(micId) {
  try {
    return axios.delete(APP_CONFIG.API_URL+'mic-detail/' + micId, {
      headers:{
        'Content-Type': 'application/json',
        'authorization': sessionStorage.getItem('token'),
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}


export function postNewMic(micData) {
  try {
    return axios.post(APP_CONFIG.API_URL+'fetch-mic', micData, {
      headers:{
       // 'Content-Type': 'application/json',
        'Content-Type' : 'multipart/form-data',
        'authorization': sessionStorage.getItem('token'),
        // 'Access-Control-Allow-Methods': 'PUT, POST, GET, DELETE, OPTIONS',
        // "Access-Control-Allow-Origin": "*",
        // "Access-Control-Allow-Headers":"Origin, X-Requested-With, Content-Type, Accept"
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}

export function postNewMicNewUser(micData) {
  try {
    return axios.post(APP_CONFIG.API_URL+'add-mic', micData, {
      headers:{
       // 'Content-Type': 'application/json',
        'Content-Type' : 'multipart/form-data',
        //'authorization': sessionStorage.getItem('token'),
        // 'Access-Control-Allow-Methods': 'PUT, POST, GET, DELETE, OPTIONS',
        // "Access-Control-Allow-Origin": "*",
        // "Access-Control-Allow-Headers":"Origin, X-Requested-With, Content-Type, Accept"
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}

export function changeMicStatus(micId, micStatus) {
  try {
    return axios.put(APP_CONFIG.API_URL+'mic-status/' + micId, micStatus, {
      headers:{
       'Content-Type': 'application/json',
      'authorization': sessionStorage.getItem('token'),
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}

export function getMicCategoty() {
  try {
    return axios.get(APP_CONFIG.API_URL+'get-category', {
      headers:{
        'Content-Type': 'application/json',
        'authorization': sessionStorage.getItem('token'),
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}

export function getTimeSlot() {
  try {
    return axios.get(APP_CONFIG.API_URL+'mic-timeslot', {
      headers:{
        'Content-Type': 'application/json',
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}