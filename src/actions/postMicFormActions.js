import * as types from './actionTypes';

export const addAccountDetails = (accountDetails) => ({
  type: types.ADD_ACCOUNT_DETAILS,
  payload: accountDetails,
});

export const setStepCount = (countData) => ({
  type: types.SET_STEP_COUNT,
  payload: countData,
});

export const addDateTime = (date) => ({
  type: types.ADD_DATE_TIME,
  payload: date,
});

export const addDescribe = (describeData) => ({
  type: types.ADD_DESCRIBE_REQ,
  payload: describeData,
});

export const addCostType = (costType) => ({
  type: types.ADD_COST_TYPE,
  payload: costType,
});

export const addAddtionalInfo = (additionalData) => ({
  type: types.ADD_ADDITIONAL_INFO,
  payload: additionalData,
});

export const addMicDetails = (micData) => ({
  type: types.ADD_MIC_DETAILS,
  payload: micData,
});

export const renderLoginModal = (renderLogin) => ({
  type: types.RENDER_LOGIN_MODAL,
  payload: renderLogin,
});




