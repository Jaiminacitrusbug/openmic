import axios from 'axios'
import {errorHandle } from './errorHandling'
import {APP_CONFIG} from '../constants/config';


export function getAllMicDayANDSearch(search,dayValue) {
  try {
    return axios.get(APP_CONFIG.API_URL+'search', {
      headers:{
        'Content-Type': 'application/json',
      },
      params: {
        search_keyword:search,
        day_value:dayValue,
      }
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}

export function getAllMicFilter(region_filter, fees_filter, featured_filter, day_filter, search_filter, category_filter,page) {
  try {
    return axios.get(APP_CONFIG.API_URL+'mic-filter', {
      headers:{
        'Content-Type': 'application/json',
      },
      params: {
        region_filter:region_filter,
        fees_filter:fees_filter,
        featured_filter:featured_filter,
        day_filter:day_filter,
        search_filter:search_filter,
        category_filter:category_filter,
        page:page
      }
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}


export function getAdvertisement(data) {
  try {
    return axios.get(APP_CONFIG.API_URL+'advertisement/' + data, {
      headers:{
        'Content-Type': 'application/json',
      },
  }).then(response => {
          return response.data;
      }).catch(error => {
          return errorHandle(error);
      });
  } catch (err) { console.log('Err::: ', err); return err }
}



