import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './assets/css/bootstrap.min.css';
import './assets/css/bootstrap-float-label.min.css';
import './assets/css/select2.min.css';
import './assets/css/animate.min.css';
import './assets/css/font-awesome.min.css';
import './assets/css/owl.theme.default.css'
import 'react-tabs/style/react-tabs.css';
import './assets/css/custom.css';
// import './assets/css/steps/style.css';

// process.env.NODE_ENV === 'development' ? module.hot.accept() : null;

ReactDOM.render(<App />, document.getElementById('root'));
