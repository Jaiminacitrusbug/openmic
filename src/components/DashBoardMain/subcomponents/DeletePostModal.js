import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import blackLogo from '../../../assets/images/logo-black.svg';
import { getMicDelete, getMyMic } from '../../../api/getMicApi';
import {getpostMicData } from '../../../actions/postMicActions';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { toast } from 'react-toastify';

class DeletePostModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editId: '',
      userdata: JSON.parse(sessionStorage.getItem('user')),
    };
    this.handleDeleteMic = this.handleDeleteMic.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { editId } = this.props;
    if(prevProps.editId !== editId) {
      this.setState({ editId: editId })
    }
  }

  handleDeleteMic () {
    const { editId } = this.state;
    getMicDelete(editId).then((result) => {
      if (result) {
        if (result.code === 200 && result.status === true ) {
          this.getMic();
          toast.error(result.message);
            this.props.hideModal();
        }
      }
    })
  }

  getMic() {
    const myMicId = this.state.userdata.id;
    const { tabIndex} = this.props;
    var mic_status = '';
    if (this.props.tabIndex === 0) {
      mic_status = 'SAVED'
    } else if (this.props.tabIndex === 1) {
      mic_status='ALL'
    }
    getMyMic(myMicId,1,mic_status).then((result) => {
      let micData = [];
      if (result && result.code === 200 && result.status === true) {
        this.props.getpostMicData(result.data);
      } else {

      }
    })
  }

  render() {
    return (
      <Modal className="modal fade modal-custom" role="dialog" show={this.props.display} onHide={this.props.hideModal} >
      <div className="modal-header">
        <h4 className=""></h4>
        <button onClick={this.props.hideModal} type="button" className="close" data-dismiss="modal">&times;</button>
      </div>
      <div className="modal-body">
        <div className="modal-logo">
          <a href="#">
            <img src={blackLogo} alt="logo" />
          </a>
        </div>
        <div className="modal-heading dotted-gradient">
          <h3>Are you sure you want to delete your post..?</h3>
        </div>
            <div className="row">
            <div className="col-lg-12 text-right">
                <div className="btn-groups">
                <button onClick={this.props.hideModal} className="btn btn-primary not-now">Not Now</button>
                  <button onClick={this.handleDeleteMic}  className="btn btn-danger delete">Delete Post</button>
                </div>
              </div>
            </div>
        </div>
    </Modal>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  getpostMicData: data => dispatch(getpostMicData(data)),
});

// const mapStateToProps = state => ({
//   tabName: state.dashboard.tabName,
// });
export default withRouter(
  connect(
    null,
    mapDispatchToProps,
  )(DeletePostModal),
);