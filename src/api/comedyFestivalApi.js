import axios from 'axios'
import {errorHandle } from './errorHandling'
import {APP_CONFIG} from '../constants/config';


export function getComedyFestivals() {
    try {
      return axios.get(APP_CONFIG.API_URL+'comedy-festivals', {
        headers:{
          'Content-Type': 'application/json',
        },
    }).then(response => {
            return response.data;
        }).catch(error => {
            return errorHandle(error);
        });
    } catch (err) { console.log('Err::: ', err); return err }
  }

  export function addComedyFestivals(data) {
    console.log('festival data', data);
    try {
      return axios.post(APP_CONFIG.API_URL+'comedy-festivals/', data, {
        headers:{
          'Content-Type': 'application/json',
         // 'Content-Type' : 'multipart/form-data',
        },
    }).then(response => {
            return response.data;
        }).catch(error => {
            return errorHandle(error);
        });
    } catch (err) { console.log('Err::: ', err); return err }
  }