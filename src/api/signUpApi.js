import axios from 'axios'
import {errorHandle }from './errorHandling'
import {APP_CONFIG} from '../constants/config'

// import axiosInstance from'../utils/axiosInstance'


export function signup(data) {
    try {
        return axios.post(APP_CONFIG.API_URL + 'register', data,  
        {
            headers:{
            'Content-Type': 'application/json',
          },
        }
          )
            .then(function (response) {
                return response.data
            })
            .catch(function (error) {
                return errorHandle(error)
            })
    }catch (err) {console.log('Err::: ', err); return err }
}

export function activateAccount(token) {
    try {
        return axios.put(APP_CONFIG.API_URL + 'activate/' + token,  
        {
            headers:{
            'Content-Type': 'application/json',
          },
        }
          )
            .then(function (response) {
                return response.data
            })
            .catch(function (error) {
                return errorHandle(error)
            })
    }catch (err) {console.log('Err::: ', err); return err }
}

export function resendSignUpMail(data) {
    try {
        return axios.post(APP_CONFIG.API_URL + 'resend_mail/',  data,  {
            headers:{
                'Content-Type' : 'multipart/form-data',
          },
        })
            .then(function (response) {
                return response.data
            })
            .catch(function (error) {
                return errorHandle(error)
            })
    }catch (err) {console.log('Err::: ', err); return err }
}




