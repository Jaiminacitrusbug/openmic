import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import AccountDetails_s1 from './subcomponents/AccountDetails_s1';
import DateTimeDetails from './subcomponents/DateTimeDetails';
import MicToOpen from './subcomponents/MicToOpen';
import CostOfPerformence from './subcomponents/CostOfPerformence';
import AdditionalInfo from './subcomponents/AdditionalInfo';
import MicPreview from './subcomponents/MicPreview';
import MicInformation from './subcomponents/MicInformation';
import { renderLoginModal } from '../../actions/postMicFormActions';

class PostFormsSteps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accountDetails: {},
      micDetails: {},
      count: 0,
      date:'',
      authenticated: false,
    };
    this.handleLoginModal = this.handleLoginModal.bind(this);
  }

  componentDidMount() {
    const { authenticated } = this.props;
    this.setState({ authenticated }, () => {
      if (this.state.authenticated === true ) {
        this.setState({ count: 1 })
      } else {
        this.setState({ count: 0 })
      }
    });
  }

  componentDidUpdate(prevProps) {
    const { accountDetails, count, micData, authenticated } = this.props;
    if (prevProps.accountDetails !== accountDetails) {
      this.setState({ accountDetails }, () => {
      });
    }
    if (prevProps.micData !== micData) {
      this.setState({ micData }, () => {
      });
    }
    if(prevProps.count !== count) {
      this.setState({ count }, () => {
      });
    }
    if(prevProps.authenticated !== authenticated) {
      this.setState({ authenticated }, () => {
        if (this.state.authenticated === true ) {
          this.setState({ count: 1 })
        } else {
          this.setState({ count: 0 })
        }
      });
    }
  }

  handleLoginModal() {
    const renderLogin = true;
    this.props.renderLoginModal(renderLogin);
  }

  render() {
    const { authenticated } = this.state;
    return (
      <div className="middle-container">
        <section className="postmic-secttion main">
          <div className="container">
            <div className="post-mic-form post-mic-form-steps">
              <div className="row">
                  <div className="col-lg-12">
                    <div className="post-mic-heading">
                      <h3 >Post your mic...</h3>
                      {authenticated === false?
                      <p>First, you need to open a free account so can you post, edit and manage the details of your Open Mic. If you have already Have account  <a href="#" onClick={this.handleLoginModal} data-toggle="modal" data-target="#login-post-mic" className="login-txt">LOGIN</a></p>
                       : null }
                      </div>
                  </div>
                </div>
              {/* step form starting */}
                {/* <form className="signup-form" encType="multipart/form-data"> */}
                <h3>
                </h3>
               
               
                {/* step1 */}
                {this.state.count === 0?
                <AccountDetails_s1 />
                : null }
                {/* end of step1  */}
               
                  {/* step2 */}
                  {this.state.count === 1?
                  <DateTimeDetails />
                  :null}
                  {/* end of step2 */}

                  {/* step3 */}
                    {this.state.count === 2?
                    <MicToOpen/> : null}
                  {/* endof setp3 */}

                  {/* step4 */}
                    {this.state.count === 3?
                    <CostOfPerformence/> : null}
                  {/* endof step4 */}

                  {this.state.count === 4? 

                  <MicInformation/> : null }

                  {/* step5 */}
                      {this.state.count === 5?
                      <AdditionalInfo />
                    : null }
                  {/* endofstep5 */}
                  
                  {/* step6 */}
                        {this.state.count === 6?
                        <MicPreview/>: null }
                  {/* endof step6 */}


              {/* </form> */}
              {/* step form end  */}
            </div>
          </div>
        </section>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  accountDetails: state.accountDetails.accountDetails,
  count: state.accountDetails.count,
  selectedDate: state.accountDetails.selectedDate,
  micData: state.accountDetails.micData,
  authenticated: state.authUser.authenticated,
});

const mapDispatchToProps = dispatch => ({
  renderLoginModal: renderLogin => dispatch(renderLoginModal(renderLogin)),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(PostFormsSteps),
);
