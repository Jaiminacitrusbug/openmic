import React, { PureComponent } from 'react'
import MaskedInput from 'react-maskedinput'
import {
    CardElement,
    CardNumberElement,
    CardExpiryElement,
    CardCVCElement,
    injectStripe,
    StripeProvider,
    Elements,
} from 'react-stripe-elements';
import RequestToParticipate from '../RequestToParticipate';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import RequestToParticipateWithPayment from '../RequestToParticipateWithPayment';
import { addParticipant, addPayment } from '../../api/participantAPI';
import history from '../../services/BrowserHistory';
import { toast, ToastContainer } from 'react-toastify';
const createOptions = () => {
    return {
        style: {
            base: {
                fontSize: '16px',
                color: '#424770',
                letterSpacing: '0.025em',
                '::placeholder': {
                    color: '#aab7c4',
                },
            },
            invalid: {
                color: '#c23d4b',
            },
        },
    };
};
class PaymentComponent extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            errorMessage: '',
            makeDefault: false,
            name: '',
            noError: false,
            cardNumber: false,
            cardNumberError: "You card number is incomplete",
            cardExpiry: false,
            cardExpiryError: "Your card's expiration date is incomplete.",
            cardCvc: false,
            cardCvcError: "Your card's security code is incomplete.",
            cardType: '',
            loading: false,
        }
    }

    handleChange = (event) => {

        const elementName = event.elementType

        if (elementName == "cardNumber") {
            this.setState({ cardNumber: event.complete, cardType: event.brand })
            if (event.error) {
                this.setState({ errorMessage: event.error.message });
                if (elementName == "cardNumber") { this.setState({ cardNumberError: event.error.message }) }
            } else {
                this.setState({ errorMessage: '', cardNumberError: '' });
            }

        }

        if (elementName == "cardExpiry") {
            this.setState({ cardExpiry: event.complete })
            if (event.error) {
                this.setState({ errorMessage: event.error.message });
                if (elementName == "cardExpiry") { this.setState({ cardExpiryError: event.error.message }) }
            } else {
                this.setState({ errorMessage: '', cardExpiryError: '' });
            }
        }

        if (elementName == "cardCvc") {
            this.setState({ cardCvc: event.complete })
            if (event.error) {
                this.setState({ errorMessage: event.error.message });
                if (elementName == "cardCvc") { this.setState({ cardCvcError: event.error.message }) }
            } else {
                this.setState({ errorMessage: '', cardCvcError: '' });
            }
        }

    };

    handleCardName = (e) => {
        const { value } = e.target;
        this.setState({ name: value });
    }

    handleSubmit = (evt) => {

        if (this.state.cardNumber === false) {
            this.setState({ errorMessage: this.state.cardNumberError });
        }
        if (this.state.cardExpiry === false) {
            this.setState({ errorMessage: this.state.cardExpiryError });
        }
        if (this.state.cardCvc === false) {
            this.setState({ errorMessage: this.state.cardCvcError });
        }
        evt.preventDefault();

        if (this.props.stripe) {
            this.setState({ loading: true })
            this.props.stripe.createToken({ name: this.state.name }).then(result => {
     
                const userData  = {
                    mic: this.props.participantData.mic,
                    participant_first_name: this.props.participantData.first_name,
                    customer_token: result.token.id,
                    participant_email: this.props.participantData.email,
                    participant_last_name: this.props.participantData.last_name,
                    profile_image_id: 1,
                    first_name: this.props.participantData.first_name,
                    last_name:this.props.participantData.last_name,
                    email: this.props.participantData.email,
                    comment: this.props.participantData.comment,
                    contact: this.props.participantData.contact,

                }
                addParticipant(userData).then((result) => {
                    if (result) {
                        if (result.code === 201 && result.status === true) {
                            this.setState({ loading: false })
                            toast.info(result.message);
                            this.setState({errorMessage: ''})
                            history.push('/thanks');
                        }else if (result.code === 200 && result.status === false) {
                            this.setState({ loading: false })
                            toast.error(result.message);
                            this.setState({errorMessage: result.message})
                        } else if(result.code === 400 && result.status === false) {
                            this.setState({ loading: false })
                            toast.error(result.message);
                            this.setState({errorMessage: result.message})
                        }
                } 
                })
            });
        } else {
            this.setState({ loading: false })
            toast.error("Stripe.js hasn't loaded yet.");
        }
    };

    render() {
        return (
            <div className="middle-container">
                <section className="postmic-secttion payment-section">
                    <div className="container">
                        <div className="post-mic-form">
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="post-mic-heading">
                                        <h3 >Payment</h3>
                                        <p>Make payment and book your seat.</p>
                                    </div>
                                </div>
                                <div className="col-lg-12 col-md-6">
                                <div className="error" role="alert">
                                    <p className="error-message-p">{this.state.errorMessage}</p>
                                </div>
                                </div>
                                {/* -----Card Number-----     */}
                                <div className="col-lg-12 col-md-6">
                                    <div className="form-group">
                                        <label className="has-float-label">
                                            <CardNumberElement {...createOptions()} required onChange={this.handleChange} className="form-control" id="exampleInputEmail2" placeholder="XXXX XXXX XXXX XXXX" aria-describedby="emailHelp" mask="1111 1111 1111 1111" />
                                            <span>Add card number</span>
                                        </label>
                                    </div>
                                </div>
                                {/* -------Card Name -------- */}
                                <div className="col-lg-6 col-md-6">
                                    <div className="form-group form-group-name">
                                        <label className="has-float-label">
                                            <input className="form-control" type="text" placeholder="Enter card holder name" name="cardName" value={this.state.name} onChange={this.handleCardName} />
                                            <span>Card holder name</span>
                                        </label>
                                    </div>
                                </div>



                                {/* --------Card Date -------- */}

                                <div className="col-lg-3 col-md-6">
                                    <div className="form-group">
                                        <label className="has-float-label">
                                            <CardExpiryElement {...createOptions()} onChange={this.handleChange} required type="text" className="form-control simple-field-data-mask" id="expiry" placeholder="MM/YY" mask="11/11" name="expiry" />
                                            <span>Expiry date</span>
                                        </label>
                                    </div>
                                </div>



                                {/* -----------Card Cvv------- */}

                                <div className="col-lg-3 col-md-6">
                                    <div className="form-group">
                                        <label className="has-float-label">
                                            <CardCVCElement {...createOptions()} required onChange={this.handleChange} type="text" className="form-control" id="exampleInputEmail2" placeholder="XXX" aria-describedby="emailHelp" mask="111" name="ccv" />
                                            <span>CVV</span>
                                        </label>
                                    </div>
                                </div>

                                {/* </div> */}
                                {/* </form> */}

                            </div>

                            <div className="login-form-footer">
                                <div className="row">

                                    <div className="col-lg-12 text-right">
                                        <div className="btn-groups">
                                            <button type="submit" disabled={this.state.noError || this.state.loading} className="btn btn-login" data-toggle="modal" data-target="#thankyou-popup" onClick={this.handleSubmit.bind(this)}>Pay Now {this.state.loading === true ? <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                  </div> : null}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
             
            </div>

        )
    }
}




const SplitFieldsForm = injectStripe(PaymentComponent);

class SaveCard extends PureComponent {
    constructor() {
        super();
        this.state = { stripe: null, renderRequestModal: false };
    }
    componentDidMount() {
        if (window.Stripe) {
            this.setState({ stripe: window.Stripe('pk_test_OpVX8vOkAfUNbP0G9pcE6oFW00KMPHzg6z') });
        } else {
            document.querySelector('#stripe-js').addEventListener('load', () => {
                // Create Stripe instance once Stripe.js loads
                this.setState({ stripe: window.Stripe('pk_test_OpVX8vOkAfUNbP0G9pcE6oFW00KMPHzg6z') });
            });
        }
        this.handleRequestModal();
    }

    handleRequestModal() {
        this.setState({ renderRequestModal: true });
    }

    handleCloseRequestModal = () => {
        this.setState({ renderRequestModal: false });
    }

    handleParticipantData = (data) => {
        this.setState({ participantData : data});
    }

    render() {
        return (
            <div>
                <StripeProvider stripe={this.state.stripe}>
                    <Elements>
                        <SplitFieldsForm participantData={this.state.participantData}/>
                    </Elements>
                </StripeProvider>
                <RequestToParticipateWithPayment display={this.state.renderRequestModal} hide={this.handleCloseRequestModal} micId={this.props.location && this.props.location.state && this.props.location.state.micId} participantData={this.handleParticipantData} />
                <ToastContainer autoClose={2000} />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    postMicData: state.postMic.postMicData,
    micData: state.postMic.micData,
});

export default withRouter(
    connect(
        mapStateToProps,
        null,
    )(SaveCard),
);